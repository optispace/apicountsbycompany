﻿using ApiCountsByCompany.Common;
using Serilog;
using Serilog.Core.Enrichers;
using Serilog.Events;
using Serilog.Sinks.Graylog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Common
{
    public static class Helper 
    {
        public static string SeparateSerialNumber(string hashCode)
        {
            var items = Coder.Decrypt(hashCode).Split(Constants.SeparatorInTextFileWithCounts);
            string serialnumber = items[1];
            return serialnumber;
        }

        public static string DecryptCoder(string value)
        {
            string result = "";
            try
            {
                result = Common.Coder.Decrypt(value);
            }
            catch (Exception e)
            {
                result = "";
            }
            return result;
        }
    }
}

