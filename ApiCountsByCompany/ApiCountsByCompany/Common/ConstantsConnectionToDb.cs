﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Common
{
    public class ConstantsConnectionToDb
    {
        #region Types of databases

        public enum TypeOfDatabes
        {
            Optispace,
            OptiSpaceHistory,
            OptiSpaceDatabaseMain,
        }

        #endregion

        #region Connection String - L48

        //public static string ConnectionStringsPart1 = "Server=L48\\SQLEXPRESS;Database=OptiSpace.";
        //public static string ConnectionStringsPart2 = ";Trusted_Connection=True;";
        //public static string ConnectionStringsHistoryPart1 = "Server=L48\\SQLEXPRESS;Database=OptiSpaceHistory.";
        //public static string ConnectionStringsHistoryPart2 = ";Trusted_Connection=True;";

        #endregion

        #region Connection String - L100

        //public static string ConnectionStringsPart1 = "Server=L100\\SQLEXPRESS;Database=OptiSpace.";
        //public static string ConnectionStringsPart2 = ";Trusted_Connection=True;";
        //public static string ConnectionStringsHistoryPart1 = "Server=L100\\SQLEXPRESS;Database=OptiSpaceHistory.";
        //public static string ConnectionStringsHistoryPart2 = ";Trusted_Connection=True;";

        #endregion

        #region Connection String - Server OPTICOUNTER

        public static string ConnectionStringsPart1 = @"Data Source=localhost;Initial Catalog=OptiSpace" + ".";
        public static string ConnectionStringsPart2 = @";user id=sa;password=USN9ajh6";
        public static string ConnectionStringsHistoryPart1 = @"Data Source=localhost;Initial Catalog=OptiSpaceHistory" + ".";
        public static string ConnectionStringsHistoryPart2 = " ;user id=sa;password=USN9ajh6";

        #endregion

        #region Connection String - Server 192.168.1.59

        //public static string ConnectionStringsPart1 = @"Data Source=192.168.1.59\SQL;Initial Catalog=OptiSpace" + ".";
        //public static string ConnectionStringsPart2 = @";user id=f642d91017db4198a62302f9348f8cd;password=opti172space!";
        //public static string ConnectionStringsHistoryPart1 = @"Data Source=192.168.1.59\SQL;Initial Catalog=OptiSpaceHistory" + ".";
        //public static string ConnectionStringsHistoryPart2 = " ;user id=f642d91017db4198a62302f9348f8cd;password=opti172space!";

        #endregion

        #region Connection String - AZ - Private

        //public static string ConnectionStringsPart1 = "Server=DESKTOP-GFUL49S\\SQLEXPRESS;Database=OptiSpace.";
        //public static string ConnectionStringsPart2 = ";Trusted_Connection=True;";
        //public static string ConnectionStringsHistoryPart1 = "Server=DESKTOP-GFUL49S\\SQLEXPRESS;Database=OptiSpaceHistory.";
        //public static string ConnectionStringsHistoryPart2 = ";Trusted_Connection=True;";

        #endregion
    }
}
