﻿using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Common
{
    public class Constants
    {
        #region Global parameters
        public static readonly string FormatTxt = ".txt";
        public static readonly string PathToFolderWithCounts = @"C:\App_Data\";
        public static readonly string InMark = "0";
        public static readonly string OutMark = "1";
        public static readonly string SeparatorInTextFileWithCounts = "|";
        public static readonly string PathToFileWthNameCompanies = "company" + FormatTxt;
        public static readonly string FileFormatCounts = "yyyy00MM00dd";
        public static readonly string FileFormatCountsHHmmss = "HH:mm:ss";
        public static readonly string DBFormatCountsyyyymmdd = "yyyy-MM-dd";
        //public static readonly DateTime nowMinusXMinutes = DateTime.Now.AddMinutes(-5);
        #endregion

        #region StatusRequest 
        public static readonly string StatusRequestOk = "Ok";
        public static readonly string StatusOpticounterError = "Server Opticounter Error ";
        public static readonly string StatusRequestBadRequest = "BadRequest";
        public static readonly string StatusRequestNotEncrypted = "NotEncrypted";
        public static readonly string StatusRequestListCountsNull = "ListCountsNull";
        public static readonly string StatusRequestOk_LiveSignal = "LiveSignal_Ok";
        public static readonly string StatusRequestOk_ListCounts = "ListCounts_Ok";
        public static readonly string StatusRequestNotEncrypted_LiveSignal = "NotEncrypted";
        public static readonly string StatusRequestNullPoint_LiveSignal = "NullPoint";
        #endregion

        #region GrayLog 
        //DEVELOP
        //public static readonly string NameStreamGrayLog = "ApiCountsByCompany_Develop";
        //public static readonly string IpServerGrayLog = "81.168.129.19";
        //public static readonly LogEventLevel minimumLogEventLevelGrayLog = LogEventLevel.Information;
        //public static readonly int PortGrayLog = 9000;
        //PRODUCTION
        public static readonly string NameStreamGrayLog = "ApiCountsByCompany";
        public static readonly string IpServerGrayLog = "81.168.129.19";
        public static readonly LogEventLevel minimumLogEventLevelGrayLog = LogEventLevel.Information;
        public static readonly int PortGrayLog = 9000;
        #endregion

        #region Tokens
        public static readonly string AddDeviceToken = "TestToken";
        #endregion

        #region AQUELLA
        public static readonly string OptimediaAquellaUrl = "http://87.98.139.216/ApiCountsTest/counts/aquellaansbach";
        public static readonly string OptimediaAquellaSaunaUrl = "http://87.98.139.216/ApiCountsTest/counts/aquellaansbachsauna";
        public static readonly string OptimediaAquellaHallenbadUrl = "http://87.98.139.216/ApiCountsTest/counts/aquellaansbachhallenbad";
        #endregion
    }
}
