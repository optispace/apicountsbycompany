﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Common
{
    public class HTTPHelper
    {
        private static readonly HttpClient client = new HttpClient();
        public static async Task<string> GetRequest(String url)
        {
            try
            {
                var result = client.GetAsync(url).Result;
                if (result.IsSuccessStatusCode)
                {
                    return result.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    return "";
                }
            }
            catch (HttpRequestException ex)
            {
                return "";
            }
        }

    }
}
