﻿using Serilog;
using Serilog.Core.Enrichers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Common
{
    public class GrayLogHelper
    {

        public static class LoggerExtensions
        {

        }

        public static void GrayLog_StatusOk(string messeage)
        {
            try
            {
                Log.Information(messeage);
            }
            catch (Exception e)
            {

            }
        }

        public static void GrayLogWithPropertyEnricherPostListToDb_StatusOk(string messeage, string serialNumber, string comapnyName, int amount)
        {
            try
            {
                Log.Information(messeage + " "+serialNumber +" "+ comapnyName+ " "+ amount);
            }
            catch (Exception e)
            {

            }
        }

        public static void GrayLogWithPropertyEnricherSmallInfo_BadRequest(string className, string messeage )
        {
            try
            {
                var withProps = Log.ForContext(new[]
                {
                    new PropertyEnricher("ClassName", className),
                    new PropertyEnricher("Error", messeage),
                });
                withProps.Fatal(Constants.StatusRequestBadRequest);
            }
            catch (Exception e)
            {

            }
        }

        public static void GrayLogWithPropertyEnricherBigInfo_BadRequest(string className, string messeage, string serialNumber, string comapnyName)
        {
            try
            {
                var withProps = Log.ForContext(new[]
                {
                    new PropertyEnricher("ClassName", className),
                    new PropertyEnricher("Error", messeage),
                });
                withProps.Fatal(Constants.StatusRequestBadRequest + " " + serialNumber + " " + comapnyName);
            }
            catch (Exception e)
            {

            }
        }

        public static void GrayLogWithPropertyEnricherError(string className, string message, string serialNumber, string companyName)
        {
            try
            {
                var withProps = Log.ForContext(new[]
                {
                    new PropertyEnricher("ClassName", className),
                    new PropertyEnricher("Error", message),
                });
                withProps.Fatal(Constants.StatusOpticounterError + " " + serialNumber + " " + companyName);
            }
            catch (Exception e)
            {

            }
        }

    }
}
