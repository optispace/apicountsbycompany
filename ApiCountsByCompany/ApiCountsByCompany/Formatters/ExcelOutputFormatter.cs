﻿using ApiCountsByCompany.Infrastructure.DTO.CountsDb;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Formatters
{
    public static class ExcelOutputFormatter
    {
        public static byte[] SumCountsFromIdGroupPerDays(ICollection<CountsSumInOutPerDay> countsSumInOutPerDays)
        {
            var comlumHeadrs = new string[]
            {
                "DAY",
                "IN",
                "OUT",
            };

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Counts"); 

                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                var j = 2;
                foreach (var counts in countsSumInOutPerDays)
                {
                    worksheet.Cells["A" + j].Value = counts.Day;
                    worksheet.Cells["B" + j].Value = counts.SumIn;
                    worksheet.Cells["C" + j].Value = counts.SumOut;
                    j++;
                }
                return package.GetAsByteArray();
            }
        }
    }
}

    

