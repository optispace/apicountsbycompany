﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCountsByCompany.Contracts;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiCountsByCompany.Controllers
{
    [Route("[controller]")]
    public class CountsDBController : Controller
    {
        private readonly ICountsDBService _countsDBService;
        private readonly ISenderSevice _senderService;

        public CountsDBController(ICountsDBService countsDBService, ISenderSevice senderSevice)
        {
            _countsDBService = countsDBService;
            _senderService = senderSevice;
        }


        [HttpGet(ApiRoutes.CountsDB.countsdb)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetCountsByCompanyName([FromQuery] CountsDbDto getCountsDbDto)
        {

            var counts = await _countsDBService.GetCountsByCompanyNameService_DB(getCountsDbDto);
            if (counts == null)
            {
                return NotFound();
            }
            return Json(counts.Amount);
        }

        [HttpGet(ApiRoutes.CountsDB.countsreal)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetRealCountsByCompanyName([FromQuery] CountsDbDto getCountsDbDto)
        {

            var counts = await _countsDBService.GetRealCountsByCompanyName(getCountsDbDto);
            if (counts == null)
            {
                return NotFound();
            }
            return Json(counts.Amount);

        }

        [HttpPost(ApiRoutes.CountsDB.countsdb)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> CreateCounts([FromBody] CountModel countModel)
        {
            await _senderService.AddCountsToDb(countModel);
            return Created($"counts/{countModel}", null);
        }

        // DELETE, usunie to wszytskie zlicznia z bieżącego dnia bezpowrotnie z bazy danych. Pytanie czy chcemy aby klient miał taką funkcjonalność ?
        [HttpDelete(ApiRoutes.CountsDB.countsdb)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteAllCounts([FromBody] CountsDbDto countsDbDto)
        {
            await _countsDBService.DeleteAllCountsWithOneScreen_DB(countsDbDto);
            return NoContent();
        }
    }
}