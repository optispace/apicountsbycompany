﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using ApiCountsByCompany.Common;
using ApiCountsByCompany.Contracts;
using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Infrastructure.Commands.Device;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql;
using ApiCountsByCompany.Infrastructure.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace ApiCountsByCompany.Controllers
{
    [Route("[controller]")]
    public class DeviceController : Controller
    {
        private readonly IDeviceService _deviceService;
        private readonly ISenderSevice _senderService;

        public DeviceController(IDeviceService deviceService, ISenderSevice senderService)
        {
            _deviceService = deviceService;
            _senderService = senderService;
        }

        [HttpGet(ApiRoutes.Device.devicedetails)]
        [HttpGet("{companyName}")]
        public async Task<IActionResult> GetAllDeviceByCompanyName(string companyName)
        {

            var devicesDetails = await _deviceService.GetDetailsDto(companyName);
            if (devicesDetails == null)
            {
                return NotFound();
            }
            return Json(devicesDetails);
        }

        [HttpGet(ApiRoutes.Device.devicepoint)]
        public async Task<IActionResult> GetAllDeviceBySerialNumber(string sn, string token)
        {

            var devicesDetails = await _deviceService.GetDetails(sn, token);
            if (devicesDetails == null)
            {
                return NotFound();
            }
            return Json(devicesDetails);
        }


        [HttpPost(ApiRoutes.Device.device)]
        public async Task<IActionResult> AddDevice(DeviceDTO deviceDTO)
        {
            var authorization = Request.Headers[HeaderNames.Authorization];
            AddDeviceResponse result = new AddDeviceResponse();
            if (AuthenticationHeaderValue.TryParse(authorization, out var headerValue))
            {

                var scheme = headerValue.Scheme;
                var parameter = headerValue.Parameter;
                if(parameter == Constants.AddDeviceToken)
                {
                     result = await _deviceService.AddDevice(deviceDTO);
                }

            }
            if(result.Success == true)
            {
                return Created($"counts/{deviceDTO}", result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPost(ApiRoutes.Device.device + "/remove")]
        public async Task<IActionResult> RemoveDevice(DeviceDTO deviceDTO)
        {
            var authorization = Request.Headers[HeaderNames.Authorization];
            AddDeviceResponse result = new AddDeviceResponse();
            if (AuthenticationHeaderValue.TryParse(authorization, out var headerValue))
            {

                var scheme = headerValue.Scheme;
                var parameter = headerValue.Parameter;
                if (parameter == Constants.AddDeviceToken)
                {
                    result = await _deviceService.RemoveDevice(deviceDTO);
                }

            }
            if (result.Success == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPost(ApiRoutes.Device.device+"/extended")]
        public async Task<IActionResult> AddExtendedDevice([FromBody] DeviceExtendedDTO deviceDTO)
        {
            var authorization = Request.Headers[HeaderNames.Authorization];
            AddDeviceResponse result = new AddDeviceResponse();
            if (AuthenticationHeaderValue.TryParse(authorization, out var headerValue))
            {

                var scheme = headerValue.Scheme;
                var parameter = headerValue.Parameter;
                if (parameter == Constants.AddDeviceToken)
                {
                    result = await _deviceService.AddExtendedDevice(deviceDTO);
                }

            }
            if (result.Success == true)
            {
                return Created($"counts/{deviceDTO}", result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPost(ApiRoutes.Device.deviceLifeSignalVpn)]
        public async Task<IActionResult> PostLiveSignal([FromBody] CreateLiveSignalVpn createLiveSignalVpn)
        {

            var response = await _senderService.AddLifeSignalVpn(createLiveSignalVpn);

            if (!response.ResponseLifeSignal.Equals(Constants.StatusRequestOk))
            {
                GrayLogHelper.GrayLogWithPropertyEnricherBigInfo_BadRequest(MethodBase.GetCurrentMethod().DeclaringType.FullName, response.ResponseLifeSignal, response.ResponseOrginalSerialNumber, response.ResponseCompanyName);
                return BadRequest(new LifeSignalDbResponse
                {
                    ResponseLifeSignal = response.ResponseLifeSignal,
                    ResponseOrginalSerialNumber = response.ResponseOrginalSerialNumber,
                    ResponseCompanyName = response.ResponseCompanyName
                });
            }
            GrayLogHelper.GrayLog_StatusOk(Constants.StatusRequestOk_LiveSignal + " " + response.ResponseOrginalSerialNumber + " " + response.ResponseCompanyName);
            return Created($"LiveSignalVpn/{response}", null);
        }
    }
}
