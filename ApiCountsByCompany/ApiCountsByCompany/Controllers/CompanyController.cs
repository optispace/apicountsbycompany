﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCountsByCompany.Contracts;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Infrastructure.Service;
using Microsoft.AspNetCore.Mvc;

namespace ApiCountsByCompany.Controllers
{
    [Route("[controller]")]
    public class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        [HttpGet(ApiRoutes.Company.company)]
        public async Task<IActionResult> GetCompanyNameBySerialNumber([FromQuery] DeviceModel device)
        {
            var company = await _companyService.GetCompanyNameBySerialNumber(device.SerialNumber);
            if (company == null)
            {
                return NotFound();
            }

            return Json(company.Name);
        }
    }
}
