﻿using ApiCountsByCompany.Infrastructure.Commands.Summary;
using ApiCountsByCompany.Infrastructure.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Controllers
{
    [Route("[controller]")]
    public class ApiClientController : Controller
    {
        private readonly ICountsSummaryService _countsSummaryService;
        public ApiClientController(ICountsSummaryService countsSummaryService)
        {
            _countsSummaryService = countsSummaryService;
        }

        [HttpGet("GetSummary")]
        public async Task<IActionResult> GetDayPeriod([FromQuery] DayPeriodQuery query)
        {
            var response = await _countsSummaryService.GetCountsDayPeriod(query);

            return Ok(response);
        }

        [HttpGet("GetSummaryPerHour")]
        public async Task<IActionResult> GetDayPerHourPeriod([FromQuery] DayPeriodQuery query)
        {
            var response = await _countsSummaryService.GetCountsDayPerHourPeriod(query);

            return Ok(response);
        }
    }
}
