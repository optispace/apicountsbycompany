﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCountsByCompany.Infrastructure.Commands;
using ApiCountsByCompany.Infrastructure.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiCountsByCompany.Controllers
{
    [Route("[controller]")]
    public class MockupController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> GetRandomCounts([FromQuery] RandomCountsQuery date)
        {
            var liststatisticsAggregaterDto = new List<CountsPerDayDto>();
            Random rnd = new Random();
            for (var day = date.startDate; day <= date.endDate; day = day.AddDays(1))
            {
                CountsPerDayDto CountsPerDayDto = new CountsPerDayDto()
                {
                    storeName = "Testowy Salon -Toruń ul. Tadeusza Kościuszki",
                    count = rnd.Next(1, 201),
                    date = day.ToString("yyyy-MM-dd")
                };
                liststatisticsAggregaterDto.Add(CountsPerDayDto);
            }

            return Json(liststatisticsAggregaterDto);
        }
    }
}