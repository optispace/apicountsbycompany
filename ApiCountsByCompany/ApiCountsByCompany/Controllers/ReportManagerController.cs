﻿using System;
using System.Threading.Tasks;
using ApiCountsByCompany.Contracts;
using ApiCountsByCompany.Formatters;
using ApiCountsByCompany.Infrastructure.Service;
using Microsoft.AspNetCore.Mvc;

namespace ApiCountsByCompany.Controllers
{
    [Route("api/[controller]")]
    public class ReportManagerController : Controller
    {
        private readonly ICountsDBService _countsDBService;

        public ReportManagerController(ICountsDBService countsDBService)
        {
            _countsDBService = countsDBService;
        }

        [HttpGet(ApiRoutes.ReportManager.getSummarryCountsByGroupIdPerDay)]
        public async Task<IActionResult> GetReport_SumCountsFromIdGroupPerDay(string companyName, DateTime dateFrom, DateTime dateTo,long groupId)
        {
            var countsByGroupId = await _countsDBService.GetSummarryCountsByGroupIdPerDay(companyName, dateFrom, dateTo, groupId);
            return File(ExcelOutputFormatter.SumCountsFromIdGroupPerDays(countsByGroupId), "application/ms-excel", $"Counts{companyName}.xlsx");
        }

    }
}

