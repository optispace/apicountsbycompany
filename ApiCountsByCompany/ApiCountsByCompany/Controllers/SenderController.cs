﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using ApiCountsByCompany.Common;
using ApiCountsByCompany.Contracts;
using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Infrastructure.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Serilog.Core.Enrichers;
using Serilog.Events;

namespace ApiCountsByCompany.Controllers
{
    [Route("[controller]")]
    public class SenderController : Controller
    {
        private readonly ISenderSevice _senderService;

        public SenderController(ISenderSevice senderService)
        {
            _senderService = senderService;
        }

        //LISTY
        [HttpPost(ApiRoutes.Sender.senderCountsFileAndDbList)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PostListToFileAndDb([FromBody] List<CountModel> countModelList)
        {
            var responseFile = await _senderService.AddCountsListToFile(countModelList);
            var responseDb = await _senderService.AddCountsListDb(countModelList);

            // W sytuacji gdzie nie można dodać do DB oraz Pliku, przyczyna leży w złej liście która metoda otrzymała, normalne zachowanie
            if (!responseFile.ResponseCountsAddToFile.Equals(Constants.StatusRequestOk) && !responseDb.ResponseCountsAddToDb.Equals(Constants.StatusRequestOk))
            {
                GrayLogHelper.GrayLogWithPropertyEnricherSmallInfo_BadRequest(MethodBase.GetCurrentMethod().DeclaringType.FullName, responseFile.ResponseCountsAddToFile);
                return BadRequest(new AddCountsFileAndDbResponse
                {
                    ResponseCountsAddToFile = responseFile.ResponseCountsAddToFile,
                    ResponseCountsAddToDb = responseDb.ResponseCountsAddToDb,
                    ResponseOrginalSerialNumber = responseDb.ResponseOrginalSerialNumber,
                    ResponseCompanyName = responseDb.ResponseCompanyName,
                    ResponseAmount = responseDb.ResponseAmount
                });
            }
            //Tutaj robi się ciekawiej, problem leży po stronie serwera, gdyż mógł dodać do tylko albo File albo DB, tutaj wysyłam Errora do greyloga!
            if ((!responseFile.ResponseCountsAddToFile.Equals(Constants.StatusRequestOk) && responseDb.ResponseCountsAddToDb.Equals(Constants.StatusRequestOk))|| responseFile.ResponseCountsAddToFile.Equals(Constants.StatusRequestOk) && !responseDb.ResponseCountsAddToDb.Equals(Constants.StatusRequestOk))
            {
                GrayLogHelper.GrayLogWithPropertyEnricherError(MethodBase.GetCurrentMethod().DeclaringType.FullName, "Failed to add to both Database and File!", responseDb.ResponseOrginalSerialNumber, responseDb.ResponseCompanyName);
                return BadRequest(new AddCountsFileAndDbResponse
                {
                    ResponseCountsAddToFile = responseFile.ResponseCountsAddToFile,
                    ResponseCountsAddToDb = responseDb.ResponseCountsAddToDb,
                    ResponseOrginalSerialNumber = responseDb.ResponseOrginalSerialNumber,
                    ResponseCompanyName = responseDb.ResponseCompanyName,
                    ResponseAmount = responseDb.ResponseAmount
                });
            }
            GrayLogHelper.GrayLog_StatusOk(responseFile.ResponseCountsAddToFile);
            return Created($"counts/{responseFile}", null);
        }


        [HttpPost(ApiRoutes.Sender.senderCountsFileList)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PostListToFile([FromBody] List<CountModel> countModelList)
        {
            var response = await _senderService.AddCountsListToFile(countModelList);

            if (!response.ResponseCountsAddToFile.Equals(Constants.StatusRequestOk))
            {
                GrayLogHelper.GrayLogWithPropertyEnricherSmallInfo_BadRequest(MethodBase.GetCurrentMethod().DeclaringType.FullName, response.ResponseCountsAddToFile);
                return BadRequest(new AddCountsFileResponse
                {
                    ResponseCountsAddToFile = response.ResponseCountsAddToFile
                });
            }
            GrayLogHelper.GrayLog_StatusOk(response.ResponseCountsAddToFile);
            return Created($"counts/{response}", null);
        }

        [HttpPost(ApiRoutes.Sender.senderCountsDbList)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PostListToDb([FromBody] List<CountModel> countModelList)
        {
            var response = await _senderService.AddCountsListDb(countModelList);

            if (!response.ResponseCountsAddToDb.Equals(Constants.StatusRequestOk))
            {
                GrayLogHelper.GrayLogWithPropertyEnricherBigInfo_BadRequest(MethodBase.GetCurrentMethod().DeclaringType.FullName, response.ResponseCountsAddToDb, response.ResponseOrginalSerialNumber, response.ResponseCompanyName);
                return BadRequest(new AddCountsDbResponse
                {
                    ResponseCountsAddToDb = response.ResponseCountsAddToDb,
                    ResponseOrginalSerialNumber = response.ResponseOrginalSerialNumber,
                    ResponseCompanyName = response.ResponseCompanyName,
                    ResponseAmount = response.ResponseAmount
                });
            }
            GrayLogHelper.GrayLog_StatusOk(Constants.StatusRequestOk_ListCounts + " " + response.ResponseOrginalSerialNumber + " " + response.ResponseCompanyName + " " + response.ResponseAmount);
            return Created($"ListCounts/{response}", null);
        }


        //OBIEKTY

        [HttpPost(ApiRoutes.Sender.senderCountsInFile)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PostObjectToFile([FromBody] CountModel countModel)
        {
            var response = await _senderService.AddCountsToFile(countModel);

            if (!response.ResponseCountsAddToFile.Equals(Constants.StatusRequestOk))
            {
                GrayLogHelper.GrayLogWithPropertyEnricherSmallInfo_BadRequest(MethodBase.GetCurrentMethod().DeclaringType.FullName, response.ResponseCountsAddToFile);
                return BadRequest(new AddCountsFileResponse
                {
                    ResponseCountsAddToFile = response.ResponseCountsAddToFile
                });
            }
            GrayLogHelper.GrayLog_StatusOk(response.ResponseCountsAddToFile);
            return Created($"counts/{response}", null);
        }

        [HttpPost(ApiRoutes.Sender.senderCountsInDb)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PostObjectToDb([FromBody] CountModel countModel)
        {
            var response = await _senderService.AddCountsToDb(countModel);

            if (!response.ResponseCountsAddToDb.Equals(Constants.StatusRequestOk))
            {
                GrayLogHelper.GrayLogWithPropertyEnricherSmallInfo_BadRequest(MethodBase.GetCurrentMethod().DeclaringType.FullName, response.ResponseCountsAddToDb);
                return BadRequest(new AddCountsDbResponse
                {
                    ResponseCountsAddToDb = response.ResponseCountsAddToDb
                });
            }
            GrayLogHelper.GrayLog_StatusOk(response.ResponseCountsAddToDb);
            return Created($"counts/{response}", null);
        }



        [HttpPost(ApiRoutes.Sender.senderLifeSignal)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PostLiveSignal([FromBody] LiveSignalModel lifeSignalModel)
        {

            var response = await _senderService.LifeSignalToDb(lifeSignalModel);

            if (!response.ResponseLifeSignal.Equals(Constants.StatusRequestOk))
            {
                GrayLogHelper.GrayLogWithPropertyEnricherBigInfo_BadRequest(MethodBase.GetCurrentMethod().DeclaringType.FullName, response.ResponseLifeSignal, response.ResponseOrginalSerialNumber,response.ResponseCompanyName);
                return BadRequest(new LifeSignalDbResponse
                {
                    ResponseLifeSignal = response.ResponseLifeSignal,
                    ResponseOrginalSerialNumber = response.ResponseOrginalSerialNumber,
                    ResponseCompanyName = response.ResponseCompanyName
                });
            }
            GrayLogHelper.GrayLog_StatusOk(Constants.StatusRequestOk_LiveSignal +" " + response.ResponseOrginalSerialNumber + " " + response.ResponseCompanyName);
            return Created($"LiveSignal/{response}", null);
        }


        [HttpPost(ApiRoutes.Sender.senderLifeSignalList)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PostLiveSignalList([FromBody] List<LiveSignalModel>  lifeSignalModelList)
        {

            var response = await _senderService.LifeSignalToListDb(lifeSignalModelList);

            if (!response.ResponseLifeSignal.Equals(Constants.StatusRequestOk))
            {
                GrayLogHelper.GrayLogWithPropertyEnricherBigInfo_BadRequest(MethodBase.GetCurrentMethod().DeclaringType.FullName, response.ResponseLifeSignal, response.ResponseOrginalSerialNumber, response.ResponseCompanyName);
                return BadRequest(new LifeSignalDbResponse
                {
                    ResponseLifeSignal = response.ResponseLifeSignal,
                    ResponseOrginalSerialNumber = response.ResponseOrginalSerialNumber,
                    ResponseCompanyName = response.ResponseCompanyName
                });
            }
            GrayLogHelper.GrayLog_StatusOk(Constants.StatusRequestOk_LiveSignal + " " + response.ResponseOrginalSerialNumber + " " + response.ResponseCompanyName);
            return Created($"LiveSignal/{response}", null);
        }




    }
}
