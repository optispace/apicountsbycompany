﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCountsByCompany.Contracts;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using ApiCountsByCompany.Infrastructure.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;

namespace ApiCountsByCompany.Controllers
{
    [Route("[controller]")]
    public class CountsController : Controller
    {
        private readonly ICountsService _countsService;
        private readonly ICountsDBService _countsDBService;
        private readonly IPointService _pointService;
        public CountsController(ICountsService countsService, IPointService pointService,ICountsDBService countsDBService)
        {
            _pointService = pointService;
            _countsService = countsService;
            _countsDBService = countsDBService;
        }

        [HttpGet]
        [Route("GetCountsBySerialNumberAndDateDefault")]
        public async Task<IActionResult> GetCountsBySerialNumberAndDateDefault(string Token)
        {
            return await GetCountsBySerialNumberAndDate("All", DateTime.Today.AddDays(-60), DateTime.Today, Token);
        }

        [HttpGet]
        [Route("GetCountsBySerialNumberAndDate")]
        public async Task<IActionResult> GetCountsBySerialNumberAndDate(string SerialNumber, DateTime DateFrom, DateTime DateTo, string Token)
        {
            Token = Common.Coder.Decrypt(Token);

            if (Token != "imad")
            {
                return Json("Wrong Token!");
            }
            else
            {
                if (SerialNumber == "All")
                {

                }
                List<CustomCountsDTO> CountsDTOList = new List<CustomCountsDTO>();
                List<Points> listOfPoints = _pointService.GetAllInAndOutPointsByCompanyName(Token);

                try
                {
                    CustomCountsDTO CountsDTO = new CustomCountsDTO();

                    List<CounterStatePerMinutes> CounterStatePerMinutes = new List<CounterStatePerMinutes>();
                    List<List<Points>> OrderedListOfPoints = listOfPoints.GroupBy(a => a.SerialNumber).Select(g => g.ToList()).ToList();



                    foreach (List<Points> InAndOutPoints in OrderedListOfPoints)
                    {
                        long PointIn = InAndOutPoints.Where(a => a.PortNumber == 1).Select(b => b.Id).FirstOrDefault();
                        long PointOut = InAndOutPoints.Where(a => a.PortNumber == 2).Select(b => b.Id).FirstOrDefault();

                        long divider = InAndOutPoints.Where(a => a.PortNumber == 1).Select(d => d.Divider).FirstOrDefault();

                        List<long> ids = InAndOutPoints.Select(a => a.Id).ToList();
                        CounterStatePerMinutes = _pointService.GetCounterStatePerMinutes(Token, ids, DateFrom, DateTo);

                        foreach (CounterStatePerMinutes counterStatePerMinutes in CounterStatePerMinutes)
                        {
                            if (counterStatePerMinutes.PointId == PointIn)
                            {
                                if (divider == 2)
                                {
                                    CountsDTO.Amount = counterStatePerMinutes.Count/2;
                                }
                                else
                                {
                                    CountsDTO.Amount = counterStatePerMinutes.Count;
                                }
                                CountsDTO.DateTime = counterStatePerMinutes.Date;
                                CountsDTO.StoreName = InAndOutPoints.FirstOrDefault().PointFather.Name;
                                CountsDTO.InOrOut = "In";
                            }
                            if (counterStatePerMinutes.PointId == PointOut)
                            {
                                CountsDTO.DateTime = counterStatePerMinutes.Date;
                                CountsDTO.StoreName = InAndOutPoints.FirstOrDefault().PointFather.Name;
                                CountsDTO.InOrOut = "Out";
                                CountsDTO.Amount = counterStatePerMinutes.Count;
                            }
                            CountsDTOList.Add(CountsDTO);
                            CountsDTO = new CustomCountsDTO();
                        }
                    }
                    /*
                */
                }
                catch (Exception)
                {
                }
                return Json(new { CustomCountsDTO = CountsDTOList });
            }
        }

        [HttpGet]
        [Route("KaltenkirchenCustom")]
        public async Task<ContentResult> GetCustomCountsViewKaltenkirchen()
        {
            string response = @"<meta http-equiv=""refresh"" content=""5""><html><body>
                                <style>
                                table, th, td { 
                                font-size:x-large;
                                border: 1px solid black; 
                                border-collapse: collapse;
                                }
                                td {
                                text-align: center;
                                vertical-align: middle;
                                }
                                .center {
                                margin-left: auto;
                                margin-right: auto;
                                }
                                </style>

                                <table 
                                border: 1px solid black; class=""center"" style =""width: 100 % "">";
            response += @"<tr>
                                    <th> Name </ th >
                                    <th> Aktuelle Kunden Anzahl </th>
                                    <th> Maximale Kunden Anzahl </th>
                                    <th> Kunden am Tag </th>
                                    <th> Error [%] </th>
                                </tr> ";

            #region Modehaus Kaltenkirchen

            CountsDbDto countsDbDto = new CountsDbDto()
            {
                Company = "Dodenhof",
                SerialNumber = "b827eb43d296",
                PortIn = 200,
                PortOut = 201
            };

            response += " <tr> <td>Modehaus Kaltenkirchen </td>";
            var countsModehausKaltenkirchen = await _countsDBService.GetCountsByCompanyNameService_DB(countsDbDto);
            var countsPerDayModehausKaltenkirchen = await _countsDBService.GetCountsByCompanyNameServicePerDayINplusOUT_DB(countsDbDto);
            decimal errorModehausKaltenkirchen = 0;
            if ((decimal)countsPerDayModehausKaltenkirchen.Amount != 0)
                errorModehausKaltenkirchen = (decimal)countsModehausKaltenkirchen.Amount / (decimal)countsPerDayModehausKaltenkirchen.Amount;

            response += "<td>" + countsModehausKaltenkirchen.Amount.ToString() + "</td>";
            response += " <td>540</td>";
            response += " <td>" + countsPerDayModehausKaltenkirchen.Amount.ToString() + "</td>";
            response += " <td>" + String.Format("{0:0.##}", errorModehausKaltenkirchen) + "</td></tr>";

            #endregion

            response += " </tr>";

            return Content(response, "text/html");

        }

        [HttpGet]
        [Route("DodenhofCustom")]
        public async Task<ContentResult> GetCustomCountsViewDodenhof()
        {
            string response = @"<meta http-equiv=""refresh"" content=""5""><html><body>
                                <style>
                                table, th, td { 
                                font-size:x-large;
                                border: 1px solid black; 
                                border-collapse: collapse;
                                }
                                td {
                                text-align: center;
                                vertical-align: middle;
                                }
                                .center {
                                margin-left: auto;
                                margin-right: auto;
                                }
                                </style>

                                <table 
                                border: 1px solid black; class=""center"" style =""width: 100 % "">";
            response += @"<tr>
                                    <th> Name </ th >
                                    <th> Aktuelle Kunden Anzahl </th>
                                    <th> Maximale Kunden Anzahl </th>
                                    <th> Kunden am Tag </th>
                                    <th> Error [%] </th>
                                </tr> ";

            #region Modehaus
            CountsDbDto countsDbDtoModehaus = new CountsDbDto()
            {
                Company = "Dodenhof",
                SerialNumber = "b827eb9c7e76",
                PortIn = 155,
                PortOut = 156
            };

            response += "<tr><td>Modehaus</td>";
            var countsModehaus = await _countsDBService.GetRealCountsByCompanyName(countsDbDtoModehaus);
            var countsPerDayModehaus = await _countsDBService.GetCountsByCompanyNameServicePerDayINplusOUT_DB(countsDbDtoModehaus);
            decimal errorModehaus = 0;
            if ((decimal)countsPerDayModehaus.Amount != 0)
                errorModehaus = (decimal)countsModehaus.Amount / (decimal)countsPerDayModehaus.Amount;

            response += "<td>" + countsModehaus.Amount.ToString() + "</td>";
            response += " <td>833</td>";
            response += " <td>" + countsPerDayModehaus.Amount.ToString() + "</td>";
            response += " <td>" + String.Format("{0:0.##}", errorModehaus) + "</td></tr> </tr>";

            #endregion

            #region Sport
            CountsDbDto countsDbDtoSport = new CountsDbDto()
            {
                Company = "Dodenhof",
                SerialNumber = "b827eb4e00d9",
                PortIn = 38,
                PortOut = 39
            };

            response += "<tr><td>Sport</td>";
            var countsSport = await _countsDBService.GetRealCountsByCompanyName(countsDbDtoSport);
            var countsPerDaySport = await _countsDBService.GetCountsByCompanyNameServicePerDayINplusOUT_DB(countsDbDtoSport);
            decimal errorSport = 0;
            if ((decimal)countsPerDaySport.Amount != 0)
                errorSport = (decimal)countsSport.Amount / (decimal)countsPerDaySport.Amount;

            response += "<td>" + countsSport.Amount.ToString() + "</td>";
            response += " <td>283</td>";
            response += " <td>" + countsPerDaySport.Amount.ToString() + "</td> ";
            response += " <td>" + String.Format("{0:0.##}", errorSport) + "</td></tr> </tr>";
            #endregion

            #region D-Strict

            CountsDbDto countsDbDtoDStrict = new CountsDbDto()
            {
                Company = "Dodenhof",
                SerialNumber = "b827eb28c2bf",
                PortIn = 83,
                PortOut = 84
            };
            response += "<tr><td>D-Strict</td>";
            var countsDStrict = await _countsDBService.GetRealCountsByCompanyName(countsDbDtoDStrict);
            var countsPerDayDStrict = await _countsDBService.GetCountsByCompanyNameServicePerDayINplusOUT_DB(countsDbDtoDStrict);
            decimal errorDStrict = 0;
            if ((decimal)countsPerDayDStrict.Amount != 0)
                errorDStrict = (decimal)countsDStrict.Amount / (decimal)countsPerDayDStrict.Amount;

            response += "<td>" + countsDStrict.Amount.ToString() + "</td>";
            response += " <td>57</td>";
            response += " <td>" + countsPerDayDStrict.Amount.ToString() + "</td> ";
            response += " <td>" + String.Format("{0:0.##}", errorDStrict) + "</td></tr> </tr>";
            #endregion

            #region Outlet
            CountsDbDto countsDbDtoOutlet = new CountsDbDto()
            {
                Company = "Dodenhof",
                SerialNumber = "b827eb14fbf9",
                PortIn = 74,
                PortOut = 75
            };

            response += "<tr><td>Outlet</td>";
            var countsOutlet = await _countsDBService.GetRealCountsByCompanyName(countsDbDtoOutlet);
            var countsPerDayOutlet = await _countsDBService.GetCountsByCompanyNameServicePerDayINplusOUT_DB(countsDbDtoOutlet);
            decimal errorOutlet = 0;
            if ((decimal)countsPerDayOutlet.Amount != 0)
                errorOutlet = (decimal)countsOutlet.Amount / (decimal)countsPerDayOutlet.Amount;

            response += "<td>" + countsOutlet.Amount.ToString() + "</td>";
            response += " <td>100</td>";
            response += " <td>" + countsPerDayOutlet.Amount.ToString() + "</td> ";
            response += " <td>" + String.Format("{0:0.##}", errorOutlet) + "</td></tr> </tr>";
            #endregion

            response += " </tr>";

            return Content(response, "text/html");
        }

        [HttpGet(ApiRoutes.Counts.counts + "/{companyName}")]
        public async Task<IActionResult> GetCountsByCompanyName(string companyName)
        {

            var counts = await _countsService.GetCountsByCompanyNameService(companyName);
            if (counts == null)
            {
                return NotFound();
            }
            return Json(counts.Amount);
        }

        [HttpPost(ApiRoutes.Counts.counts)]
        public async Task<IActionResult> CreateCounts([FromBody] DeviceModel device)
        {
            await _countsService.AddCountsToOnescreen(device.SerialNumber, device.InOutMark);
            return Created($"counts/{device}", null);
        }

        [HttpDelete(ApiRoutes.Counts.counts)]
        public async Task<IActionResult> DeleteAllCounts([FromBody] DeviceModel device)
        {
            await _countsService.DeleteAllCountsWithOneScreen(device.SerialNumber);
            return NoContent();
        }

    }


}

