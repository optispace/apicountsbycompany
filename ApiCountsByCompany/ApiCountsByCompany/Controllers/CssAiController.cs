﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Contracts;
using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain.CssAi;
using ApiCountsByCompany.Infrastructure.Commands.CssAi;
using ApiCountsByCompany.Infrastructure.Service;
using ApiCountsByCompany.Infrastructure.Service.CssAiGroup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Controllers
{
    [Route("[controller]")]
    public class CssAiController : Controller
    {
        private readonly ICssAiService _cssAiService;

        public CssAiController(ICssAiService cssAiService)
        {
            _cssAiService = cssAiService;
        }

        [HttpPost(ApiRoutes.CssAi.PostCounts)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PostCounts([FromBody] List<Counts> countsCommand)
        {
            var response =  _cssAiService.AddCounts(countsCommand);
            return Created($"[CssAi]Counts/{response}", null);
        }

        [HttpPost(ApiRoutes.CssAi.PostLiveSignals)]
        public async Task<IActionResult> PostLiveSignals([FromBody] List<LiveSignals> liveSignalsCommand)
        {
            var response =  _cssAiService.AddLiveSignals(liveSignalsCommand);
            return Created($"[CssAi]LiveSignals/{response}", null);
        }
    }
}
