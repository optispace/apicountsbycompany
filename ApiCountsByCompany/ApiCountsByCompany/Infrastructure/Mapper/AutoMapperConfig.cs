﻿using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Domain.Model;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.DTO.Shop;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Mapper
{
    public class AutoMapperConfig
    {
    public static IMapper Initialize()
    => new MapperConfiguration(cfg =>
    {
        cfg.CreateMap<Company, ComapnyDto>();
        cfg.CreateMap<CountsModel, CountsDto>();
        cfg.CreateMap<CountsPerDay, CountsPerDayDto>();
        cfg.CreateMap<ShopGroups, ShopDetialDto>();
        cfg.CreateMap<PortsModel, PortsDetialsDto>()
            .ForMember(dest => dest.PortIn, act => act.MapFrom(src => src.PortOne_Eye))
            .ForMember(dest => dest.PortOut, act => act.MapFrom(src => src.PortTwo_Eye)); 

        //Database
        cfg.CreateMap<CountModel, CounterStatePerMinutes>()
        .ForMember(dest => dest.PointId, act => act.MapFrom(src => src.Pointid))
        .ForMember(dest => dest.Date, act => act.MapFrom(src => new DateTime(src.Year, src.Month, src.Day, src.Hour, src.Minutes, src.Seconds)))
        .ForMember(dest => dest.Count, act => act.MapFrom(src => src.Count))
        .ForMember(dest => dest.PersonHeight, act => act.MapFrom(src => src.personHeight));


        cfg.CreateMap<LiveSignalModel,LiveSignals>()
        .ForMember(dest => dest.DateFrom, act => act.MapFrom(src => src.DateTimeFrom))
        .ForMember(dest => dest.DateTo, act => act.MapFrom(src => src.DateTimeTo))
        .ForMember(dest =>dest.Live, act => act.MapFrom(src => src.Life))
        .ForMember(dest => dest.Type, act => act.MapFrom(src => src.Type));

        cfg.CreateMap<LiveSignalVpn, LiveSignalVpns>()
        .ForMember(dest => dest.DateFrom, act => act.MapFrom(src => src.DateFrom))
        .ForMember(dest => dest.DateTo, act => act.MapFrom(src => src.DateTo))
        .ForMember(dest => dest.Live, act => act.MapFrom(src => src.Live))
        .ForMember(dest => dest.ip, act => act.MapFrom(src => src.Ip))
        .ForMember(dest => dest.Live, act => act.MapFrom(src => src.PointId));
    })
    .CreateMapper();
    }
}

