﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Mapper.Dictionary
{
    public class DictionaryCompany : IDictionaryCompany
    {
        public Dictionary<string, string> GetDirectory()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            foreach (string line in File.ReadLines(Common.Constants.PathToFileWthNameCompanies))
            {
                string[] keyvalue = line.Split(Common.Constants.SeparatorInTextFileWithCounts);
                if (keyvalue.Length == 2)
                {
                    dictionary.Add(keyvalue[0].ToString(), keyvalue[1].ToString());
                }
            }
            return dictionary;
        }

    }
}
