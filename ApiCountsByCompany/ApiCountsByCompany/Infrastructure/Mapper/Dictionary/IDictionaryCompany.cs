﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Mapper.Dictionary
{
    public interface IDictionaryCompany : IDictionaryMarker
    {
        Dictionary<string, string> GetDirectory();
    }
}
