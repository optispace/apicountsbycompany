﻿using ApiCountsByCompany.Infrastructure.Mapper.Dictionary;
using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.IoC.Modules
{
    public class DictionaryModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(ServiceModule)
                .GetTypeInfo()
                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                   .Where(x => x.IsAssignableTo<IDictionaryMarker>())
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }
    }
}
