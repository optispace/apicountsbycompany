﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Core.Repository;
using Autofac;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Graylog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.IoC.Modules
{
    public class GrayLogModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var log = new LoggerConfiguration()
            .WriteTo.Console(outputTemplate:
             "[{Timestamp:dd-MM HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}")
             .WriteTo.Graylog(new GraylogSinkOptions
             {
                 MinimumLogEventLevel = Constants.minimumLogEventLevelGrayLog,
                 HostnameOrAddress = Constants.IpServerGrayLog,
                 Port = Constants.PortGrayLog,
             })
             .Enrich.WithProperty(Constants.NameStreamGrayLog, Constants.NameStreamGrayLog)
            .CreateLogger();
            Log.Logger = log;
            log.Information("START SERVER");
        }
    }
}
