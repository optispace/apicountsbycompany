﻿using ApiCountsByCompany.Infrastructure.IoC.Modules;
using ApiCountsByCompany.Infrastructure.Mapper;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using Autofac;
using Microsoft.Extensions.Configuration;

namespace ApiCountsByCompany.Infrastructure.IoC
{
    public class ContainerModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;

        public ContainerModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(AutoMapperConfig.Initialize())
                .SingleInstance();
            builder.RegisterModule<RepositoryModule>();
            builder.RegisterModule<ServiceModule>();
            builder.RegisterModule<DictionaryModule>();
            builder.RegisterType<OptiSpaceHistoryContext>().SingleInstance(); ;
            builder.RegisterModule<GrayLogModule>();


        }

     
    }
}
