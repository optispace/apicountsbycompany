﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.Commands.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public class SenderService : ISenderSevice
    {
        private readonly ISenderRepository _senderRepository;

        public SenderService(ISenderRepository senderRepository)
        {
            _senderRepository = senderRepository;
        }

        public async Task<AddCountsDbResponse> AddCountsListDb(List<CountModel> countsDtos)
            => await _senderRepository.AddCountsListDBAsync(countsDtos);

        public async Task<AddCountsDbResponse> AddCountsToDb(CountModel countModel)
            => await _senderRepository.AddCountsToDbAsync(countModel);

        public async Task<AddCountsFileResponse> AddCountsListToFile(List<CountModel> countsDtos)
             => await _senderRepository.AddCountsListFileAsync(countsDtos);
        
        public async Task<AddCountsFileResponse> AddCountsToFile(CountModel countModel)
             => await _senderRepository.AddCountsToFileAsync(countModel);

        public async Task<LifeSignalDbResponse> LifeSignalToDb(LiveSignalModel lifeSignalModel)
            => await _senderRepository.LifeSignalAsync(lifeSignalModel);
        
        public async Task<LifeSignalDbResponse> LifeSignalToListDb(List<LiveSignalModel> lifeSignalModelList)
            => await _senderRepository.LifeSignalToListDbAsync(lifeSignalModelList);

        public async Task<LifeSignalDbResponse> AddLifeSignalVpn(CreateLiveSignalVpn createLiveSignalVpn)
            => await _senderRepository.LifeSignalVpnAsync(createLiveSignalVpn);


    }
}
