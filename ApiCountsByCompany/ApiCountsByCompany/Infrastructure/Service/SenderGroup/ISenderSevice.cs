﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Infrastructure.Commands.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public interface ISenderSevice : IService
    {
        //sender
        Task<AddCountsFileResponse> AddCountsListToFile(List<CountModel> content);
        Task<AddCountsDbResponse> AddCountsListDb(List<CountModel> content);
        Task<AddCountsFileResponse> AddCountsToFile(CountModel countModel);
        Task<AddCountsDbResponse> AddCountsToDb(CountModel countModel);
        Task<LifeSignalDbResponse> LifeSignalToDb(LiveSignalModel lifeSignalModel);
        Task<LifeSignalDbResponse> AddLifeSignalVpn(CreateLiveSignalVpn createLiveSignalVpn);
        Task<LifeSignalDbResponse> LifeSignalToListDb(List<LiveSignalModel> lifeSignalModelList);

    }
}
