﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Extensions;
using ApiCountsByCompany.Infrastructure.Mapper.Dictionary;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public class PointService : IPointService
    {
        private readonly IShopRepository _shopRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IMapper _mapper;
        private readonly IPointRepository _pointRepository;

        public PointService(IPointRepository pointRepository, IShopRepository shopRepository, IMapper mapper)
        {
            _mapper = mapper;
            _pointRepository = pointRepository;
            _shopRepository = shopRepository;
        }

        public async Task<PortsDetialsDto> GetPointsByShops(OptiSpaceContext contextOptispace, long groupId)
        {
            var allShops = await _shopRepository.GetAllShopsByGroupIdAsync(contextOptispace, groupId);
            var portsDetialsDtoIn = await _pointRepository.GetPointsIdbyShopIdAsync(contextOptispace, allShops,1);
            var portsDetialsDtoOut = await _pointRepository.GetPointsIdbyShopIdAsync(contextOptispace, allShops, 2);
            return PortsDetialsDto.Create(portsDetialsDtoIn, portsDetialsDtoOut);
        }



        public  List<Points> GetAllInAndOutPointsByCompanyName(string companyName)
        {
            var points =  _pointRepository.GetPointsByCompanyName(companyName);
            return points;
        }

        public async Task<ComapnyDto> GetCompanyNameBySerialNumber(string serialNumber)
        {
            var company = await _companyRepository.GetCompanyNameBySerialNumber(Common.Coder.Decrypt(serialNumber));

            return _mapper.Map<Company, ComapnyDto>(company);
        }

        public List<CounterStatePerMinutes> GetCounterStatePerMinutes(string companyName,List<long> pointsIds, DateTime from, DateTime to)
        {
            var points = _pointRepository.GetCounterStatePerMinutes(companyName,pointsIds, from, to);
            return points;
        }


    }
}
