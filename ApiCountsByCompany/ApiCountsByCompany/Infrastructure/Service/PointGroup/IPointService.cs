﻿using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public interface IPointService :IService
    {
        Task<PortsDetialsDto> GetPointsByShops(OptiSpaceContext contextOptispace, long groupId);
        Task<ComapnyDto> GetCompanyNameBySerialNumber(string serialNumber);
        List<Points> GetAllInAndOutPointsByCompanyName(string companyName);
        List<CounterStatePerMinutes> GetCounterStatePerMinutes(string companyName,List<long> pointsIds, DateTime from, DateTime to);
    }
}
