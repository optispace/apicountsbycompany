﻿using ApiCountsByCompany.Infrastructure.DTO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public interface ICompanyService :IService
    {
        Task<ComapnyDto> GetCompanyNameBySerialNumber(string serialNumber);
    }
}
