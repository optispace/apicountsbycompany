﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Mapper.Dictionary;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IMapper _mapper;

        public CompanyService(ICompanyRepository deviceRepository, IMapper mapper)
        {
            _companyRepository = deviceRepository;
            _mapper = mapper;
        }

        public async Task<ComapnyDto> GetCompanyNameBySerialNumber(string serialNumber)
        {
            var company = await _companyRepository.GetCompanyNameBySerialNumber(Common.Coder.Decrypt(serialNumber));

            return _mapper.Map<Company, ComapnyDto>(company);
        }
    }
}
