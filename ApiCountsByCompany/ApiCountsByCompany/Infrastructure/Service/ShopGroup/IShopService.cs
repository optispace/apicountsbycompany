﻿using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.DTO.Shop;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service.ShopGroup
{
    public interface IShopService : IService 
    {
        Task<IEnumerable<long>> getShopsIdbyGroupId(OptiSpaceContext contextOptispace, long groupId);
    }
}
