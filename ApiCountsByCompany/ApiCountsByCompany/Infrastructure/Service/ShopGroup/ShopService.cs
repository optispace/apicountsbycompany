﻿using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.DTO.Shop;
using ApiCountsByCompany.Infrastructure.Extensions;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service.ShopGroup
{
    public class ShopService : IShopService
    {
        private readonly IShopRepository _shopRepository;
        private readonly IPointService _pointService;
        private readonly IMapper _mapper;
        public ShopService(IShopRepository shopRepository, IPointService pointService, IMapper mapper)
        {
            _shopRepository = shopRepository;
            _pointService = pointService;
            _mapper = mapper;
        }

        public async Task<IEnumerable<long>> getShopsIdbyGroupId(OptiSpaceContext contextOptispace, long groupId) 
            => await _shopRepository.GetAllShopsByGroupIdAsync(contextOptispace, groupId);

        

    }
}
