﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public interface ICountsService : IService
    {
        Task<CountsDto> GetCountsByCompanyNameService(string companyName);
        Task AddCountsToOnescreen(string serialNumber, string InOut);
        Task DeleteAllCountsWithOneScreen(string hashCode);
    }
}
