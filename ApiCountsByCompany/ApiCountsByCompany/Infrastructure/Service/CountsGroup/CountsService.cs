﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Domain.Model;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Mapper.Dictionary;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public class CountsService : ICountsService
    {
        private readonly ICountsRepository _countsRepository;
        private readonly IDictionaryCompany _dictionaryCompany;
        private readonly IMapper _mapper;

        public CountsService(ICountsRepository countsRepository, IDictionaryCompany dictionaryCompany, IMapper mapper)
        {
            _countsRepository = countsRepository;
            _dictionaryCompany = dictionaryCompany;
            _mapper = mapper;
        }

        public async Task<CountsDto> GetCountsByCompanyNameService(string company)
        {
            var counts = await _countsRepository.GetCountsByCompanyNameService(_dictionaryCompany.GetDirectory()[company]);
            return _mapper.Map<CountsModel, CountsDto>(counts);
        }

        public async Task AddCountsToOnescreen(string serialNumber, string InOut)
        {
            await _countsRepository.AddCountsAsync(Common.Coder.Decrypt(serialNumber), InOut);
        }

        public async Task DeleteAllCountsWithOneScreen(string hashCode)
        {
            await _countsRepository.DeleteAllCountsAsync(Common.Helper.SeparateSerialNumber(hashCode));
        }

    }
}
