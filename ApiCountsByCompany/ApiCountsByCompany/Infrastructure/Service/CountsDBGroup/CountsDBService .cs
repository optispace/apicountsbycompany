﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Domain.Model;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.DTO.CountsDb;
using ApiCountsByCompany.Infrastructure.DTO.Shop;
using ApiCountsByCompany.Infrastructure.Extensions;
using ApiCountsByCompany.Infrastructure.Mapper.Dictionary;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.ConnectionToDb;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using ApiCountsByCompany.Infrastructure.Service.ShopGroup;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public class CountsDBService : ICountsDBService
    {
        private readonly ICountsRepository _countsRepository;
        private readonly IDictionaryCompany _dictionaryCompany;
        private readonly IMapper _mapper;
        private readonly IPointService _pointService;
        private readonly IShopService _shopService;
        private readonly IContextDb _contextDb;

        public CountsDBService(ICountsRepository countsRepository, IDictionaryCompany dictionaryCompany,
            IPointService pointService, IShopService shopService, IMapper mapper, IContextDb contextDb)
        {
            _countsRepository = countsRepository;
            _dictionaryCompany = dictionaryCompany;
            _shopService = shopService;
            _mapper = mapper;
            _pointService = pointService;
            _contextDb = contextDb;
        }

        public async Task<ICollection<CountsSumInOutPerDay>> GetSummarryCountsByGroupIdPerDay(string apiKey, DateTime dateFrom, DateTime dateTo, long groupId)
        {
            ICollection<CountsSumInOutPerDay> listcountsPerDay = new Collection<CountsSumInOutPerDay>();
            
            string companyName = apiKey;
            var contextOptispace = _contextDb.SetOptiSpaceContext(companyName);
            var contextOptispaceHistory = _contextDb.SetOptiSpaceHistoryContext(companyName);
            var getPointsByShops = await _pointService.GetPointsByShops(contextOptispace, groupId);

            for (var day = dateFrom.Date; day <= dateTo.Date; day = day.AddDays(1))
            {
                DateTime dateTomorrow = day.AddDays(1).AddSeconds(-1);
                var countIn = await GetCountsByPortAsync(contextOptispaceHistory, day, dateTomorrow, getPointsByShops.PortIn);
                var countOut = await GetCountsByPortAsync(contextOptispaceHistory, day, dateTomorrow, getPointsByShops.PortOut);
                var resultDay = CountsSumInOutPerDay.Create(day.ToShortDateString(), countIn, countOut);
                listcountsPerDay.Add(resultDay);
            }
            return listcountsPerDay;
        }

        public async Task<long> GetCountsByPortAsync(OptiSpaceHistoryContext optiSpaceHistoryContext, DateTime dateForm, DateTime dateTo, List<long> arrayPort)
            => await _countsRepository.GetCountsByPortAsync(optiSpaceHistoryContext, dateForm, dateTo, arrayPort);

        public async Task<CountsDto> GetCountsByCompanyNameService_DB(CountsDbDto getCountsDbDto)
        {
            var counts = await _countsRepository.GetCountsByCompanyNameService_DB(getCountsDbDto);
            return _mapper.Map<CountsModel, CountsDto>(counts);
        }

        public async Task<CountsDto> GetCountsByCompanyNameServicePerDayINplusOUT_DB(CountsDbDto getCountsDbDto)
        {
            var counts = await _countsRepository.GetCountsByCompanyNamePerDayINplusOUTService_DB(getCountsDbDto);
            return _mapper.Map<CountsModel, CountsDto>(counts);
        }

        //AddCountsToOnescreen_DB --> Korzystam z metody która została napisana w SenderController

        public async Task DeleteAllCountsWithOneScreen_DB(CountsDbDto countsDbDto)
        {
            await _countsRepository.DeleteAllCountsAsync_DB(countsDbDto);
        }

        public async Task<CountsDto> GetRealCountsByCompanyName(CountsDbDto getCountsDbDto)
        {
            var counts = await _countsRepository.GetRealCountsByCompanyName(getCountsDbDto);
            return _mapper.Map<CountsModel, CountsDto>(counts);
        }


    }
}
