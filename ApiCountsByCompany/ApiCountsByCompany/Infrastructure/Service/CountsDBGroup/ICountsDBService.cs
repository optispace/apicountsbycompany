﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.DTO.CountsDb;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public interface ICountsDBService : IService
    {
        Task<ICollection<CountsSumInOutPerDay>> GetSummarryCountsByGroupIdPerDay(string apiKey, DateTime dateForm, DateTime dateTo, long groupId);

        Task<long> GetCountsByPortAsync(OptiSpaceHistoryContext optiSpaceHistoryContext, DateTime dateForm, DateTime dateTo, List<long> arrayPort);
        Task<CountsDto> GetCountsByCompanyNameService_DB(CountsDbDto getCountsDbDto);

        Task<CountsDto> GetCountsByCompanyNameServicePerDayINplusOUT_DB(CountsDbDto getCountsDbDto);

        Task<CountsDto> GetRealCountsByCompanyName(CountsDbDto getCountsDbDto);

        Task DeleteAllCountsWithOneScreen_DB(CountsDbDto countsDbDto);

        
    }
}
