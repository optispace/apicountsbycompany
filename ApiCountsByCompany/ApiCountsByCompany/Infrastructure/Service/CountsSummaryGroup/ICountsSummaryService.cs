﻿using ApiCountsByCompany.Infrastructure.Commands.ReportManager.query;
using ApiCountsByCompany.Infrastructure.Commands.Summary;
using ApiCountsByCompany.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public interface ICountsSummaryService : IService
    {
        Task<IEnumerable<CountsPerDayDto>> GetCountsDayPeriod(DayPeriodQuery dayPeriodQuery);
        Task<IEnumerable<CountsPerDayDto>> GetCountsDayPerHourPeriod(DayPeriodQuery dayPeriodQuery);
    }
}
