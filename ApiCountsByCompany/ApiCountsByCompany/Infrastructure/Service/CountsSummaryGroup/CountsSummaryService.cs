﻿using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.Commands.ReportManager.query;
using ApiCountsByCompany.Infrastructure.Commands.Summary;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Extensions;
using ApiCountsByCompany.Infrastructure.Service.ShopGroup;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public class CountsSummaryService : ICountsSummaryService
    {
        private readonly ISummaryCountsRepository _countsSummaryRepository;
        private readonly IShopService _shopService;
        private readonly IMapper _mapper;

        public CountsSummaryService(ISummaryCountsRepository countsSummaryRepository, IShopService shopService, IMapper mapper)
        {
            _countsSummaryRepository = countsSummaryRepository;
            _shopService = shopService;
            _mapper = mapper;
        }
        public async Task<IEnumerable<CountsPerDayDto>> GetCountsDayPeriod(DayPeriodQuery dayPeriodQuery)
        {
            dayPeriodQuery.apiKey = dayPeriodQuery.apiKey.Decrypt();
            var @events = await _countsSummaryRepository.GetCountsPerDayAsync(dayPeriodQuery);
            return _mapper.Map<IEnumerable<CountsPerDayDto>>(@events);
        }

        public async Task<IEnumerable<CountsPerDayDto>> GetCountsDayPerHourPeriod(DayPeriodQuery dayPeriodQuery)
        {
            dayPeriodQuery.apiKey = dayPeriodQuery.apiKey.Decrypt();
            var @events = await _countsSummaryRepository.GetCountsPerDayPerHourAsync(dayPeriodQuery);
            return _mapper.Map<IEnumerable<CountsPerDayDto>>(@events);
        }

    }
}
