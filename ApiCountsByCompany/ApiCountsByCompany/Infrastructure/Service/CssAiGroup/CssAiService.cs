﻿using ApiCountsByCompany.Core.Domain.CssAi;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service.CssAiGroup
{
    public class CssAiService : ICssAiService
    {
        private readonly ICssAiRepository _cssAiRepository;
        public CssAiService(ICssAiRepository cssAiRepository)
        {
            _cssAiRepository = cssAiRepository;
        }
        public async Task AddCounts(List<Counts> counts)
            => await _cssAiRepository.AddCounts(counts);
        

        public async Task AddLiveSignals(List<LiveSignals> liveSignals)
            => await _cssAiRepository.AddLiveSignals(liveSignals);
    }
}
