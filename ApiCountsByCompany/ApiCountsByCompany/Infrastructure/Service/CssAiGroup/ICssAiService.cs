﻿using ApiCountsByCompany.Core.Domain.CssAi;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service.CssAiGroup
{
    public interface ICssAiService : IService
    {
        Task AddCounts(List<Counts> counts);
        Task AddLiveSignals(List<LiveSignals> liveSignals);
    }
}
