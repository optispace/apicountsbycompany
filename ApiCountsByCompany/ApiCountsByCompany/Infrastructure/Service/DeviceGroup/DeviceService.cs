﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Controllers;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Service
{
    public class DeviceService : IDeviceService
    {

        private readonly IDeviceRepository _deviceRepository;
        private static readonly HttpClient client = new HttpClient();

        public DeviceService(IDeviceRepository deviceRepository)
        {
            _deviceRepository = deviceRepository;
        }


        public async Task<AddDeviceResponse> AddDevice(DeviceDTO deviceDTO)
        {
            AddDeviceResponse result = await _deviceRepository.AddDevice(deviceDTO);
            return result;
        }
        public async Task<AddDeviceResponse> RemoveDevice(DeviceDTO deviceDTO)
        {
            AddDeviceResponse result = await _deviceRepository.RemoveDevice(deviceDTO);
            return result;
        }
        public async Task<DeviceInfoDto> GetDetails(string sn, string token)
        {
            return await Task.Run(() => GetResponseFromURI(new Uri("http://opticounter.pl/f642d910-17db-4198-a62-302f9348f8cd/Service.asmx/GetDeviceInfobySerialNumber?serialNumber="+sn+"&token="+token)));
        }

        public async Task<IEnumerable<DeviceDetailsDto>> GetDetailsDto(string companyName)
        {
            var devicesDetials = await _deviceRepository.GetDevices(companyName);
            return devicesDetials;
        }

        private static async Task<DeviceInfoDto> GetResponseFromURI(Uri u)
        {
            var result1 = new DeviceInfoDto();
            using (var client = new HttpClient())
            {
                HttpResponseMessage result = await client.GetAsync(u);
                if (result.IsSuccessStatusCode)
                {
                    var response = await result.Content.ReadAsStringAsync();
                    result1 = JsonConvert.DeserializeObject<DeviceInfoDto>(response);
                }
            }
            return result1;
        }

        public async Task<AddDeviceResponse> AddExtendedDevice(DeviceExtendedDTO deviceDTO)
        {
            AddDeviceResponse result = await _deviceRepository.AddExtendedDevice(deviceDTO);
            return result;
        }
    }
}
