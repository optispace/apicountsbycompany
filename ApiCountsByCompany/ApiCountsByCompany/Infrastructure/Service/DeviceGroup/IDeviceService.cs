﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql;
using ApiCountsByCompany.Infrastructure.Service;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Controllers
{
    public interface IDeviceService : IService
    {
        Task<IEnumerable<DeviceDetailsDto>> GetDetailsDto(string companyName);
        Task<DeviceInfoDto> GetDetails(string sn, string token);
        Task<AddDeviceResponse> AddDevice(DeviceDTO deviceDTO);
        Task<AddDeviceResponse> AddExtendedDevice(DeviceExtendedDTO deviceDTO);
        Task<AddDeviceResponse> RemoveDevice(DeviceDTO deviceDTO);
    }
}