﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Commands
{
    public class RandomCountsQuery
    {
        public string sn { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
