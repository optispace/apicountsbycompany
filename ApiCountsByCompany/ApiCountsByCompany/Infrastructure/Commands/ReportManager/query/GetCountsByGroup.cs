﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Commands.ReportManager.query
{
    public class GetCountsByGroup
    {
        public string apiKey { get; set; }
        public int groupId { get; set; }
        public DateTime dataFrom { get; set; }
        public DateTime dataTo { get; set; }

    }
}
