﻿using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Commands.Summary.query
{
    public class GetCountPerDay
    {
        public OptiSpaceHistoryContext optiSpaceHistoryContext { get; set; }
        public DateTime currentDateTime { get; set; }

    }
}
