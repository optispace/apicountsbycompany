﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Commands.Summary
{
    public class DayPeriodQuery
    {
        public string apiKey { get; set; }
        public string sn { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
