﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Commands.Device
{
    public class CreateLiveSignalVpn
    {
        public string companyName { get; set; }
        public string snApiKey { get; set; }
        public string ip { get; set; }

    }
}
