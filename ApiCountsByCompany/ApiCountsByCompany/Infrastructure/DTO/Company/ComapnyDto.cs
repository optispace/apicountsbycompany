﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO
{
    public class ComapnyDto
    {
        public string Name { get; set; }
    }
}
