﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO
{
    public class PortsDetialsDto
    {
        public List<long> PortIn { get; set; }
        public List<long> PortOut { get; set; }

        protected PortsDetialsDto()
        {
        }

        protected PortsDetialsDto(List<long> portIn, List<long> portOut)
        {
            PortIn = portIn;
            PortOut = portOut;
        }

        public static PortsDetialsDto Create(List<long> portIn, List<long> portOut)
            => new PortsDetialsDto(portIn, portOut);
    }
}
