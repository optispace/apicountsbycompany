﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO.CountsDb
{
    public class CountsInOutDto
    {
        public long CountIn { get; set; }
        public long CountOut { get; set; }

        protected CountsInOutDto()
        {
        }

        protected CountsInOutDto(long countIn, long countOut)
        {
            CountIn = countIn;
            CountOut = countOut;
        }

        public static CountsInOutDto Create(long countIn, long countOut)
            => new CountsInOutDto(countIn, countOut);
    }
}
