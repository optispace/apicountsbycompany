﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO.CountsDb
{
    public class CountsSumInOutPerDay
    {
        public string Day { get; protected set; }
        public long SumIn { get; protected set; }
        public long SumOut { get; protected set; }
    

    protected CountsSumInOutPerDay()
    {
    }

    protected CountsSumInOutPerDay(string day, long sumIn, long sumOut)
    {
        Day = day;
        SumIn = sumIn;
        SumOut = sumOut;
    }

    public static CountsSumInOutPerDay Create(string day, long sumIn, long sumOut)
        => new CountsSumInOutPerDay(day, sumIn, sumOut);
    }
}
