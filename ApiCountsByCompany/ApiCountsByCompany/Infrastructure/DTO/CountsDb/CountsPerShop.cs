﻿using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.DTO.CountsDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Domain
{
    public class CountsPerShop
    {
        public long ShopId { get; set; }
        public CountsInOutDto CountsInandOutDto { get; set; }

        protected CountsPerShop()
        {
        }

        protected CountsPerShop(long shopId, CountsInOutDto countsInandOutDto)
        {
            ShopId = shopId;
            CountsInandOutDto = countsInandOutDto;
        }

        public static CountsPerShop Create(long shopId, CountsInOutDto countsInandOutDto)
            => new CountsPerShop(shopId, countsInandOutDto);
    }
}

