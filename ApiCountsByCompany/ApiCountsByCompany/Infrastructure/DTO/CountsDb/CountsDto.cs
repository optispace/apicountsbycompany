﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO
{
    public class CountsDto
    {
        public int Amount { get; set; }
        public string InOutMark { get; set; }
        
    }
}
