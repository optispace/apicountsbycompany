﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO
{
    public class CountsDbDto
    {
        public string SerialNumber { get; set; }

        public string Company { get; set; }

        public int PortIn { get; set; }
        public int PortOut { get; set; }
        public string OriginalDeviceSerialNumberHash { get; set; }
    }
}
