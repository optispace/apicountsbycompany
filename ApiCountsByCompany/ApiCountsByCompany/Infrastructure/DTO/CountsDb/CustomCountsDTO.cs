﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO
{
    public class CustomCountsDTO
    {
            public DateTime DateTime { get; set; }
            public string StoreName { get; set; }
            public string InOrOut { get; set; }
            public long Amount { get; set; }
    }
}
