﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO
{
    public class CountsPerDayDetailDto
    {
        public DateTime day { get; set; }
        public long countIn { get; set; }
        public string countOut { get; set; }
    }
}
