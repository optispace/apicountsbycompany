﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO
{
    public class CountsPerDayDto
    {
        public string storeName { get; set; }
        public int count { get; set; }
        public string date { get; set; }
    }
}
