﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO.Shop
{
    public class ShopDetialDto
    {
        public long shopId { get; set; }
        public PortsDetialsDto portsDetials { get; set; }
        
    }
}
