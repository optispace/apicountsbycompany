﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO
{
    public class DeviceInfoDto
    {
        public string nameDatabase { get; set; }
        public string namePoint { get; set; }
        public string sn { get; set; }
        public string snEncrypt { get; set; }
        public double[] coordinates { get; set; }
        public long portOne { get; set; }
        public long portTwo { get; set; }
        public long portThree { get; set; }
        public long portFour { get; set; }
        public long portFive { get; set; }
        public long portSix { get; set; }
        public long portSeven { get; set; }
        public long portEight { get; set; }
    }
}
