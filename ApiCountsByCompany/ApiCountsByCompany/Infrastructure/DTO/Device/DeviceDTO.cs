﻿namespace ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql
{
    public class DeviceDTO
    {
        public string serialNumber { get; set; }
        public string database { get; set; }
    }
}