﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.DTO
{
    public class DeviceDetailsDto
    {
        public string serialNumber { get; set; }

        public int PointIn { get; set; }

        public int PointOut { get; set; }
    }
}
