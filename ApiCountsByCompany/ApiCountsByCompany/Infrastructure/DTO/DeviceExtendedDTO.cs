﻿namespace ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql
{
    public class DeviceExtendedDTO
    {
        public string deviceName { get; set; }
        public string serialNumber { get; set; }
        public string database { get; set; }
        public string description { get; set; }
        public string city { get; set; }
        public string address { get; set; }
        public string existingShopName { get; set; }
    }
}