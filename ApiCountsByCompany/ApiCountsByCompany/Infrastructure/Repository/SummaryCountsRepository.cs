﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.Commands.Summary;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.ConnectionToDb;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository
{
    public class SummaryCountsRepository : ISummaryCountsRepository
    {
        private readonly IContextDb _contextDb;
        private readonly IDbManager _dbManager;

        public SummaryCountsRepository(IContextDb contextDb, IDbManager dbManager)
        {
            _contextDb = contextDb;
            _dbManager = dbManager;
        }
        public async Task<IEnumerable<CountsPerDay>> GetCountsPerDayAsync(DayPeriodQuery dayPeriodQuery)
        {

            var contextOptispace = _contextDb.SetOptiSpaceContext(dayPeriodQuery.apiKey);
            var contextOptispaceHistory = _contextDb.SetOptiSpaceHistoryContext(dayPeriodQuery.apiKey);
            int pointId = Convert.ToInt32(contextOptispace.Points.FirstOrDefault(p => p.SerialNumber == dayPeriodQuery.sn && p.PortNumber == 0).ShopId);
            string storeName =  contextOptispace.Shops.FirstOrDefault(p => p.Id == pointId).Name;
            var liststatisticsAggregaterDto = new List<CountsPerDay>();
            var listPointId = new List<int>();
            var listPoint = contextOptispace.Points.Where(p => p.SerialNumber == dayPeriodQuery.sn && p.ShopId != null && p.PortNumber != 0).ToList();
            foreach (var list in listPoint)
            {
                listPointId.Add(Convert.ToInt32(list.Id));
            }

            for (var day = dayPeriodQuery.startDate; day <= dayPeriodQuery.endDate; day = day.AddDays(1))
            {
                var countsPerDay = new CountsPerDay();
                countsPerDay.storeName = storeName;
                countsPerDay.date = day.ToString("yyyy-MM-dd");
                countsPerDay.count = await _dbManager.GetCountsDayByPoints(contextOptispaceHistory, listPointId, day);
                liststatisticsAggregaterDto.Add(countsPerDay);
            }

            return liststatisticsAggregaterDto;
        }

        public async Task<IEnumerable<CountsPerDay>> GetCountsPerDayPerHourAsync(DayPeriodQuery dayPeriodQuery)
        {

            var contextOptispace = _contextDb.SetOptiSpaceContext(dayPeriodQuery.apiKey);
            var contextOptispaceHistory = _contextDb.SetOptiSpaceHistoryContext(dayPeriodQuery.apiKey);
            int pointId = Convert.ToInt32(contextOptispace.Points.FirstOrDefault(p => p.SerialNumber == dayPeriodQuery.sn && p.PortNumber == 0).ShopId);
            string storeName = contextOptispace.Shops.FirstOrDefault(p => p.Id == pointId).Name;
            var liststatisticsAggregaterDto = new List<CountsPerDay>();
            var listPointId = new List<int>();
            var listPoint = contextOptispace.Points.Where(p => p.SerialNumber == dayPeriodQuery.sn && p.ShopId != null && p.PortNumber != 0).ToList();
            foreach (var list in listPoint)
            {
                listPointId.Add(Convert.ToInt32(list.Id));
            }

            for (var day = dayPeriodQuery.startDate; day <= dayPeriodQuery.endDate; day = day.AddDays(1))
            {
                var countsPerDay = new CountsPerDay();
                countsPerDay.storeName = storeName;
                countsPerDay.date = day.ToString("yyyy-MM-dd");
                countsPerDay.count = await _dbManager.GetCountsDayByPoints(contextOptispaceHistory, listPointId, day);
                liststatisticsAggregaterDto.Add(countsPerDay);
            }

            return liststatisticsAggregaterDto;
        }
    }
}
