﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Domain.Model;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.ConnectionToDb;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository
{
    public class CountsRepository : ICountsRepository
    {
        private readonly IFileManager _fileManager;
        private readonly IDbManager _dbManager;
        private readonly IContextDb _contextDb;

        public CountsRepository(IContextDb contextDb, IFileManager fileManager, IDbManager dbManager)
        {
            _fileManager = fileManager;
            _dbManager = dbManager;
            _contextDb = contextDb;
        }

        public async Task<long> GetCountsByPortAsync(OptiSpaceHistoryContext optiSpaceHistoryContext, DateTime dateFrom, DateTime dateTo, List<long> arrayPointId)
            => await Task.FromResult(optiSpaceHistoryContext.CounterStatePerDay
                .Where(x => x.Date >= dateFrom && x.Date <= dateTo && arrayPointId.Contains(x.PointId))
                .Sum(x => x.Count));



        public async Task<CountsModel> GetCountsByCompanyNamePerDayINplusOUTService_DB(CountsDbDto getCountsDbDto)
            => await await Task.FromResult(_dbManager.GetAllCountsPerDay(getCountsDbDto));

        public async Task<CountsModel> GetRealCountsByCompanyName(CountsDbDto countsDbDto)
            => await await Task.FromResult(_dbManager.GetRealCountsByCompanyName(countsDbDto));

        public async Task<CountsModel> GetCountsByCompanyNameService_DB(CountsDbDto getCountsDbDto)
            => await await Task.FromResult(_dbManager.GetCounts(getCountsDbDto));

        public async Task<bool> AddCountsAsync(string serialNumber, string InOut)
            => await await Task.FromResult(_fileManager.IsAddCountsToTxtFileBySerial_Bool(serialNumber, InOut));

        public async Task<bool> DeleteAllCountsAsync(string serialNumber)
            => await await Task.FromResult(_fileManager.IsDeleteAllCountsWithfileTxt(serialNumber));

        public async Task<bool> DeleteAllCountsAsync_DB(CountsDbDto countsDbDto)
            => await await Task.FromResult(_dbManager.DeleteCounts(countsDbDto));

        #region Aqqula Ansbach
        public async Task<CountsModel> GetCountsByCompanyNameService(string companyName)
        {
            if (companyName.Equals("b827eb3ccf5e"))  // Aquella Ansbach
            {
                string resultTemp = await HTTPHelper.GetRequest(Constants.OptimediaAquellaUrl);
                Int32.TryParse(resultTemp, out int intResult);
                return new CountsModel()
                {
                    Amount = intResult,
                };
            }
            if (companyName.Equals("b827eb71b4a8"))  // aquellaansbachsauna
            {
                string resultTemp = await HTTPHelper.GetRequest(Constants.OptimediaAquellaSaunaUrl);
                Int32.TryParse(resultTemp, out int intResult);
                return new CountsModel()
                {
                    Amount = intResult,
                };
            }
            if (companyName.Equals("b827ebb74813"))  // aquellaansbachhallenbad
            {
                string resultTemp = await HTTPHelper.GetRequest(Constants.OptimediaAquellaHallenbadUrl);
                Int32.TryParse(resultTemp, out int intResult);
                return new CountsModel()
                {
                    Amount = intResult,
                };
            }

            return await await Task.FromResult(_fileManager.AbsAmountPeopleInStore(companyName));
        }
        #endregion
    }
}
