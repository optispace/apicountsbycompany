﻿using ApiCountsByCompany.Core.Domain.CssAi;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.ConnectionToDb;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiveSignals = ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel.LiveSignals;

namespace ApiCountsByCompany.Infrastructure.Repository
{
    public class CssAiRepository : ICssAiRepository
    {
        private readonly IContextDb _contextDb;
        private readonly IPointRepository _pointRepository;
        public CssAiRepository(IContextDb contextDb, IPointRepository pointRepository)
        {
            _contextDb = contextDb;
            _pointRepository = pointRepository;
        }

        public async Task AddCounts(List<Counts> counts)
        {
            var context = _contextDb.SetOptiSpaceHistoryContext(counts[0].companyName);
            if ((counts.Count != 0))
            {
                List<CounterStatePerMinutes> counterStatePerMinutesList = counts
                   .Select(x => new CounterStatePerMinutes()
                   {
                       Count = x.countNumber,
                       PersonHeight = x.personHeight,
                       PointId = x.pointId,
                       Date = x.dateTime
                   })
                   .ToList();

                context.CounterStatePerMinutes.AddRange(counterStatePerMinutesList);
                await context.SaveChangesAsync();
            }
        }

        public async Task AddLiveSignals(List<Core.Domain.CssAi.LiveSignals> liveSignals)
        {
            var contextOptispace = _contextDb.SetOptiSpaceContext(liveSignals[0].companyName);
            var context = _contextDb.SetOptiSpaceHistoryContext(liveSignals[0].companyName);
            var ponitId = _pointRepository.GetPointIdBySerialnumber(contextOptispace, liveSignals[0].serialNumber);
            if ((liveSignals.Count != 0))
            {
                List<LiveSignals> liveSignalsList = liveSignals
                   .Select(x => new LiveSignals()
                   {
                       DateFrom = x.dateTimeFrom,
                       DateTo = x.dateTimeTo,
                       Live = Convert.ToBoolean(x.life),
                       Type = x.type,
                       PointId = ponitId
                   })
                   .ToList();

                context.LiveSignals.AddRange(liveSignalsList);
                await context.SaveChangesAsync();
            }
        }
    }
}
