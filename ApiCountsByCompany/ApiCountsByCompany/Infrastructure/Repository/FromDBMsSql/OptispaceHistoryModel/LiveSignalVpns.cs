﻿using System;
using System.Collections.Generic;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel
{
    public partial class LiveSignalVpns
    {
        public long Id { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public bool Live { get; set; }
        public long PointId { get; set; }
        public string ip { get; set; }
    }

}
