﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.OptispaceHistoryModel
{
    [Table("CounterStatePerDay")]
    public partial class CounterStatePerDay
    {
        [Key]
        public long PointId { get; set; }
        public DateTime Date { get; set; }
        public long Count { get; set; }
    }
}
