﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel
{
    public partial class LiveSignals
    {
        public long Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DateFrom { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DateTo { get; set; }
        public bool Live { get; set; }
        public long PointId { get; set; }
        public long Type { get; set; }
    }
}
