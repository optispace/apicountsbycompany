﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel
{
    public partial class RecordingWorkTimes
    {
        public long Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        public long? DatabaseMainUserId { get; set; }
        public long? ShopId { get; set; }
        public bool IsStartWork { get; set; }
    }
}
