﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel
{
    public partial class CounterStatePerHourProccesseds
    {
        public long Id { get; set; }
        public long PointId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        public long Count { get; set; }
    }
}
