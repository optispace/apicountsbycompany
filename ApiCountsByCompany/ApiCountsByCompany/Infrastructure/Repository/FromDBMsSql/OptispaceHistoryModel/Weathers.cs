﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel
{
    public partial class Weathers
    {
        public long Id { get; set; }
        public long? ShopId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DateTime { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Temperature { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Pressure { get; set; }
        public string Description { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Wind { get; set; }
        public string Cloudy { get; set; }
        public string City { get; set; }
        public string Code { get; set; }
    }
}
