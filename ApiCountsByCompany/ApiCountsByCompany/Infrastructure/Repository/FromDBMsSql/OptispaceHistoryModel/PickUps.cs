﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel
{
    public partial class PickUps
    {
        public long Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DateUp { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DateDown { get; set; }
        public long PointId { get; set; }
        public long ProductId { get; set; }
    }
}
