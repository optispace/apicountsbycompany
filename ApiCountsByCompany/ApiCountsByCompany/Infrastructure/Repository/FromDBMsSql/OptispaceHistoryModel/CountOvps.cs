﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel
{
    [Table("CountOVPs")]
    public partial class CountOvps
    {
        public long Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        public string FileName { get; set; }
        public int Port { get; set; }
        public long LiftTime { get; set; }
        public long PickUp { get; set; }
        public long PointId { get; set; }
    }
}
