﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel
{
    public partial class Alarms
    {
        public long Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        public long Port { get; set; }
        public string SerialNumber { get; set; }
        public string CentralName { get; set; }
        public string PortName { get; set; }
        public long RemoteSerialNumberOptical { get; set; }
        public string RemoteNameOptical { get; set; }
        public long RemoteSerialNumberSound { get; set; }
        public string RemoteNameSound { get; set; }
        public long TimeTurnOffSound { get; set; }
        public long TimeTurnOffOptical { get; set; }
        public long ResponseTime { get; set; }
        public string AlarmCode { get; set; }
        public string Destination { get; set; }
    }
}
