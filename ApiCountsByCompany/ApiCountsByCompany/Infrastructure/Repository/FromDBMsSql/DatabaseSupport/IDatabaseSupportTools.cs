﻿using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.DatabaseSupport
{
    public interface IDatabaseSupportTools : IRepository
    {
        #region Database Support - Tools
   
        Task UpdateLastActivtyByPoint(OptiSpaceContext context,Points point);
        Task UpdateLiveSignalByPoint(OptiSpaceHistoryContext context, LiveSignals liveSignals);
        Task UpdateLiveSignalByPointVpn(OptiSpaceHistoryContext context, LiveSignalVpns liveSignalVpns);
        Task AddLiveSignal(OptiSpaceHistoryContext context,long PointId, DateTime dataTimeFrom, DateTime dataTimeTo, bool IsLife,long Type);
        Task AddLiveSignalVpn(OptiSpaceHistoryContext context, long Pointid, DateTime dataTimeFrom, DateTime dataTimeTo, bool IsLife, string ip);
        #endregion
    }
}
