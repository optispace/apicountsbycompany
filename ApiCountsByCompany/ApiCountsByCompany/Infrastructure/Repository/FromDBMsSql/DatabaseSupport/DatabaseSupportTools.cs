﻿using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.DatabaseSupport
{
    public class DatabaseSupportTools : IDatabaseSupportTools
    {
        private readonly IMapper _mapper;

        public DatabaseSupportTools(IMapper mapper)
        {
            _mapper = mapper;
        }

        #region Database Support - Tools
        public async Task UpdateLastActivtyByPoint(OptiSpaceContext context, Points point)
        {
            point.LastActivity = DateTime.Now;
            context.Points.Update(point);
            await context.SaveChangesAsync();
        }

        public async Task UpdateLiveSignalByPoint(OptiSpaceHistoryContext context, LiveSignals liveSignals)
        {
            liveSignals.DateTo = DateTime.Now;
            context.LiveSignals.Update(liveSignals);
            await context.SaveChangesAsync();
        }

        public async Task UpdateLiveSignalByPointVpn(OptiSpaceHistoryContext context, LiveSignalVpns liveSignalVpns)
        {
            liveSignalVpns.DateTo = DateTime.Now;
            context.LiveSignalVpns.Update(liveSignalVpns);
            await context.SaveChangesAsync();
        }

        public async Task AddLiveSignal(OptiSpaceHistoryContext context,long Pointid, DateTime dataTimeFrom, DateTime dataTimeTo, bool IsLife,long Type)
        {
            LiveSignalModel liveSignalsModel = new LiveSignalModel();
            liveSignalsModel.Pointid = Pointid;
            liveSignalsModel.DateTimeFrom = dataTimeFrom;
            liveSignalsModel.DateTimeTo = dataTimeTo;
            liveSignalsModel.Life = IsLife;
            liveSignalsModel.Type = Type;
            LiveSignals liveSignals = _mapper.Map<LiveSignals>(liveSignalsModel);
            context.LiveSignals.Add(liveSignals);
            await context.SaveChangesAsync();
        }

        public async Task AddLiveSignalVpn(OptiSpaceHistoryContext context, long Pointid, DateTime dataTimeFrom, DateTime dataTimeTo, bool IsLife, string ipVpn)
        {
            LiveSignalVpns liveSignalsVpnModel = new LiveSignalVpns();
            liveSignalsVpnModel.PointId = Pointid;
            liveSignalsVpnModel.DateFrom = dataTimeFrom;
            liveSignalsVpnModel.DateTo = dataTimeTo;
            liveSignalsVpnModel.Live = IsLife;
            liveSignalsVpnModel.ip = ipVpn;
            LiveSignalVpns liveSignals = _mapper.Map<LiveSignalVpns>(liveSignalsVpnModel);
            context.LiveSignalVpns.Add(liveSignals);
            await context.SaveChangesAsync();
        }
    }
        #endregion
}
