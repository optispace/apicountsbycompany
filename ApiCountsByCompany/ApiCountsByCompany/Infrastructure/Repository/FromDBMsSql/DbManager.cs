﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Domain.Model;
using ApiCountsByCompany.Extensions;
using ApiCountsByCompany.Infrastructure.Commands.Device;
using ApiCountsByCompany.Infrastructure.Commands.Summary.query;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.ConnectionToDb;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.DatabaseSupport;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;


namespace ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql
{
    public class DbManager : IDbManager
    {
        private readonly IContextDb _contextDb;
        private readonly IMapper _mapper;
        private readonly IDatabaseSupportTools _databaseSupportTools;
        public DbManager(IContextDb contextDb, IMapper mapper, IDatabaseSupportTools databaseSupportTools)
        {
            _contextDb = contextDb;
            _mapper = mapper;
            _databaseSupportTools = databaseSupportTools;
        }

        #region Count
        public async Task<CountsModel> GetCounts(CountsDbDto getCountsDbDto)
        {
            CountsModel CountsModelResponse = new CountsModel();
            int result = 0;
            long counts_in = 0;
            long counts_out = 0;
            DateTime dateTimeNow = DateTime.Now.Date;
            DateTime dateTomorrow = dateTimeNow.Date.AddDays(1);
            try
            {
                var contextOptispace = _contextDb.SetOptiSpaceContext(getCountsDbDto.Company);
                var contextOptispaceHistory = _contextDb.SetOptiSpaceHistoryContext(getCountsDbDto.Company);
                counts_in = contextOptispaceHistory.CounterStatePerMinutes.Where(x => x.Date >= dateTimeNow && x.Date <= dateTomorrow && x.PointId == getCountsDbDto.PortIn).Sum(x => x.Count);
                counts_out = contextOptispaceHistory.CounterStatePerMinutes.Where(x => x.Date >= dateTimeNow && x.Date <= dateTomorrow && x.PointId == getCountsDbDto.PortOut).Sum(x => x.Count);
                result = Convert.ToInt32(Math.Abs(counts_in - counts_out));
            }
            catch (FormatException e)
            {

            }

            return await Task.FromResult (new CountsModel()
            {
                Amount = result,
            });
        }
        public async Task<CountsModel> GetRealCountsByCompanyName(CountsDbDto countsDbDto)
        {
            CountsModel CountsModelResponse = new CountsModel();
            int result = 0;
            long counts_in = 0;
            long counts_out = 0;
            DateTime dateTimeNow = DateTime.Now.Date;
            DateTime dateTomorrow = dateTimeNow.Date.AddDays(1);
            try
            {
                var contextOptispace = _contextDb.SetOptiSpaceContext(countsDbDto.Company);
                var contextOptispaceHistory = _contextDb.SetOptiSpaceHistoryContext(countsDbDto.Company);
                counts_in = contextOptispaceHistory.CounterStatePerMinutes.Where(x => x.Date >= dateTimeNow && x.Date <= dateTomorrow && x.PointId == countsDbDto.PortIn).Sum(x => x.Count);
                counts_out = contextOptispaceHistory.CounterStatePerMinutes.Where(x => x.Date >= dateTimeNow && x.Date <= dateTomorrow && x.PointId == countsDbDto.PortOut).Sum(x => x.Count);
                result = Convert.ToInt32(counts_in - counts_out);
            }
            catch (FormatException e)
            {

            }

            return await Task.FromResult(new CountsModel()
            {
                Amount = result,
            });
        }
        public List<CounterStatePerMinutes> GetCounterStatePerMinutes(string companyName, List<long> pointsIds, DateTime from, DateTime to)
        {
            var contextOptispaceHistory = _contextDb.SetOptiSpaceHistoryContext(companyName);
            List<CounterStatePerMinutes> minutes = new List<CounterStatePerMinutes>();
            List<CounterStatePerMinutes> counterStatePerMinutes = contextOptispaceHistory.CounterStatePerMinutes.OrderByDescending(x => x.Date).Where(x => x.Date >= from && x.Date < to && pointsIds.Contains(x.PointId)).ToList();


            return counterStatePerMinutes;
        }
        public async Task<CountsModel> GetAllCountsPerDay(CountsDbDto getCountsDbDto)
        {
            CountsModel CountsModelResponse = new CountsModel();
            int result = 0;
            long counts_in = 0;
            long counts_out = 0;
            DateTime dateTimeNow = DateTime.Now.Date;
            DateTime dateTomorrow = dateTimeNow.Date.AddDays(1);
            try
            {
                var contextOptispace = _contextDb.SetOptiSpaceContext(getCountsDbDto.Company);
                var contextOptispaceHistory = _contextDb.SetOptiSpaceHistoryContext(getCountsDbDto.Company);
                counts_in = contextOptispaceHistory.CounterStatePerMinutes.Where(x => x.Date >= dateTimeNow && x.Date <= dateTomorrow && x.PointId == getCountsDbDto.PortIn).Sum(x => x.Count);
                counts_out = contextOptispaceHistory.CounterStatePerMinutes.Where(x => x.Date >= dateTimeNow && x.Date <= dateTomorrow && x.PointId == getCountsDbDto.PortOut).Sum(x => x.Count);
                result = Convert.ToInt32(Math.Abs(counts_in + counts_out));
            }
            catch (FormatException e)
            {

            }

            return await Task.FromResult(new CountsModel()
            {
                Amount = result,
            });
        }

        public async Task<int> GetCountsDayByPoints(OptiSpaceHistoryContext optiSpaceHistoryContext,List<int> listPoint, DateTime currentDateTime)
        {
            int result =0;
            List<int> ListCountsPerPorts = new List<int>();
            DateTime dateTimeNow = currentDateTime;
            DateTime dateTomorrow = dateTimeNow.Date.AddDays(1);
            try
            {
                foreach (var point in listPoint)
                {
                    var AmountCountsPerPointId = await Task.FromResult(Convert.ToInt32(optiSpaceHistoryContext.CounterStatePerMinutes.Where(x => x.Date >= dateTimeNow && x.Date <= dateTomorrow && x.PointId == point).Sum(x => x.Count)));
                    ListCountsPerPorts.Add(AmountCountsPerPointId);
                }
                result = ListCountsPerPorts.Sum()/ListCountsPerPorts.Count;
            }
            catch (FormatException e)
            {

            }
            return result;
        }
        public async Task<string> AddCount(CountModel countmodel)
        {
            string result = "";
            string OriginalDeviceSerialNumberDecrypt = Helper.DecryptCoder(countmodel.OriginalDeviceSerialNumber);
            try
            {
                if (OriginalDeviceSerialNumberDecrypt != "")
                {
                    var context = _contextDb.SetOptiSpaceHistoryContext(countmodel.companyName);
                    CounterStatePerMinutes counterStatePerMinutes = _mapper.Map<CounterStatePerMinutes>(countmodel);
                    context.CounterStatePerMinutes.Add(counterStatePerMinutes);
                    await context.SaveChangesAsync();
                    result = Constants.StatusRequestOk;
                }
                else
                {
                    result = Constants.StatusRequestNotEncrypted;
                }

            }
            catch (FormatException e)
            {
                result = e.ToString();
            }

            return result;
        }
        public async Task<AddCountsDbResponse> AddCountList(List<CountModel> countmodelList)
        {
            string result = "";
            string OriginalDeviceSerialNumberDecrypt = "";
            string CompanyName = "";
            int AmountCounts = 0;
            List<CountModel> countModelList_decrypted = new List<CountModel>();

                foreach (var singleCount in countmodelList)
                {
                    OriginalDeviceSerialNumberDecrypt = Helper.DecryptCoder(singleCount.OriginalDeviceSerialNumber);
                    if (OriginalDeviceSerialNumberDecrypt != "")
                    {
                        countModelList_decrypted.Add(singleCount);
                        AmountCounts += singleCount.Count;
                    }
                    else
                    {
                        result = Constants.StatusRequestNotEncrypted;
                    }
                }

                if ((countModelList_decrypted.Count != 0))
                {
                    CompanyName = countModelList_decrypted.FirstOrDefault().companyName;
                    var context = _contextDb.SetOptiSpaceHistoryContext(CompanyName);
                    List<CounterStatePerMinutes> counterStatePerMinutesList = countModelList_decrypted
                       .Select(x => new CounterStatePerMinutes()
                       {
                           Count = x.Count,
                           PersonHeight = x.personHeight,
                           PointId = x.Pointid,
                           Date = new DateTime(x.Year, x.Month, x.Day, x.Hour, x.Minutes, x.Seconds)
                       })
                       .ToList();
                        try
                        {
                            context.CounterStatePerMinutes.AddRange(counterStatePerMinutesList);
                            await context.SaveChangesAsync();
                            result = Constants.StatusRequestOk;
                        }
                        catch (System.Exception ex)
                        {
                            result = Constants.StatusOpticounterError;
                        }
                }
                else
                {
                    result = Constants.StatusRequestListCountsNull;
                }

            return new AddCountsDbResponse()
            {
                ResponseCountsAddToDb = result,
                ResponseOrginalSerialNumber = OriginalDeviceSerialNumberDecrypt,
                ResponseCompanyName = CompanyName,
                ResponseAmount = AmountCounts,
            };
        }
        public async Task<bool> DeleteCounts(CountsDbDto countsDbDto)
        {
            CountsModel CountsModelResponse = new CountsModel();
            bool result = false;

            DateTime dateTimeNow = DateTime.Now.Date;
            DateTime dateTomorrow = dateTimeNow.Date.AddDays(1);
            try
            {
                var contextOptispace = _contextDb.SetOptiSpaceContext(countsDbDto.Company);
                var contextOptispaceHistory = _contextDb.SetOptiSpaceHistoryContext(countsDbDto.Company);

                string OriginalDeviceSerialNumberDecrypt = Helper.DecryptCoder(countsDbDto.OriginalDeviceSerialNumberHash);

                if (OriginalDeviceSerialNumberDecrypt != "")
                {
                    var counts_in = contextOptispaceHistory.CounterStatePerMinutes.Where(x => x.Date >= dateTimeNow && x.Date <= dateTomorrow && x.PointId == countsDbDto.PortIn);
                    var counts_out = contextOptispaceHistory.CounterStatePerMinutes.Where(x => x.Date >= dateTimeNow && x.Date <= dateTomorrow && x.PointId == countsDbDto.PortOut);
                    contextOptispaceHistory.CounterStatePerMinutes.RemoveRange(counts_in);
                    contextOptispaceHistory.CounterStatePerMinutes.RemoveRange(counts_out);
                    contextOptispaceHistory.SaveChanges();
                    result = true;
                }
            }
            catch (FormatException e)
            {
                result = false;
            }

            return await Task.FromResult(result);
        }
        #endregion

        #region Point
        public Points GetPointBySerialNumber(DeviceDTO deviceDTO)
        {
            var context = _contextDb.SetOptiSpaceContext(deviceDTO.database);

            Points point = context.Points.FirstOrDefault(p => p.SerialNumber.Equals(deviceDTO.serialNumber, StringComparison.InvariantCultureIgnoreCase) == true);
            return point;// && p.Type == typeCentral);
        }
        public List<Points> GetPointChildrenSerialNumber(string companyName, long PointFatherID)
        {
            var context = _contextDb.SetOptiSpaceContext(companyName);

            List<Points> point = context.Points.Where(p => p.PointFatherId == PointFatherID).ToList();
            return point;// && p.Type == typeCentral);
        }
        public List<Points> GetPointWithChildrenSerialNumber(string companyName, string serialNumber)
        {
            var context = _contextDb.SetOptiSpaceContext(companyName);

            List<Points> points = context.Points.Where(p => p.SerialNumber.ToLower().Contains(serialNumber.ToLower())).ToList();
            return points;
        }
        public Points GetPointBySerialNumber(string companyName,string serialNumber)
        {
            var context = _contextDb.SetOptiSpaceContext(companyName);
            //https://github.com/dotnet/efcore/issues/1222
            Points point = context.Points.Where(p => p.SerialNumber.ToLower().Contains(serialNumber.ToLower())).FirstOrDefault();
            return point;
        }
        public List<Points> GetInAndOutPointsBySerialNumber(string serialNumber, string companyName)
        {
            var contextOptispace = _contextDb.SetOptiSpaceContext(companyName);
            List<Points> listOfPoints = new List<Points>();
            listOfPoints = contextOptispace.Points.Where(p => (p.SerialNumber == serialNumber) && (p.PortNumber == 0 || p.PortNumber == 1)).ToList();
            return listOfPoints;
        }
        public Task<Task<Points>> GetPointsByCompanyName(string companyName)
        {
            throw new NotImplementedException();
        }
        Task<Points> IDbManager.GetInAndOutPointsBySerialNumber(string serialNumber, string companyName)
        {
            throw new NotImplementedException();
        }
        public List<Points> GetAllInAndOutPointsByCompany(string companyName)
        {
            var contextOptispace = _contextDb.SetOptiSpaceContext(companyName);
            List<Points> listOfPoints = new List<Points>();
            listOfPoints = contextOptispace.Points.Where(p => p.PortNumber == 1 || p.PortNumber == 2).Include(a => a.PointFather).ToList();
            return listOfPoints;
        }
        #endregion

        #region Device
        public async Task<AddDeviceResponse> AddDevice(DeviceDTO deviceDTO)
        {
            AddDeviceResponse response = new AddDeviceResponse();
            try
            {
                var context = _contextDb.SetOptiSpaceContext(deviceDTO.database);
                response.CompanyName = deviceDTO.database;
                response.SerialNumber = deviceDTO.serialNumber;
                if (GetPointBySerialNumber(deviceDTO) == null)
                {
                    Points pointFather = new Points()
                    {
                        SensorType = 1,
                        Published = true,
                        Divider = 2,
                        Name = deviceDTO.serialNumber,
                        Type = 0,
                        LastWebUpdate = DateTime.Now,
                        LastActivity = DateTime.Now,
                        SerialNumber = deviceDTO.serialNumber,
                        PortNumber = 0,
                   
                    };
                    context.Points.Add(pointFather);

                    for (int i = 1; i < 9; i++)
                    {
                        Points pointChild = new Points()
                        {
                            SensorType = 1,
                            Divider = 1,
                            Type = 1,
                            LastWebUpdate = DateTime.Now,
                            LastActivity = DateTime.Now,
                            SerialNumber = deviceDTO.serialNumber,
                            PortNumber = i,
                            PointFather = pointFather
                        };
                        context.Points.Add(pointChild);
                    }
                    context.SaveChanges();
                }
                else
                {
                    response.Message = "Taka kamera znajduje się w bazie.";
                    response.Success = false;
                    return response;
                }
            }
            catch (FormatException ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
                return response;
            }
            response.Message = "Kamera została dodana";
            response.Success = true;
            return await Task.FromResult(response);
        }

        public async Task<AddDeviceResponse> RemoveDevice(DeviceDTO deviceDTO)
        {
            AddDeviceResponse response = new AddDeviceResponse();
            try
            {
                var context = _contextDb.SetOptiSpaceContext(deviceDTO.database);
                response.CompanyName = deviceDTO.database;
                response.SerialNumber = deviceDTO.serialNumber;
                List<Points> points = GetPointWithChildrenSerialNumber(deviceDTO.database, deviceDTO.serialNumber);
                if (points.Count > 0)
                {
                    context.RemoveRange(points);
                    context.SaveChanges();
                }
                else
                {
                    response.Message = "Taka kamera nie znajduje się w bazie.";
                    response.Success = false;
                    return response;
                }
            }
            catch (FormatException ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
                return response;
            }
            response.Message = "Kamera została usunięta";
            response.Success = true;
            return await Task.FromResult(response);
        }

        public async Task<AddDeviceResponse> AddExtendedDevice(DeviceExtendedDTO deviceDTO)
        {
            AddDeviceResponse response = new AddDeviceResponse();
            Points pointChildEntrance = new Points();
            Points pointChildExit = new Points();
            Points pointFather = new Points();
            try
            {
                var context = _contextDb.SetOptiSpaceContext(deviceDTO.database);
                response.CompanyName = deviceDTO.database;
                response.SerialNumber = deviceDTO.serialNumber;
                if (GetPointBySerialNumber(deviceDTO.database, deviceDTO.serialNumber) == null)
                {

                    pointFather = new Points()
                    {
                        SensorType = 1,
                        Published = true,
                        Divider = 2,
                        Name = deviceDTO.deviceName,
                        Type = 0,
                        LastWebUpdate = DateTime.Now,
                        LastActivity = DateTime.Now,
                        SerialNumber = deviceDTO.serialNumber,
                        PortNumber = 0,
                        //CameraMacAddress = deviceDTO.cameraMacAddress
                    };
                   
                    for (int i = 1; i < 9; i++)
                    {
                        if (i == 1)
                        {
                             pointChildEntrance = new Points()
                            {
                                Name = "Wejście",
                                SensorType = 1,
                                Divider = 1,
                                Type = 1,
                                LastWebUpdate = DateTime.Now,
                                LastActivity = DateTime.Now,
                                SerialNumber = deviceDTO.serialNumber,
                                PortNumber = i,
                                PointFather = pointFather,
                                Published = true
                            };
                            continue;
                        }
                        if (i == 2)
                        {
                             pointChildExit = new Points()
                            {
                                Name = "Wyjście",
                                SensorType = 1,
                                Divider = 1,
                                Type = 1,
                                LastWebUpdate = DateTime.Now,
                                LastActivity = DateTime.Now,
                                SerialNumber = deviceDTO.serialNumber,
                                PortNumber = i,
                                PointFather = pointFather,
                                Published = true
                            };
                            continue;
                        }
                        Points pointChild = new Points()
                        {
                            SensorType = 1,
                            Divider = 1,
                            Type = 1,
                            LastWebUpdate = DateTime.Now,
                            LastActivity = DateTime.Now,
                            SerialNumber = deviceDTO.serialNumber,
                            PortNumber = i,
                            PointFather = pointFather
                        };
                        context.Points.Add(pointChild);
                    }

                    ShopGroups shopGroups = new ShopGroups();

                    Shops shop = null;
                    if (!String.IsNullOrEmpty(deviceDTO.existingShopName))
                    {
                        shop = GetShopByName(deviceDTO.existingShopName, deviceDTO.database);
                        if (shop == null)
                        {
                            response.Message = "Nie mogłem znaleźć takiego sklepu w bazie.";
                            response.Success = false;
                            return response;
                        }
                    }
                    else
                    {
                        response.Message += "Sklep już istnieję. ";
                        shop = GetShopByName(deviceDTO.city + " " + deviceDTO.address, deviceDTO.database);
                    }

                    if (shop == null)
                    {
                        shop = new Shops()
                        {
                            Name = deviceDTO.city + " " + deviceDTO.address,
                            Address = deviceDTO.address,
                            City = deviceDTO.city,
                        };
                        response.Message = "Stworzyłem nowy sklep. ";
                        shopGroups.Shop = shop;
                        pointFather.Shop = shop;
                        pointChildEntrance.Shop = shop;
                        pointChildExit.Shop = shop;
                    }
                    else
                    {
                        shopGroups.ShopId = shop.Id;
                        pointFather.ShopId = shop.Id;
                        pointChildEntrance.ShopId = shop.Id;
                        pointChildExit.ShopId = shop.Id;
                    }
                   
                    Groups existingGroup = GetGroupByName(deviceDTO.database, deviceDTO.database);

                    if(existingGroup == null)
                    {
                        existingGroup = new Groups();
                        existingGroup.Name = deviceDTO.database;
                        shopGroups.Group = existingGroup;
                    }
                    else
                    { 
                       shopGroups.GroupId = existingGroup.Id;
                    }
                  

                    context.ShopGroups.Add(shopGroups); // Check whether exists
                    context.Points.Add(pointFather);
                    context.Points.Add(pointChildEntrance);
                    context.Points.Add(pointChildExit);
                    try
                    {
                        context.SaveChanges();
                    }
                    catch (System.Exception ex)
                    {
                        response.Message = "Nie mogłem zapisać w bazie danych. Skontaktuj się z działem IT.";
                        response.Success = false;
                        return response;
                    }
                    
                }
                else
                {
                    response.Message = "Taka kamera znajduje się w bazie.";
                    response.Success = false;
                    return response;
                }
            }
            catch (FormatException ex)
            {
                response.Message = ex.ToString();
                response.Success = false;
                return response;
            }
            response.Message += "Kamera została dodana! ";
            response.PointIDIn += pointChildEntrance.Id;
            response.PointIDOut += pointChildExit.Id;
            response.Success = true;
            return await Task.FromResult(response);
        }


        public Groups GetGroupByName(string GroupName, string comapnyName)
        {
            var contextOptispace = _contextDb.SetOptiSpaceContext(comapnyName);
            return contextOptispace.Groups.FirstOrDefault(s => s.Name == GroupName);
        }
        public Shops GetShopByName(string shopName, string comapnyName)
        {
            var contextOptispace = _contextDb.SetOptiSpaceContext(comapnyName);
            return contextOptispace.Shops.FirstOrDefault(s => s.Name.ToLower() == shopName.ToLower());
        }
        public async Task<IEnumerable<DeviceDetailsDto>> GetDevicesDetials(string comapnyName)
        {
            List<DeviceDetailsDto> listdeviceDetailsDto = new List<DeviceDetailsDto>();

            var contextOptispace = _contextDb.SetOptiSpaceContext(comapnyName);
            List<string> serialnumberList = new List<string>();
            serialnumberList = await contextOptispace.Points.Where(p => p.PortNumber == 0).Select(p => p.SerialNumber).ToListAsync();

            foreach (string serialNumber in serialnumberList)
            {
                DeviceDetailsDto deviceDetailsDto = new DeviceDetailsDto();
                deviceDetailsDto.serialNumber = serialNumber.ToLower();
                var pointZero = Convert.ToInt32(contextOptispace.Points.FirstOrDefault(p => p.SerialNumber == serialNumber && p.PortNumber == 0).Id);
                deviceDetailsDto.PointIn = pointZero + 1;
                deviceDetailsDto.PointOut = pointZero + 2;
                listdeviceDetailsDto.Add(deviceDetailsDto);
            }

            return listdeviceDetailsDto.AsEnumerable();
        }
        #endregion

        #region LifeSignal
        public async Task<LifeSignalDbResponse> AddLifeSignal(LiveSignalModel lifeSignalModel)
        {
            #region STEPS
            //0 - Odszyfrowywane OriginalDeviceSerialNumber aby ustalić czy zadanie pochodzi od naszych liczników
            //1 - pobieram Context -y aplikacji aby połączyć sie z baza danych
            //2 - Sprawdzam na podstawie Point czy istnieje w bazie i pobieram o niem informacje
            //3 - Jeżeli przerwa trwała dłużej to: Dodaj sygnał o tym jak długo urządzanie nie działało. Dodaj sygnał o tym że już działa. Zaktualizuj LastActivty w Point.
            //4 - Jeżeli przerwa nie trwała dłużej niż 5 min to zaktualizuj LastActivty w Point i LiveSignal
            #endregion
            string result = "";
            string OriginalDeviceSerialNumberDecrypt = "";


            DateTime nowMinusXMinutes = DateTime.Now.AddMinutes(-5);
            LifeSignalDbResponse lifeSignalDbResponse = new LifeSignalDbResponse();
            try
            {
                OriginalDeviceSerialNumberDecrypt = Helper.DecryptCoder(lifeSignalModel.OriginalDeviceSerialNumber);

                if (OriginalDeviceSerialNumberDecrypt != "")
                {
                    var contextOptispace = _contextDb.SetOptiSpaceContext(lifeSignalModel.companyName);
                    var contextOptispaceHistory = _contextDb.SetOptiSpaceHistoryContext(lifeSignalModel.companyName);
                    var point = await contextOptispace.Points.FirstOrDefaultAsync(x => x.SerialNumber == OriginalDeviceSerialNumberDecrypt && x.PortNumber == 0);

                    if (point != null)
                    {
                        if (point.LastActivity > nowMinusXMinutes) // Urządznie działo bez zmian
                        {
                            var lifeSignal = await contextOptispaceHistory.LiveSignals.FirstOrDefaultAsync(x => x.Live == true && x.DateTo > nowMinusXMinutes && x.PointId == point.Id);
                            await _databaseSupportTools.UpdateLastActivtyByPoint(contextOptispace, point);
                            await _databaseSupportTools.UpdateLiveSignalByPoint(contextOptispaceHistory, lifeSignal);
                        }
                        else //  NIE BYŁO Internetu, a teraz się pojawił.
                        {
                            await _databaseSupportTools.AddLiveSignal(contextOptispaceHistory, point.Id, point.LastActivity, DateTime.Now, false, lifeSignalModel.Type);
                            await _databaseSupportTools.AddLiveSignal(contextOptispaceHistory, point.Id, DateTime.Now, DateTime.Now, true, lifeSignalModel.Type);
                            await _databaseSupportTools.UpdateLastActivtyByPoint(contextOptispace, point);
                        }

                        result = Constants.StatusRequestOk;
                    }
                    else
                    {
                        result = Constants.StatusRequestNullPoint_LiveSignal;
                    }
                }
                else
                {
                    result = Constants.StatusRequestNotEncrypted_LiveSignal;
                }
            }
            catch (FormatException e)
            {
                result = e.ToString();
            }

            return lifeSignalDbResponse = new LifeSignalDbResponse()
            {
                ResponseLifeSignal = result,
                ResponseOrginalSerialNumber = OriginalDeviceSerialNumberDecrypt,
                ResponseCompanyName = lifeSignalModel.companyName
            };

        }
        public async Task<LifeSignalDbResponse> AddLifeSignalVpn(CreateLiveSignalVpn createLiveSignalVpn)
        {
            #region STEPS
            //0 - Odszyfrowywane snApiKey aby ustalić czy zadanie pochodzi od naszych liczników
            //1 - pobieram Context -y aplikacji aby połączyć sie z baza danych
            //2 - Sprawdzam na podstawie Point czy istnieje w bazie i pobieram o niem informacje
            //3 - Jeżeli przerwa trwała dłużej to: Dodaj sygnał o tym jak długo urządzanie nie działało. Dodaj sygnał o tym że już działa. Zaktualizuj LastActivty w Point.
            //4 - Jeżeli przerwa nie trwała dłużej niż 5 min to zaktualizuj  LiveSignal
            #endregion
            string result = "";
            string OriginalDeviceSerialNumberDecrypt = "";

            DateTime nowMinusXMinutes = DateTime.Now.AddMinutes(-5);
            LifeSignalDbResponse lifeSignalDbResponse = new LifeSignalDbResponse();
            try
            {
                OriginalDeviceSerialNumberDecrypt = Helper.DecryptCoder(createLiveSignalVpn.snApiKey);

                if (OriginalDeviceSerialNumberDecrypt != "")
                {
                    var contextOptispace = _contextDb.SetOptiSpaceContext(createLiveSignalVpn.companyName);
                    var contextOptispaceHistory = _contextDb.SetOptiSpaceHistoryContext(createLiveSignalVpn.companyName);
                    var point = await contextOptispace.Points.FirstOrDefaultAsync(x => x.SerialNumber == OriginalDeviceSerialNumberDecrypt && x.PortNumber == 0);

                    if (point != null)
                    {
                        if (point.LastActivity > nowMinusXMinutes) // Urządznie działo bez zmian
                        {
                            var lifeSignal = await contextOptispaceHistory.LiveSignalVpns.FirstOrDefaultAsync(x => x.Live == true && x.DateTo > nowMinusXMinutes && x.PointId == point.Id);
                            await _databaseSupportTools.UpdateLastActivtyByPoint(contextOptispace, point);
                            await _databaseSupportTools.UpdateLiveSignalByPointVpn(contextOptispaceHistory, lifeSignal);
                        }
                        else //  NIE BYŁO Internetu, a teraz się pojawił.
                        {
                            await _databaseSupportTools.AddLiveSignalVpn(contextOptispaceHistory, point.Id, point.LastActivity, DateTime.Now, false, createLiveSignalVpn.ip);
                            await _databaseSupportTools.AddLiveSignalVpn(contextOptispaceHistory, point.Id, DateTime.Now, DateTime.Now, true, createLiveSignalVpn.ip);
                            await _databaseSupportTools.UpdateLastActivtyByPoint(contextOptispace, point);
                        }

                        result = Constants.StatusRequestOk;
                    }
                    else
                    {
                        result = Constants.StatusRequestNullPoint_LiveSignal;
                    }
                }
                else
                {
                    result = Constants.StatusRequestNotEncrypted_LiveSignal;
                }
            }
            catch (FormatException e)
            {
                result = e.ToString();
            }

            return lifeSignalDbResponse = new LifeSignalDbResponse()
            {
                ResponseLifeSignal = result,
                ResponseOrginalSerialNumber = OriginalDeviceSerialNumberDecrypt,
                ResponseCompanyName = createLiveSignalVpn.companyName
            };
        }
        public async Task<LifeSignalDbResponse> AddLifeSignalList(List<LiveSignalModel> lifeSignalModelList)
        {
            #region STEPS
            //0 - Odszyfrowywane OriginalDeviceSerialNumber aby ustalić czy zadanie pochodzi od naszych liczników
            //1 - pobieram Context -y aplikacji aby połączyć sie z baza danych
            //2 - Sprawdzam na podstawie Point czy istnieje w bazie i pobieram o niem informacje
            //3 - Jeżeli przerwa trwała dłużej to: Dodaj sygnał o tym jak długo urządzanie nie działało. Dodaj sygnał o tym że już działa. Zaktualizuj LastActivty w Point.
            //4 - Jeżeli przerwa nie trwała dłużej niż 5 min to zaktualizuj LastActivty w Point i LiveSignal
            #endregion
            string result = "";
            string OriginalDeviceSerialNumberDecrypt = "";
            string CompanyName = "";
            DateTime nowMinusXMinutes = DateTime.Now.AddMinutes(-5);
            LifeSignalDbResponse lifeSignalDbResponse = new LifeSignalDbResponse();
            try
            {
                foreach (var singleLifeSignal in lifeSignalModelList)
                {
                    OriginalDeviceSerialNumberDecrypt = Helper.DecryptCoder(singleLifeSignal.OriginalDeviceSerialNumber);
                    if (OriginalDeviceSerialNumberDecrypt != "")
                    {
                        if (OriginalDeviceSerialNumberDecrypt != "")
                        {
                            CompanyName = singleLifeSignal.companyName;
                            var contextOptispace = _contextDb.SetOptiSpaceContext(singleLifeSignal.companyName);
                            var contextOptispaceHistory = _contextDb.SetOptiSpaceHistoryContext(singleLifeSignal.companyName);
                            var point = await contextOptispace.Points.FirstOrDefaultAsync(x => x.SerialNumber == OriginalDeviceSerialNumberDecrypt && x.PortNumber == 0);

                            if (point != null)
                            {
                                if (point.LastActivity > nowMinusXMinutes) // Urządznie działo bez zmian
                                {
                                    var lifeSignal = await contextOptispaceHistory.LiveSignals.FirstOrDefaultAsync(x => x.Live == true && x.DateTo > nowMinusXMinutes && x.PointId == point.Id);
                                    await _databaseSupportTools.UpdateLastActivtyByPoint(contextOptispace, point);
                                    await _databaseSupportTools.UpdateLiveSignalByPoint(contextOptispaceHistory, lifeSignal);
                                }
                                else //  NIE BYŁO Internetu, a teraz się pojawił.
                                {
                                    await _databaseSupportTools.AddLiveSignal(contextOptispaceHistory, point.Id, point.LastActivity, DateTime.Now, false, singleLifeSignal.Type);
                                    await _databaseSupportTools.AddLiveSignal(contextOptispaceHistory, point.Id, DateTime.Now, DateTime.Now, true, singleLifeSignal.Type);
                                    await _databaseSupportTools.UpdateLastActivtyByPoint(contextOptispace, point);
                                }

                                result = Constants.StatusRequestOk;
                            }
                            else
                            {
                                result = Constants.StatusRequestNullPoint_LiveSignal;
                            }
                        }
                        else
                        {
                            result = Constants.StatusRequestNotEncrypted_LiveSignal;
                        }
                    }
                    else
                    {
                        result = Constants.StatusRequestNotEncrypted;
                    }
                }

            }
            catch (FormatException e)
            {
                result = e.ToString();
            }

            return lifeSignalDbResponse = new LifeSignalDbResponse()
            {
                ResponseLifeSignal = result,
                ResponseOrginalSerialNumber = "allDeviceList",
                ResponseCompanyName = CompanyName
            };

        }

        #endregion

    }
}
