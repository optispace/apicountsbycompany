﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class ApplicationAuthenticationPoints
    {
        public long Id { get; set; }
        public long? ApplicationAuthenticationId { get; set; }
        public long? PointId { get; set; }

        [ForeignKey("ApplicationAuthenticationId")]
        [InverseProperty("ApplicationAuthenticationPoints")]
        public virtual ApplicationAuthentications ApplicationAuthentication { get; set; }
        [ForeignKey("PointId")]
        [InverseProperty("ApplicationAuthenticationPoints")]
        public virtual Points Point { get; set; }
    }
}
