﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class PresetGroups
    {
        public long Id { get; set; }
        public long PresetId { get; set; }
        public long GroupId { get; set; }

        [ForeignKey("GroupId")]
        [InverseProperty("PresetGroups")]
        public virtual Groups Group { get; set; }
        [ForeignKey("Id")]
        [InverseProperty("PresetGroups")]
        public virtual Presets IdNavigation { get; set; }
    }
}
