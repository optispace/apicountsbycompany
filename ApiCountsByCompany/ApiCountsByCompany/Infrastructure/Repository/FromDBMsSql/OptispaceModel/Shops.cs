﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Shops
    {
        public Shops()
        {
            Points = new HashSet<Points>();
            ShopGroups = new HashSet<ShopGroups>();
            ShopWorkTimes = new HashSet<ShopWorkTimes>();
            ShopWorkTimesToDisplay = new HashSet<ShopWorkTimesToDisplay>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public long? PresentationId { get; set; }
        public string Address { get; set; }
        public string ResponsiblePerson { get; set; }
        [Column("SID")]
        public string Sid { get; set; }
        public string MailResponsiblePerson { get; set; }
        public string Phone { get; set; }
        public string Mail { get; set; }
        public string PostalCode { get; set; }
        public string TimeOpen { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastAddWeather { get; set; }
        public string Street { get; set; }
        public string CityId { get; set; }
        public long? StoreChainId { get; set; }

        [ForeignKey("PresentationId")]
        [InverseProperty("Shops")]
        public virtual Media Presentation { get; set; }
        [ForeignKey("StoreChainId")]
        [InverseProperty("Shops")]
        public virtual StoreChains StoreChain { get; set; }
        [InverseProperty("Shop")]
        public virtual ICollection<Points> Points { get; set; }
        [InverseProperty("Shop")]
        public virtual ICollection<ShopGroups> ShopGroups { get; set; }
        [InverseProperty("Shop")]
        public virtual ICollection<ShopWorkTimes> ShopWorkTimes { get; set; }
        [InverseProperty("Shop")]
        public virtual ICollection<ShopWorkTimesToDisplay> ShopWorkTimesToDisplay { get; set; }
    }
}
