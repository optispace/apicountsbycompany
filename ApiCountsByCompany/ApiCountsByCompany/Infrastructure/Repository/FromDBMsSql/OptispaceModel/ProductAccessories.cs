﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class ProductAccessories
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public long AccessoryId { get; set; }

        [ForeignKey("AccessoryId")]
        [InverseProperty("ProductAccessoriesAccessory")]
        public virtual Products Accessory { get; set; }
        [ForeignKey("ProductId")]
        [InverseProperty("ProductAccessoriesProduct")]
        public virtual Products Product { get; set; }
    }
}
