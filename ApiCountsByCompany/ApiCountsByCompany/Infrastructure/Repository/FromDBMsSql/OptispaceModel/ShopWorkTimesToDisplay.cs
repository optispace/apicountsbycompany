﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class ShopWorkTimesToDisplay
    {
        public long Id { get; set; }
        public long ShopId { get; set; }
        public int DayOfWeek { get; set; }
        public int OpenTimeHour { get; set; }
        public int OpenTimeMinute { get; set; }
        public int CloseTimeHour { get; set; }
        public int CloseTimeMinute { get; set; }

        [ForeignKey("ShopId")]
        [InverseProperty("ShopWorkTimesToDisplay")]
        public virtual Shops Shop { get; set; }
    }
}
