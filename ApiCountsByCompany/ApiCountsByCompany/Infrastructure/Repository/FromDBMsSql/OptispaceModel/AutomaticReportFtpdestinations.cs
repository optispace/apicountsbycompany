﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    [Table("AutomaticReportFTPDestinations")]
    public partial class AutomaticReportFtpdestinations
    {
        public long Id { get; set; }
        public string Host { get; set; }
        public string Directory { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool Ssl { get; set; }
        public bool Passive { get; set; }
        public long ReportId { get; set; }

        [ForeignKey("ReportId")]
        [InverseProperty("AutomaticReportFtpdestinations")]
        public virtual AutomaticReports Report { get; set; }
    }
}
