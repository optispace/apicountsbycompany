﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Translations
    {
        public long Id { get; set; }
        public string Value { get; set; }
        public string TagNameId { get; set; }
        public long LanguageId { get; set; }

        [ForeignKey("LanguageId")]
        [InverseProperty("Translations")]
        public virtual Languages Language { get; set; }
    }
}
