﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class UserGroups
    {
        public long Id { get; set; }
        public int UserId { get; set; }
        public long GroupId { get; set; }

        [ForeignKey("GroupId")]
        [InverseProperty("UserGroups")]
        public virtual Groups Group { get; set; }
        [ForeignKey("UserId")]
        [InverseProperty("UserGroups")]
        public virtual LocalUsers User { get; set; }
    }
}
