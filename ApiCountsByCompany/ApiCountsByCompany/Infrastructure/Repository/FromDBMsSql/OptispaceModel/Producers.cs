﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Producers
    {
        public Producers()
        {
            Products = new HashSet<Products>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        [InverseProperty("ProducerOwner")]
        public virtual ICollection<Products> Products { get; set; }
    }
}
