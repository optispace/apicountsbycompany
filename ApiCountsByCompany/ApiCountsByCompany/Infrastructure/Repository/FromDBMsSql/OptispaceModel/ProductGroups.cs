﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class ProductGroups
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public long GroupId { get; set; }

        [ForeignKey("GroupId")]
        [InverseProperty("ProductGroups")]
        public virtual Groups Group { get; set; }
        [ForeignKey("Id")]
        [InverseProperty("ProductGroups")]
        public virtual Products IdNavigation { get; set; }
    }
}
