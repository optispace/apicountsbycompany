﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Groups
    {
        public Groups()
        {
            AutomaticReportGroups = new HashSet<AutomaticReportGroups>();
            MediumGroups = new HashSet<MediumGroups>();
            PointGroups = new HashSet<PointGroups>();
            PresetGroups = new HashSet<PresetGroups>();
            ProductGroups = new HashSet<ProductGroups>();
            ShopGroups = new HashSet<ShopGroups>();
            UserGroups = new HashSet<UserGroups>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        [InverseProperty("Group")]
        public virtual ICollection<AutomaticReportGroups> AutomaticReportGroups { get; set; }
        [InverseProperty("Group")]
        public virtual ICollection<MediumGroups> MediumGroups { get; set; }
        [InverseProperty("Group")]
        public virtual ICollection<PointGroups> PointGroups { get; set; }
        [InverseProperty("Group")]
        public virtual ICollection<PresetGroups> PresetGroups { get; set; }
        [InverseProperty("Group")]
        public virtual ICollection<ProductGroups> ProductGroups { get; set; }
        [InverseProperty("Group")]
        public virtual ICollection<ShopGroups> ShopGroups { get; set; }
        [InverseProperty("Group")]
        public virtual ICollection<UserGroups> UserGroups { get; set; }
    }
}
