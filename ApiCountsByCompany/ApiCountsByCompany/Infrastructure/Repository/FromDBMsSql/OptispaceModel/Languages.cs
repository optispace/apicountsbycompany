﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Languages
    {
        public Languages()
        {
            Translations = new HashSet<Translations>();
        }

        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long? FlagImageId { get; set; }
        public long? PresentationId { get; set; }

        [ForeignKey("FlagImageId")]
        [InverseProperty("LanguagesFlagImage")]
        public virtual Media FlagImage { get; set; }
        [ForeignKey("PresentationId")]
        [InverseProperty("LanguagesPresentation")]
        public virtual Media Presentation { get; set; }
        [InverseProperty("Language")]
        public virtual ICollection<Translations> Translations { get; set; }
    }
}
