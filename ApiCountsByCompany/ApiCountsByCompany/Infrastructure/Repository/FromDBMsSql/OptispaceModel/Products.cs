﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Products
    {
        public Products()
        {
            MediaImageOwner = new HashSet<Media>();
            MediaVideoOwner = new HashSet<Media>();
            Points = new HashSet<Points>();
            PresetLines = new HashSet<PresetLines>();
            ProductAccessoriesAccessory = new HashSet<ProductAccessories>();
            ProductAccessoriesProduct = new HashSet<ProductAccessories>();
            ProductParameterValues = new HashSet<ProductParameterValues>();
        }

        public long Id { get; set; }
        public long? ForeignId { get; set; }
        public int Type { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public long Order { get; set; }
        public string TagDescriptionId { get; set; }
        public string TagNameId { get; set; }
        public long? MainImageId { get; set; }
        public long? SmallImageId { get; set; }
        public long? ProducerOwnerId { get; set; }
        public string DisplayedTagNameId { get; set; }

        [ForeignKey("MainImageId")]
        [InverseProperty("ProductsMainImage")]
        public virtual Media MainImage { get; set; }
        [ForeignKey("ProducerOwnerId")]
        [InverseProperty("Products")]
        public virtual Producers ProducerOwner { get; set; }
        [ForeignKey("SmallImageId")]
        [InverseProperty("ProductsSmallImage")]
        public virtual Media SmallImage { get; set; }
        [InverseProperty("IdNavigation")]
        public virtual ProductGroups ProductGroups { get; set; }
        [InverseProperty("ImageOwner")]
        public virtual ICollection<Media> MediaImageOwner { get; set; }
        [InverseProperty("VideoOwner")]
        public virtual ICollection<Media> MediaVideoOwner { get; set; }
        [InverseProperty("Product")]
        public virtual ICollection<Points> Points { get; set; }
        [InverseProperty("Product")]
        public virtual ICollection<PresetLines> PresetLines { get; set; }
        [InverseProperty("Accessory")]
        public virtual ICollection<ProductAccessories> ProductAccessoriesAccessory { get; set; }
        [InverseProperty("Product")]
        public virtual ICollection<ProductAccessories> ProductAccessoriesProduct { get; set; }
        [InverseProperty("Product")]
        public virtual ICollection<ProductParameterValues> ProductParameterValues { get; set; }
    }
}
