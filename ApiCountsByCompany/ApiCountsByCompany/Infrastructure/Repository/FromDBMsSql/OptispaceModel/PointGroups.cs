﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class PointGroups
    {
        public long Id { get; set; }
        public long PointId { get; set; }
        public long GroupId { get; set; }

        [ForeignKey("GroupId")]
        [InverseProperty("PointGroups")]
        public virtual Groups Group { get; set; }
        [ForeignKey("Id")]
        [InverseProperty("PointGroups")]
        public virtual Points IdNavigation { get; set; }
    }
}
