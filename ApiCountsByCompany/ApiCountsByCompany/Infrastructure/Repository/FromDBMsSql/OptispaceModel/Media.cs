﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Media
    {
        public Media()
        {
            ApplicationAuthenticationsBottomAdvert = new HashSet<ApplicationAuthentications>();
            ApplicationAuthenticationsPresentation = new HashSet<ApplicationAuthentications>();
            ApplicationAuthenticationsTopAdvert = new HashSet<ApplicationAuthentications>();
            InverseFirstFrame = new HashSet<Media>();
            LanguagesFlagImage = new HashSet<Languages>();
            LanguagesPresentation = new HashSet<Languages>();
            Points = new HashSet<Points>();
            Presets = new HashSet<Presets>();
            ProductParameterValues = new HashSet<ProductParameterValues>();
            ProductsMainImage = new HashSet<Products>();
            ProductsSmallImage = new HashSet<Products>();
            Shops = new HashSet<Shops>();
        }

        public long Id { get; set; }
        public string Path { get; set; }
        public long Type { get; set; }
        public string TagNameId { get; set; }
        public long? FirstFrameId { get; set; }
        public long? ImageOwnerId { get; set; }
        public long? VideoOwnerId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime LastChanged { get; set; }
        public long Order { get; set; }

        [ForeignKey("FirstFrameId")]
        [InverseProperty("InverseFirstFrame")]
        public virtual Media FirstFrame { get; set; }
        [ForeignKey("ImageOwnerId")]
        [InverseProperty("MediaImageOwner")]
        public virtual Products ImageOwner { get; set; }
        [ForeignKey("VideoOwnerId")]
        [InverseProperty("MediaVideoOwner")]
        public virtual Products VideoOwner { get; set; }
        [InverseProperty("IdNavigation")]
        public virtual ApplicationPresentations ApplicationPresentations { get; set; }
        [InverseProperty("IdNavigation")]
        public virtual MediumGroups MediumGroups { get; set; }
        [InverseProperty("BottomAdvert")]
        public virtual ICollection<ApplicationAuthentications> ApplicationAuthenticationsBottomAdvert { get; set; }
        [InverseProperty("Presentation")]
        public virtual ICollection<ApplicationAuthentications> ApplicationAuthenticationsPresentation { get; set; }
        [InverseProperty("TopAdvert")]
        public virtual ICollection<ApplicationAuthentications> ApplicationAuthenticationsTopAdvert { get; set; }
        [InverseProperty("FirstFrame")]
        public virtual ICollection<Media> InverseFirstFrame { get; set; }
        [InverseProperty("FlagImage")]
        public virtual ICollection<Languages> LanguagesFlagImage { get; set; }
        [InverseProperty("Presentation")]
        public virtual ICollection<Languages> LanguagesPresentation { get; set; }
        [InverseProperty("MainImage")]
        public virtual ICollection<Points> Points { get; set; }
        [InverseProperty("MainImage")]
        public virtual ICollection<Presets> Presets { get; set; }
        [InverseProperty("ImageValue")]
        public virtual ICollection<ProductParameterValues> ProductParameterValues { get; set; }
        [InverseProperty("MainImage")]
        public virtual ICollection<Products> ProductsMainImage { get; set; }
        [InverseProperty("SmallImage")]
        public virtual ICollection<Products> ProductsSmallImage { get; set; }
        [InverseProperty("Presentation")]
        public virtual ICollection<Shops> Shops { get; set; }
    }
}
