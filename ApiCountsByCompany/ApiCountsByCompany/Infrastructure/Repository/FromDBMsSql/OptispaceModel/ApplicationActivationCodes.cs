﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class ApplicationActivationCodes
    {
        public ApplicationActivationCodes()
        {
            ApplicationAuthentications = new HashSet<ApplicationAuthentications>();
        }

        public long Id { get; set; }
        public string Value { get; set; }
        public int LicensesTotal { get; set; }
        public int LicensesLeft { get; set; }
        public int DaysOfLicense { get; set; }

        [InverseProperty("ApplicationActivationCode")]
        public virtual ICollection<ApplicationAuthentications> ApplicationAuthentications { get; set; }
    }
}
