﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class ApplicationAuthentications
    {
        public ApplicationAuthentications()
        {
            ApplicationAuthenticationPoints = new HashSet<ApplicationAuthenticationPoints>();
            ApplicationPresentations = new HashSet<ApplicationPresentations>();
            OptiMediaInterfaceConfigurations = new HashSet<OptiMediaInterfaceConfigurations>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string GenerationKey { get; set; }
        public string ActivationKey { get; set; }
        public long Type { get; set; }
        public long? UserId { get; set; }
        [Column("IP")]
        public string Ip { get; set; }
        public long? PointId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime StartTimeLicense { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime EndTimeLicense { get; set; }
        public long? PresentationId { get; set; }
        public long? TopAdvertId { get; set; }
        public long? BottomAdvertId { get; set; }
        [Column("LocalUser_Id")]
        public int? LocalUserId { get; set; }
        public long? ApplictionPointId { get; set; }
        public bool Updated { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastDateTimeUpdated { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime StartDownloadTime { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime EndDownloadTime { get; set; }
        public bool IsDownloadAllowed { get; set; }
        public bool IsDownloadFinished { get; set; }
        public long HowManyTimesAskedForDownload { get; set; }
        public long GroupId { get; set; }
        public long? ApplicationActivationCodeId { get; set; }

        [ForeignKey("ApplicationActivationCodeId")]
        [InverseProperty("ApplicationAuthentications")]
        public virtual ApplicationActivationCodes ApplicationActivationCode { get; set; }
        [ForeignKey("ApplictionPointId")]
        [InverseProperty("ApplicationAuthenticationsApplictionPoint")]
        public virtual Points ApplictionPoint { get; set; }
        [ForeignKey("BottomAdvertId")]
        [InverseProperty("ApplicationAuthenticationsBottomAdvert")]
        public virtual Media BottomAdvert { get; set; }
        [ForeignKey("LocalUserId")]
        [InverseProperty("ApplicationAuthentications")]
        public virtual LocalUsers LocalUser { get; set; }
        [ForeignKey("PointId")]
        [InverseProperty("ApplicationAuthenticationsPoint")]
        public virtual Points Point { get; set; }
        [ForeignKey("PresentationId")]
        [InverseProperty("ApplicationAuthenticationsPresentation")]
        public virtual Media Presentation { get; set; }
        [ForeignKey("TopAdvertId")]
        [InverseProperty("ApplicationAuthenticationsTopAdvert")]
        public virtual Media TopAdvert { get; set; }
        [InverseProperty("ApplicationAuthentication")]
        public virtual ICollection<ApplicationAuthenticationPoints> ApplicationAuthenticationPoints { get; set; }
        [InverseProperty("ApplicationAuthentication")]
        public virtual ICollection<ApplicationPresentations> ApplicationPresentations { get; set; }
        [InverseProperty("Application")]
        public virtual ICollection<OptiMediaInterfaceConfigurations> OptiMediaInterfaceConfigurations { get; set; }
    }
}
