﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class CameraConfigurations
    {
        public long Id { get; set; }
        public string SerialNumber { get; set; }
        public long? PointId { get; set; }
        [Column("x1")]
        public long X1 { get; set; }
        [Column("y1")]
        public long Y1 { get; set; }
        [Column("x2")]
        public long X2 { get; set; }
        [Column("y2")]
        public long Y2 { get; set; }
        [Column("zone")]
        public long Zone { get; set; }
        public string PathCamera { get; set; }
        [Column("PathSaveCounterIN_toTXT")]
        public string PathSaveCounterInToTxt { get; set; }
        [Column("PathSaveCounterOUT_toTXT")]
        public string PathSaveCounterOutToTxt { get; set; }
        [Column("numberErode")]
        public long NumberErode { get; set; }
        [Column("circleSize")]
        public long CircleSize { get; set; }
        public string EnterString { get; set; }
        public string ExitString { get; set; }
        [Column("colorR")]
        public long ColorR { get; set; }
        [Column("colorG")]
        public long ColorG { get; set; }
        [Column("colorB")]
        public long ColorB { get; set; }
        [Column("IsDebugTXT")]
        public string IsDebugTxt { get; set; }
        [Column("IsDetectingIPTXT")]
        public string IsDetectingIptxt { get; set; }
        public string StreamPathBeforeIp { get; set; }
        public string StreamPathAfterIp { get; set; }
        public string CameraMacAddress { get; set; }
        [Column("IsShadowDetectingTXT")]
        public string IsShadowDetectingTxt { get; set; }
        public string ShadowMaxChangeH { get; set; }
        public string ShadowMaxChangeS { get; set; }
        public string ShadowMinRelativeChangeV { get; set; }
        public string ShadowMaxRelativeChangeV { get; set; }
        public string MaxWhitePixelPercentage { get; set; }
        [Column("IsDistanceHandicapForChangingDirectionEnabledTXT")]
        public string IsDistanceHandicapForChangingDirectionEnabledTxt { get; set; }
        public string DistanceHandicapValueAdded { get; set; }
        public string DistanceHandicapValueMulitiplier { get; set; }
        public string DepthSensorMinimumDistanceInMilimeters { get; set; }
        public string DepthSensorMaximumDistanceInMilimeters { get; set; }

        [ForeignKey("PointId")]
        [InverseProperty("CameraConfigurations")]
        public virtual Points Point { get; set; }
    }
}
