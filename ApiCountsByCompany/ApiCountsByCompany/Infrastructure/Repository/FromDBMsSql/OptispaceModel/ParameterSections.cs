﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class ParameterSections
    {
        public long Id { get; set; }
        public long Order { get; set; }
        public long ParameterId { get; set; }
        public long SectionId { get; set; }
        public long? StyleId { get; set; }

        [ForeignKey("ParameterId")]
        [InverseProperty("ParameterSections")]
        public virtual Parameters Parameter { get; set; }
        [ForeignKey("SectionId")]
        [InverseProperty("ParameterSections")]
        public virtual Sections Section { get; set; }
        [ForeignKey("StyleId")]
        [InverseProperty("ParameterSections")]
        public virtual Styles Style { get; set; }
    }
}
