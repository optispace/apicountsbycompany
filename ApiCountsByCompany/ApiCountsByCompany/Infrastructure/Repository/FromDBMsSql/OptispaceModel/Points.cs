﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Points
    {
        public Points()
        {
            ApplicationAuthenticationPoints = new HashSet<ApplicationAuthenticationPoints>();
            ApplicationAuthenticationsApplictionPoint = new HashSet<ApplicationAuthentications>();
            ApplicationAuthenticationsPoint = new HashSet<ApplicationAuthentications>();
            CameraConfigurations = new HashSet<CameraConfigurations>();
            Employees = new HashSet<Employees>();
            InverseObserverPointFather = new HashSet<Points>();
            InversePointFather = new HashSet<Points>();
            Lines = new HashSet<Lines>();
        }

        public long Id { get; set; }
        public string SerialNumber { get; set; }
        public long PortNumber { get; set; }
        public string Name { get; set; }
        public long Type { get; set; }
        public long X { get; set; }
        public long Y { get; set; }
        public long Z { get; set; }
        public bool Published { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime LastActivity { get; set; }
        public long? ShopId { get; set; }
        public long? PointFatherId { get; set; }
        public long? PresetId { get; set; }
        public long? ProductId { get; set; }
        public long CounterStateOnCentral { get; set; }
        public string Version { get; set; }
        public long SensorType { get; set; }
        public long Divider { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastWebUpdate { get; set; }
        public long? MainImageId { get; set; }
        [Column("CameraIPPath")]
        public string CameraIppath { get; set; }
        public long ErosionPower { get; set; }
        public long ObjectSize { get; set; }
        public string CameraInPath { get; set; }
        public string CameraOutPath { get; set; }
        public bool CameraDebugMode { get; set; }
        public long MinObjectSize { get; set; }
        public string StreamPathBeforeIp { get; set; }
        public string CameraMacAddress { get; set; }
        public string ShadowMaxChangeH { get; set; }
        public string ShadowMaxChangeS { get; set; }
        public string ShadowMinRelativeChangeV { get; set; }
        public string ShadowMaxRelativeChangeV { get; set; }
        public string MaxWhitePixelPercentage { get; set; }
        public string DistanceHandicapValueAdded { get; set; }
        public string DistanceHandicapValueMulitiplier { get; set; }
        [Column("IP")]
        public string Ip { get; set; }
        [Column("IsDetectingIPTXT")]
        public bool IsDetectingIptxt { get; set; }
        [Column("IsShadowDetectingTXT")]
        public bool IsShadowDetectingTxt { get; set; }
        [Column("IsDistanceHandicapForChangingDirectionEnabledTXT")]
        public bool IsDistanceHandicapForChangingDirectionEnabledTxt { get; set; }
        public long Height { get; set; }
        public long Width { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? VersionUpdate { get; set; }
        public string ControllerNumber { get; set; }
        public long? ObserverPointFatherId { get; set; }

        [ForeignKey("MainImageId")]
        [InverseProperty("Points")]
        public virtual Media MainImage { get; set; }
        [ForeignKey("ObserverPointFatherId")]
        [InverseProperty("InverseObserverPointFather")]
        public virtual Points ObserverPointFather { get; set; }
        [ForeignKey("PointFatherId")]
        [InverseProperty("InversePointFather")]
        public virtual Points PointFather { get; set; }
        [ForeignKey("PresetId")]
        [InverseProperty("Points")]
        public virtual Presets Preset { get; set; }
        [ForeignKey("ProductId")]
        [InverseProperty("Points")]
        public virtual Products Product { get; set; }
        [ForeignKey("ShopId")]
        [InverseProperty("Points")]
        public virtual Shops Shop { get; set; }
        [InverseProperty("IdNavigation")]
        public virtual PointGroups PointGroups { get; set; }
        [InverseProperty("Point")]
        public virtual ICollection<ApplicationAuthenticationPoints> ApplicationAuthenticationPoints { get; set; }
        [InverseProperty("ApplictionPoint")]
        public virtual ICollection<ApplicationAuthentications> ApplicationAuthenticationsApplictionPoint { get; set; }
        [InverseProperty("Point")]
        public virtual ICollection<ApplicationAuthentications> ApplicationAuthenticationsPoint { get; set; }
        [InverseProperty("Point")]
        public virtual ICollection<CameraConfigurations> CameraConfigurations { get; set; }
        [InverseProperty("Point")]
        public virtual ICollection<Employees> Employees { get; set; }
        [InverseProperty("ObserverPointFather")]
        public virtual ICollection<Points> InverseObserverPointFather { get; set; }
        [InverseProperty("PointFather")]
        public virtual ICollection<Points> InversePointFather { get; set; }
        [InverseProperty("Point")]
        public virtual ICollection<Lines> Lines { get; set; }
    }
}
