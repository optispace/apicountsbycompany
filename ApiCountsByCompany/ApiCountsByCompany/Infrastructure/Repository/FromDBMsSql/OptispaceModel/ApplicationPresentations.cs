﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class ApplicationPresentations
    {
        public long Id { get; set; }
        public long MediumId { get; set; }
        public long ApplicationAuthenticationId { get; set; }
        public long Order { get; set; }

        [ForeignKey("ApplicationAuthenticationId")]
        [InverseProperty("ApplicationPresentations")]
        public virtual ApplicationAuthentications ApplicationAuthentication { get; set; }
        [ForeignKey("Id")]
        [InverseProperty("ApplicationPresentations")]
        public virtual Media IdNavigation { get; set; }
    }
}
