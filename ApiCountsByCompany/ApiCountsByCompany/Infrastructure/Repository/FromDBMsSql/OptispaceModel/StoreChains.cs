﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class StoreChains
    {
        public StoreChains()
        {
            Shops = new HashSet<Shops>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        [InverseProperty("StoreChain")]
        public virtual ICollection<Shops> Shops { get; set; }
    }
}
