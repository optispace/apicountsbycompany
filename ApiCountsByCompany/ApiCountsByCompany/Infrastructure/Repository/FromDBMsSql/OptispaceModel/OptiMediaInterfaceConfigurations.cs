﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class OptiMediaInterfaceConfigurations
    {
        public long Id { get; set; }
        public long ApplicationId { get; set; }
        public int PresentationDelayInSeconds { get; set; }
        public bool AllowUserInputOnInterface { get; set; }
        public bool ShowCursor { get; set; }

        [ForeignKey("ApplicationId")]
        [InverseProperty("OptiMediaInterfaceConfigurations")]
        public virtual ApplicationAuthentications Application { get; set; }
    }
}
