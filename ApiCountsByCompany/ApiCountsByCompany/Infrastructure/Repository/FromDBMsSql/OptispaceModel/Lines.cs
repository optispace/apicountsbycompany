﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Lines
    {
        public long Id { get; set; }
        public long StartX { get; set; }
        public long StartY { get; set; }
        public long StartZ { get; set; }
        public long EndX { get; set; }
        public long EndY { get; set; }
        public long EndZ { get; set; }
        public long Zone { get; set; }
        public long? PointId { get; set; }
        public long ColourR { get; set; }
        public long ColourG { get; set; }
        public long ColourB { get; set; }
        public string Name { get; set; }
        public string Label1 { get; set; }
        public string Label2 { get; set; }

        [ForeignKey("PointId")]
        [InverseProperty("Lines")]
        public virtual Points Point { get; set; }
    }
}
