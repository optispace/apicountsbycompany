﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class ProductParameterValues
    {
        public long Id { get; set; }
        public string TagValueId { get; set; }
        public int Type { get; set; }
        public long? ImageValueId { get; set; }
        public long? ProductId { get; set; }
        public long? ParameterId { get; set; }

        [ForeignKey("ImageValueId")]
        [InverseProperty("ProductParameterValues")]
        public virtual Media ImageValue { get; set; }
        [ForeignKey("ParameterId")]
        [InverseProperty("ProductParameterValues")]
        public virtual Parameters Parameter { get; set; }
        [ForeignKey("ProductId")]
        [InverseProperty("ProductParameterValues")]
        public virtual Products Product { get; set; }
    }
}
