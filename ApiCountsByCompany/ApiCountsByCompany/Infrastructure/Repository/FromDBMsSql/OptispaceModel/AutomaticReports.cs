﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class AutomaticReports
    {
        public AutomaticReports()
        {
            AutomaticReportFtpdestinations = new HashSet<AutomaticReportFtpdestinations>();
            AutomaticReportGroups = new HashSet<AutomaticReportGroups>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime StartDate { get; set; }
        public string ReportInterval { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime NextDate { get; set; }
        public bool SendNow { get; set; }
        public bool Enabled { get; set; }
        public string SerialNumber { get; set; }
        public int DaysNumber { get; set; }
        public string Emails { get; set; }
        public int MinuteInterval { get; set; }
        public int HourInterval { get; set; }
        public int DayInterval { get; set; }
        public int MonthInterval { get; set; }
        public string Cron { get; set; }
        public string FileType { get; set; }
        public string FileEncoding { get; set; }
        [StringLength(10)]
        public string Locale { get; set; }

        [InverseProperty("Report")]
        public virtual ICollection<AutomaticReportFtpdestinations> AutomaticReportFtpdestinations { get; set; }
        [InverseProperty("Report")]
        public virtual ICollection<AutomaticReportGroups> AutomaticReportGroups { get; set; }
    }
}
