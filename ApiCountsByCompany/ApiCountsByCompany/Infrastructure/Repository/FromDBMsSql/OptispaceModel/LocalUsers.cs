﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class LocalUsers
    {
        public LocalUsers()
        {
            ApplicationAuthentications = new HashSet<ApplicationAuthentications>();
            UserGroups = new HashSet<UserGroups>();
        }

        public int Id { get; set; }
        public int DatabaseMainId { get; set; }
        public int AuthenticaitonType { get; set; }

        [InverseProperty("LocalUser")]
        public virtual ICollection<ApplicationAuthentications> ApplicationAuthentications { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<UserGroups> UserGroups { get; set; }
    }
}
