﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Styles
    {
        public Styles()
        {
            ParameterSections = new HashSet<ParameterSections>();
            SectionsStyleName = new HashSet<Sections>();
            SectionsStyleParameter = new HashSet<Sections>();
            SectionsStyleValue = new HashSet<Sections>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public int FontSize { get; set; }
        public int ColorR { get; set; }
        public int ColorG { get; set; }
        public int ColorB { get; set; }
        public bool Published { get; set; }
        public bool Bold { get; set; }
        public bool Cursive { get; set; }
        public bool Underlined { get; set; }
        public bool Uppercase { get; set; }
        public string FontType { get; set; }

        [InverseProperty("Style")]
        public virtual ICollection<ParameterSections> ParameterSections { get; set; }
        [InverseProperty("StyleName")]
        public virtual ICollection<Sections> SectionsStyleName { get; set; }
        [InverseProperty("StyleParameter")]
        public virtual ICollection<Sections> SectionsStyleParameter { get; set; }
        [InverseProperty("StyleValue")]
        public virtual ICollection<Sections> SectionsStyleValue { get; set; }
    }
}
