﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class ShopGroups
    {
        public long Id { get; set; }
        public long ShopId { get; set; }
        public long GroupId { get; set; }

        [ForeignKey("GroupId")]
        [InverseProperty("ShopGroups")]
        public virtual Groups Group { get; set; }
        [ForeignKey("ShopId")]
        [InverseProperty("ShopGroups")]
        public virtual Shops Shop { get; set; }
    }
}
