﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class PresetLines
    {
        public long Id { get; set; }
        public string PortNumber { get; set; }
        public long? ProductId { get; set; }
        public long PresetOwnerId { get; set; }

        [ForeignKey("PresetOwnerId")]
        [InverseProperty("PresetLines")]
        public virtual Presets PresetOwner { get; set; }
        [ForeignKey("ProductId")]
        [InverseProperty("PresetLines")]
        public virtual Products Product { get; set; }
    }
}
