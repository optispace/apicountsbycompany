﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class MediumGroups
    {
        public long Id { get; set; }
        public long MediumId { get; set; }
        public long GroupId { get; set; }

        [ForeignKey("GroupId")]
        [InverseProperty("MediumGroups")]
        public virtual Groups Group { get; set; }
        [ForeignKey("Id")]
        [InverseProperty("MediumGroups")]
        public virtual Media IdNavigation { get; set; }
    }
}
