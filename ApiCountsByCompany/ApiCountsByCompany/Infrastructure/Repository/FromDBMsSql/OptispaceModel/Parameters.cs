﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Parameters
    {
        public Parameters()
        {
            InverseParameterFather = new HashSet<Parameters>();
            ParameterSections = new HashSet<ParameterSections>();
            ProductParameterValues = new HashSet<ProductParameterValues>();
        }

        public long Id { get; set; }
        public string TagNameId { get; set; }
        public bool Deleted { get; set; }
        public bool Published { get; set; }
        public long Order { get; set; }
        public long? ParameterFatherId { get; set; }
        public string DisplayedTagNameId { get; set; }

        [ForeignKey("ParameterFatherId")]
        [InverseProperty("InverseParameterFather")]
        public virtual Parameters ParameterFather { get; set; }
        [InverseProperty("ParameterFather")]
        public virtual ICollection<Parameters> InverseParameterFather { get; set; }
        [InverseProperty("Parameter")]
        public virtual ICollection<ParameterSections> ParameterSections { get; set; }
        [InverseProperty("Parameter")]
        public virtual ICollection<ProductParameterValues> ProductParameterValues { get; set; }
    }
}
