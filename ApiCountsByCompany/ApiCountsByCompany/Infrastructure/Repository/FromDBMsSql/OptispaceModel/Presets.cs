﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Presets
    {
        public Presets()
        {
            Points = new HashSet<Points>();
            PresetLines = new HashSet<PresetLines>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }
        public long? MainImageId { get; set; }

        [ForeignKey("MainImageId")]
        [InverseProperty("Presets")]
        public virtual Media MainImage { get; set; }
        [InverseProperty("IdNavigation")]
        public virtual PresetGroups PresetGroups { get; set; }
        [InverseProperty("Preset")]
        public virtual ICollection<Points> Points { get; set; }
        [InverseProperty("PresetOwner")]
        public virtual ICollection<PresetLines> PresetLines { get; set; }
    }
}
