﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class AutomaticReportGroups
    {
        public long Id { get; set; }
        public long ReportId { get; set; }
        public long GroupId { get; set; }

        [ForeignKey("GroupId")]
        [InverseProperty("AutomaticReportGroups")]
        public virtual Groups Group { get; set; }
        [ForeignKey("ReportId")]
        [InverseProperty("AutomaticReportGroups")]
        public virtual AutomaticReports Report { get; set; }
    }
}
