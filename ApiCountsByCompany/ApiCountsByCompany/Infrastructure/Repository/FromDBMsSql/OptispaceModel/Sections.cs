﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class Sections
    {
        public Sections()
        {
            ParameterSections = new HashSet<ParameterSections>();
        }

        public long Id { get; set; }
        public string TagNameId { get; set; }
        public bool Deleted { get; set; }
        public bool MenuPublished { get; set; }
        public long Order { get; set; }
        public long StyleNameId { get; set; }
        public long StyleParameterId { get; set; }
        public long StyleValueId { get; set; }
        public string DisplayedTagNameId { get; set; }

        [ForeignKey("StyleNameId")]
        [InverseProperty("SectionsStyleName")]
        public virtual Styles StyleName { get; set; }
        [ForeignKey("StyleParameterId")]
        [InverseProperty("SectionsStyleParameter")]
        public virtual Styles StyleParameter { get; set; }
        [ForeignKey("StyleValueId")]
        [InverseProperty("SectionsStyleValue")]
        public virtual Styles StyleValue { get; set; }
        [InverseProperty("Section")]
        public virtual ICollection<ParameterSections> ParameterSections { get; set; }
    }
}
