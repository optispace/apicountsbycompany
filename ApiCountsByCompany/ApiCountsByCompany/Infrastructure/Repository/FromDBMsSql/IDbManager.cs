﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Domain.Model;
using ApiCountsByCompany.Infrastructure.Commands.Device;
using ApiCountsByCompany.Infrastructure.Commands.Summary.query;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql
{
    public interface IDbManager : IRepository
    {
        Task<CountsModel> GetRealCountsByCompanyName(CountsDbDto getCountsDbDto);

        Task<int> GetCountsDayByPoints(OptiSpaceHistoryContext optiSpaceHistoryContext, List<int> listPoint, DateTime currentDateTime);

        Task<Task<Points>> GetPointsByCompanyName(string companyName);
        Task<Points> GetInAndOutPointsBySerialNumber(string serialNumber, string companyName);
        Task<AddDeviceResponse> AddExtendedDevice(DeviceExtendedDTO deviceDTO);
        List<Points> GetAllInAndOutPointsByCompany(string companyName);
        List<CounterStatePerMinutes> GetCounterStatePerMinutes(string companyName, List<long> pointsIds, DateTime from, DateTime to);
        Task<CountsModel> GetCounts(CountsDbDto getCountsDbDto);
        Task<CountsModel> GetAllCountsPerDay(CountsDbDto getCountsDbDto);
        Task<IEnumerable<DeviceDetailsDto>> GetDevicesDetials(string comapnyName);
        Task<string> AddCount(CountModel countmodel);
        Task<AddCountsDbResponse> AddCountList(List<CountModel> countmodelList);
        Task<LifeSignalDbResponse> AddLifeSignal(LiveSignalModel lifeSignalModel);
        Task<LifeSignalDbResponse> AddLifeSignalVpn(CreateLiveSignalVpn createLiveSignalVpn);
        Task<LifeSignalDbResponse> AddLifeSignalList(List<LiveSignalModel> lifeSignalModelList);
        Task<AddDeviceResponse> AddDevice(DeviceDTO deviceDTO);
        Task<bool> DeleteCounts(CountsDbDto getCountsDbDto);
        Task<AddDeviceResponse> RemoveDevice(DeviceDTO deviceDTO);
    }
}
