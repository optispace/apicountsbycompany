﻿using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.ConnectionToDb
{
    public interface IContextDb:IRepository
    {
        OptiSpaceContext SetOptiSpaceContext(string comapanyName);
        OptiSpaceHistoryContext SetOptiSpaceHistoryContext(string comapanyName);
    }
}
