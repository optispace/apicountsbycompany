﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.ConnectionToDb
{
    public class ContextDb : IContextDb
    {
        public string ConnectionStrings = "";
        public OptiSpaceContext SetOptiSpaceContext(string comapanyName)
        {
            ConnectionStrings = ConstantsConnectionToDb.ConnectionStringsPart1 + comapanyName + ConstantsConnectionToDb.ConnectionStringsPart2;
            return new OptiSpaceContext(dbContextOptionsBuilder(ConnectionStrings).Options);
        }

        public OptiSpaceHistoryContext SetOptiSpaceHistoryContext(string comapanyName)
        {
            ConnectionStrings = ConstantsConnectionToDb.ConnectionStringsHistoryPart1 + comapanyName + ConstantsConnectionToDb.ConnectionStringsHistoryPart2;
            return new OptiSpaceHistoryContext(dbContextOptionsBuilder(ConnectionStrings).Options);
        }


        #region Tools in class

        public DbContextOptionsBuilder dbContextOptionsBuilder(string connectionStrings)
        {
            var optionsBuilder = new DbContextOptionsBuilder();
            optionsBuilder.UseSqlServer(connectionStrings);
            return optionsBuilder;
        }

        #endregion
    }
}
