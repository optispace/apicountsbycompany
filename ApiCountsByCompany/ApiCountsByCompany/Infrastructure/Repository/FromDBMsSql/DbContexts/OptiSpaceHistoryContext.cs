﻿using System;
using System.Threading.Tasks;
using ApiCountsByCompany.Common;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.OptispaceHistoryModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel
{
    public partial class OptiSpaceHistoryContext : DbContext
    {
        public OptiSpaceHistoryContext()
        {
        }

        public OptiSpaceHistoryContext(DbContextOptions<OptiSpaceHistoryContext> options)
            : base(options)
        {
        }

        public OptiSpaceHistoryContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Alarms> Alarms { get; set; }
        public virtual DbSet<AproximatedCountsPerHours> AproximatedCountsPerHours { get; set; }
        public virtual DbSet<CountOvps> CountOvps { get; set; }
        public virtual DbSet<CounterStatePerDay> CounterStatePerDay { get; set; }
        public virtual DbSet<CounterStatePerHourProccesseds> CounterStatePerHourProccesseds { get; set; }
        public virtual DbSet<CounterStatePerHours> CounterStatePerHours { get; set; }
        public virtual DbSet<CounterStatePerMinutes> CounterStatePerMinutes { get; set; }
        public virtual DbSet<CoverSignals> CoverSignals { get; set; }
        public virtual DbSet<HistoryDescriptions> HistoryDescriptions { get; set; }
        public virtual DbSet<HistoryUserActivities> HistoryUserActivities { get; set; }
        public virtual DbSet<HistoryUsers> HistoryUsers { get; set; }
        public virtual DbSet<LiveSignalVpns> LiveSignalVpns { get; set; }
        public virtual DbSet<LiveSignals> LiveSignals { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<PickUps> PickUps { get; set; }
        public virtual DbSet<RecordingWorkTimes> RecordingWorkTimes { get; set; }
        public virtual DbSet<Weathers> Weathers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=192.168.1.59\\SQL;Database=OptiSpaceHistory.AdRepublic;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.1-servicing-10028");

            modelBuilder.Entity<CounterStatePerDay>().ToTable("CounterStatePerDay");

            modelBuilder.Entity<Alarms>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("datetime");
            });

            modelBuilder.Entity<AproximatedCountsPerHours>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("datetime");
            });

            modelBuilder.Entity<CountOvps>(entity =>
            {
                entity.ToTable("CountOVPs");

                entity.Property(e => e.Date).HasColumnType("datetime");
            });

            modelBuilder.Entity<CounterStatePerHourProccesseds>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("datetime");
            });

            modelBuilder.Entity<CounterStatePerHours>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("datetime");
            });

            modelBuilder.Entity<CounterStatePerMinutes>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("datetime");
            });

            modelBuilder.Entity<CoverSignals>(entity =>
            {
                entity.Property(e => e.DateFrom).HasColumnType("datetime");

                entity.Property(e => e.DateTo).HasColumnType("datetime");
            });

            modelBuilder.Entity<HistoryUserActivities>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("datetime");
            });

            modelBuilder.Entity<HistoryUsers>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("datetime");
            });

            modelBuilder.Entity<LiveSignalVpns>(entity =>
            {
                entity.Property(e => e.DateFrom).HasColumnType("datetime");

                entity.Property(e => e.DateTo).HasColumnType("datetime");
            });

            modelBuilder.Entity<LiveSignals>(entity =>
            {
                entity.Property(e => e.DateFrom).HasColumnType("datetime");

                entity.Property(e => e.DateTo).HasColumnType("datetime");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<PickUps>(entity =>
            {
                entity.Property(e => e.DateDown).HasColumnType("datetime");

                entity.Property(e => e.DateUp).HasColumnType("datetime");
            });

            modelBuilder.Entity<RecordingWorkTimes>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("datetime");
            });

            modelBuilder.Entity<Weathers>(entity =>
            {
                entity.Property(e => e.DateTime).HasColumnType("datetime");

                entity.Property(e => e.Pressure).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Temperature).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Wind).HasColumnType("decimal(18, 2)");
            });
        }
    }
}

