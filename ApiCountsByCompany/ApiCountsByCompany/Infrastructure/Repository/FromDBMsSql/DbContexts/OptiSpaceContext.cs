﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel
{
    public partial class OptiSpaceContext : DbContext
    {
        public OptiSpaceContext()
        {
        }

        public OptiSpaceContext(DbContextOptions<OptiSpaceContext> options)
            : base(options)
        {
        }

        public OptiSpaceContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<ApplicationActivationCodes> ApplicationActivationCodes { get; set; }
        public virtual DbSet<ApplicationAuthenticationPoints> ApplicationAuthenticationPoints { get; set; }
        public virtual DbSet<ApplicationAuthentications> ApplicationAuthentications { get; set; }
        public virtual DbSet<ApplicationPresentations> ApplicationPresentations { get; set; }
        public virtual DbSet<AutomaticReportFtpdestinations> AutomaticReportFtpdestinations { get; set; }
        public virtual DbSet<AutomaticReportGroups> AutomaticReportGroups { get; set; }
        public virtual DbSet<AutomaticReports> AutomaticReports { get; set; }
        public virtual DbSet<CameraConfigurations> CameraConfigurations { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<GlobalConfigurations> GlobalConfigurations { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
        public virtual DbSet<Languages> Languages { get; set; }
        public virtual DbSet<Lines> Lines { get; set; }
        public virtual DbSet<LocalUsers> LocalUsers { get; set; }
        public virtual DbSet<Media> Media { get; set; }
        public virtual DbSet<MediumGroups> MediumGroups { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }
        public virtual DbSet<OptiMediaInterfaceConfigurations> OptiMediaInterfaceConfigurations { get; set; }
        public virtual DbSet<ParameterSections> ParameterSections { get; set; }
        public virtual DbSet<Parameters> Parameters { get; set; }
        public virtual DbSet<PointGroups> PointGroups { get; set; }
        public virtual DbSet<Points> Points { get; set; }
        public virtual DbSet<PresetGroups> PresetGroups { get; set; }
        public virtual DbSet<PresetLines> PresetLines { get; set; }
        public virtual DbSet<Presets> Presets { get; set; }
        public virtual DbSet<Producers> Producers { get; set; }
        public virtual DbSet<ProductAccessories> ProductAccessories { get; set; }
        public virtual DbSet<ProductGroups> ProductGroups { get; set; }
        public virtual DbSet<ProductParameterValues> ProductParameterValues { get; set; }
        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<Remotes> Remotes { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<Sections> Sections { get; set; }
        public virtual DbSet<ShopGroups> ShopGroups { get; set; }
        public virtual DbSet<ShopWorkTimes> ShopWorkTimes { get; set; }
        public virtual DbSet<ShopWorkTimesToDisplay> ShopWorkTimesToDisplay { get; set; }
        public virtual DbSet<Shops> Shops { get; set; }
        public virtual DbSet<StoreChains> StoreChains { get; set; }
        public virtual DbSet<Styles> Styles { get; set; }
        public virtual DbSet<Translations> Translations { get; set; }
        public virtual DbSet<UserGroups> UserGroups { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.


                optionsBuilder.UseSqlServer("Server=L48\\SQLEXPRESS;Database=OptiSpace.Dodenhof;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<ApplicationAuthenticationPoints>(entity =>
            {
                entity.HasIndex(e => e.ApplicationAuthenticationId)
                    .HasName("IX_ApplicationAuthenticationId");

                entity.HasIndex(e => e.PointId)
                    .HasName("IX_PointId");

                entity.HasOne(d => d.ApplicationAuthentication)
                    .WithMany(p => p.ApplicationAuthenticationPoints)
                    .HasForeignKey(d => d.ApplicationAuthenticationId)
                    .HasConstraintName("FK_dbo.ApplicationAuthenticationPoints_dbo.ApplicationAuthentications_ApplicationAuthenticationId");

                entity.HasOne(d => d.Point)
                    .WithMany(p => p.ApplicationAuthenticationPoints)
                    .HasForeignKey(d => d.PointId)
                    .HasConstraintName("FK_dbo.ApplicationAuthenticationPoints_dbo.Points_PointId");
            });

            modelBuilder.Entity<ApplicationAuthentications>(entity =>
            {
                entity.HasIndex(e => e.ApplicationActivationCodeId)
                    .HasName("IX_ApplicationActivationCodeId");

                entity.HasIndex(e => e.ApplictionPointId)
                    .HasName("IX_ApplictionPointId");

                entity.HasIndex(e => e.BottomAdvertId)
                    .HasName("IX_BottomAdvertId");

                entity.HasIndex(e => e.GroupId)
                    .HasName("IX_GroupId");

                entity.HasIndex(e => e.LocalUserId)
                    .HasName("IX_LocalUser_Id");

                entity.HasIndex(e => e.PointId)
                    .HasName("IX_PointId");

                entity.HasIndex(e => e.PresentationId)
                    .HasName("IX_PresentationId");

                entity.HasIndex(e => e.TopAdvertId)
                    .HasName("IX_TopAdvertId");

                entity.HasOne(d => d.ApplicationActivationCode)
                    .WithMany(p => p.ApplicationAuthentications)
                    .HasForeignKey(d => d.ApplicationActivationCodeId)
                    .HasConstraintName("FK_dbo.ApplicationAuthentications_dbo.ApplicationActivationCodes_ApplicationActivationCodeId");

                entity.HasOne(d => d.ApplictionPoint)
                    .WithMany(p => p.ApplicationAuthenticationsApplictionPoint)
                    .HasForeignKey(d => d.ApplictionPointId)
                    .HasConstraintName("FK_dbo.ApplicationAuthentications_dbo.Points_ApplictionPointId");

                entity.HasOne(d => d.BottomAdvert)
                    .WithMany(p => p.ApplicationAuthenticationsBottomAdvert)
                    .HasForeignKey(d => d.BottomAdvertId)
                    .HasConstraintName("FK_dbo.ApplicationAuthentications_dbo.Media_BottomAdvertId");

                entity.HasOne(d => d.LocalUser)
                    .WithMany(p => p.ApplicationAuthentications)
                    .HasForeignKey(d => d.LocalUserId)
                    .HasConstraintName("FK_dbo.ApplicationAuthentications_dbo.LocalUsers_LocalUser_Id");

                entity.HasOne(d => d.Point)
                    .WithMany(p => p.ApplicationAuthenticationsPoint)
                    .HasForeignKey(d => d.PointId)
                    .HasConstraintName("FK_dbo.ApplicationAuthentications_dbo.Points_PointId");

                entity.HasOne(d => d.Presentation)
                    .WithMany(p => p.ApplicationAuthenticationsPresentation)
                    .HasForeignKey(d => d.PresentationId)
                    .HasConstraintName("FK_dbo.ApplicationAuthentications_dbo.Media_PresentationId");

                entity.HasOne(d => d.TopAdvert)
                    .WithMany(p => p.ApplicationAuthenticationsTopAdvert)
                    .HasForeignKey(d => d.TopAdvertId)
                    .HasConstraintName("FK_dbo.ApplicationAuthentications_dbo.Media_TopAdvertId");
            });

            modelBuilder.Entity<ApplicationPresentations>(entity =>
            {
                entity.HasIndex(e => e.ApplicationAuthenticationId)
                    .HasName("IX_ApplicationAuthenticationId");

                entity.HasIndex(e => e.Id)
                    .HasName("IX_Id");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.ApplicationAuthentication)
                    .WithMany(p => p.ApplicationPresentations)
                    .HasForeignKey(d => d.ApplicationAuthenticationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ApplicationPresentations_dbo.ApplicationAuthentications_ApplicationAuthenticationId");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.ApplicationPresentations)
                    .HasForeignKey<ApplicationPresentations>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ApplicationPresentations_dbo.Media_Id");
            });

            modelBuilder.Entity<AutomaticReportFtpdestinations>(entity =>
            {
                entity.HasIndex(e => e.ReportId)
                    .HasName("IX_ReportId");

                entity.HasOne(d => d.Report)
                    .WithMany(p => p.AutomaticReportFtpdestinations)
                    .HasForeignKey(d => d.ReportId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.AutomaticReportFTPDestinations_dbo.AutomaticReports_ReportId");
            });

            modelBuilder.Entity<AutomaticReportGroups>(entity =>
            {
                entity.HasIndex(e => e.GroupId)
                    .HasName("IX_GroupId");

                entity.HasIndex(e => e.ReportId)
                    .HasName("IX_ReportId");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.AutomaticReportGroups)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.AutomaticReportGroups_dbo.Groups_GroupId");

                entity.HasOne(d => d.Report)
                    .WithMany(p => p.AutomaticReportGroups)
                    .HasForeignKey(d => d.ReportId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.AutomaticReportGroups_dbo.AutomaticReports_ReportId");
            });

            modelBuilder.Entity<CameraConfigurations>(entity =>
            {
                entity.HasIndex(e => e.PointId)
                    .HasName("IX_PointId");

                entity.HasOne(d => d.Point)
                    .WithMany(p => p.CameraConfigurations)
                    .HasForeignKey(d => d.PointId)
                    .HasConstraintName("FK_dbo.CameraConfigurations_dbo.Points_PointId");
            });

            modelBuilder.Entity<Employees>(entity =>
            {
                entity.HasIndex(e => e.PointId)
                    .HasName("IX_PointId");

                entity.HasOne(d => d.Point)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.PointId)
                    .HasConstraintName("FK_dbo.Employees_dbo.Points_PointId");
            });

            modelBuilder.Entity<Languages>(entity =>
            {
                entity.HasIndex(e => e.FlagImageId)
                    .HasName("IX_FlagImageId");

                entity.HasIndex(e => e.PresentationId)
                    .HasName("IX_PresentationId");

                entity.HasOne(d => d.FlagImage)
                    .WithMany(p => p.LanguagesFlagImage)
                    .HasForeignKey(d => d.FlagImageId)
                    .HasConstraintName("FK_dbo.Languages_dbo.Media_FlagImageId");

                entity.HasOne(d => d.Presentation)
                    .WithMany(p => p.LanguagesPresentation)
                    .HasForeignKey(d => d.PresentationId)
                    .HasConstraintName("FK_dbo.Languages_dbo.Media_PresentationId");
            });

            modelBuilder.Entity<Lines>(entity =>
            {
                entity.HasIndex(e => e.PointId)
                    .HasName("IX_PointId");

                entity.HasOne(d => d.Point)
                    .WithMany(p => p.Lines)
                    .HasForeignKey(d => d.PointId)
                    .HasConstraintName("FK_dbo.Lines_dbo.Points_PointId");
            });

            modelBuilder.Entity<Media>(entity =>
            {
                entity.HasIndex(e => e.FirstFrameId)
                    .HasName("IX_FirstFrameId");

                entity.HasIndex(e => e.ImageOwnerId)
                    .HasName("IX_ImageOwnerId");

                entity.HasIndex(e => e.VideoOwnerId)
                    .HasName("IX_VideoOwnerId");

                entity.HasOne(d => d.FirstFrame)
                    .WithMany(p => p.InverseFirstFrame)
                    .HasForeignKey(d => d.FirstFrameId)
                    .HasConstraintName("FK_dbo.Media_dbo.Media_FirstFrameId");

                entity.HasOne(d => d.ImageOwner)
                    .WithMany(p => p.MediaImageOwner)
                    .HasForeignKey(d => d.ImageOwnerId)
                    .HasConstraintName("FK_dbo.Media_dbo.Products_ImageOwnerId");

                entity.HasOne(d => d.VideoOwner)
                    .WithMany(p => p.MediaVideoOwner)
                    .HasForeignKey(d => d.VideoOwnerId)
                    .HasConstraintName("FK_dbo.Media_dbo.Products_VideoOwnerId");
            });

            modelBuilder.Entity<MediumGroups>(entity =>
            {
                entity.HasIndex(e => e.GroupId)
                    .HasName("IX_GroupId");

                entity.HasIndex(e => e.Id)
                    .HasName("IX_Id");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.MediumGroups)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MediumGroups_dbo.Groups_GroupId");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.MediumGroups)
                    .HasForeignKey<MediumGroups>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.MediumGroups_dbo.Media_Id");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");
            });

            modelBuilder.Entity<OptiMediaInterfaceConfigurations>(entity =>
            {
                entity.HasIndex(e => e.ApplicationId)
                    .HasName("IX_ApplicationId");

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.OptiMediaInterfaceConfigurations)
                    .HasForeignKey(d => d.ApplicationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.OptiMediaInterfaceConfigurations_dbo.ApplicationAuthentications_ApplicationId");
            });

            modelBuilder.Entity<ParameterSections>(entity =>
            {
                entity.HasIndex(e => e.ParameterId)
                    .HasName("IX_ParameterId");

                entity.HasIndex(e => e.SectionId)
                    .HasName("IX_SectionId");

                entity.HasIndex(e => e.StyleId)
                    .HasName("IX_StyleId");

                entity.HasOne(d => d.Parameter)
                    .WithMany(p => p.ParameterSections)
                    .HasForeignKey(d => d.ParameterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ParameterSections_dbo.Parameters_ParameterId");

                entity.HasOne(d => d.Section)
                    .WithMany(p => p.ParameterSections)
                    .HasForeignKey(d => d.SectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ParameterSections_dbo.Sections_SectionId");

                entity.HasOne(d => d.Style)
                    .WithMany(p => p.ParameterSections)
                    .HasForeignKey(d => d.StyleId)
                    .HasConstraintName("FK_dbo.ParameterSections_dbo.Styles_StyleId");
            });

            modelBuilder.Entity<Parameters>(entity =>
            {
                entity.HasIndex(e => e.ParameterFatherId)
                    .HasName("IX_ParameterFatherId");

                entity.HasOne(d => d.ParameterFather)
                    .WithMany(p => p.InverseParameterFather)
                    .HasForeignKey(d => d.ParameterFatherId)
                    .HasConstraintName("FK_dbo.Parameters_dbo.Parameters_ParameterFatherId");
            });

            modelBuilder.Entity<PointGroups>(entity =>
            {
                entity.HasIndex(e => e.GroupId)
                    .HasName("IX_GroupId");

                entity.HasIndex(e => e.Id)
                    .HasName("IX_Id");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.PointGroups)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.PointGroups_dbo.Groups_GroupId");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.PointGroups)
                    .HasForeignKey<PointGroups>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.PointGroups_dbo.Points_Id");
            });

            modelBuilder.Entity<Points>(entity =>
            {
                entity.HasIndex(e => e.MainImageId)
                    .HasName("IX_MainImageId");

                entity.HasIndex(e => e.ObserverPointFatherId)
                    .HasName("IX_ObserverPointFatherId");

                entity.HasIndex(e => e.PointFatherId)
                    .HasName("IX_PointFatherId");

                entity.HasIndex(e => e.PresetId)
                    .HasName("IX_PresetId");

                entity.HasIndex(e => e.ProductId)
                    .HasName("IX_ProductId");

                entity.HasIndex(e => e.ShopId)
                    .HasName("IX_ShopId");

                entity.HasOne(d => d.MainImage)
                    .WithMany(p => p.Points)
                    .HasForeignKey(d => d.MainImageId)
                    .HasConstraintName("FK_dbo.Points_dbo.Media_MainImageId");

                entity.HasOne(d => d.ObserverPointFather)
                    .WithMany(p => p.InverseObserverPointFather)
                    .HasForeignKey(d => d.ObserverPointFatherId)
                    .HasConstraintName("FK_dbo.Points_dbo.Points_ObserverPointFatherId");

                entity.HasOne(d => d.PointFather)
                    .WithMany(p => p.InversePointFather)
                    .HasForeignKey(d => d.PointFatherId)
                    .HasConstraintName("FK_dbo.Points_dbo.Points_PointFatherId");

                entity.HasOne(d => d.Preset)
                    .WithMany(p => p.Points)
                    .HasForeignKey(d => d.PresetId)
                    .HasConstraintName("FK_dbo.Points_dbo.Presets_PresetId");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Points)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_dbo.Points_dbo.Products_ProductId");

                entity.HasOne(d => d.Shop)
                    .WithMany(p => p.Points)
                    .HasForeignKey(d => d.ShopId)
                    .HasConstraintName("FK_dbo.Points_dbo.Shops_ShopId");
            });

            modelBuilder.Entity<PresetGroups>(entity =>
            {
                entity.HasIndex(e => e.GroupId)
                    .HasName("IX_GroupId");

                entity.HasIndex(e => e.Id)
                    .HasName("IX_Id");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.PresetGroups)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.PresetGroups_dbo.Groups_GroupId");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.PresetGroups)
                    .HasForeignKey<PresetGroups>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.PresetGroups_dbo.Presets_Id");
            });

            modelBuilder.Entity<PresetLines>(entity =>
            {
                entity.HasIndex(e => e.PresetOwnerId)
                    .HasName("IX_PresetOwnerId");

                entity.HasIndex(e => e.ProductId)
                    .HasName("IX_ProductId");

                entity.HasOne(d => d.PresetOwner)
                    .WithMany(p => p.PresetLines)
                    .HasForeignKey(d => d.PresetOwnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.PresetLines_dbo.Presets_PresetOwnerId");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.PresetLines)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_dbo.PresetLines_dbo.Products_ProductId");
            });

            modelBuilder.Entity<Presets>(entity =>
            {
                entity.HasIndex(e => e.MainImageId)
                    .HasName("IX_MainImageId");

                entity.HasOne(d => d.MainImage)
                    .WithMany(p => p.Presets)
                    .HasForeignKey(d => d.MainImageId)
                    .HasConstraintName("FK_dbo.Presets_dbo.Media_MainImageId");
            });

            modelBuilder.Entity<ProductAccessories>(entity =>
            {
                entity.HasIndex(e => e.AccessoryId)
                    .HasName("IX_AccessoryId");

                entity.HasIndex(e => e.ProductId)
                    .HasName("IX_ProductId");

                entity.HasOne(d => d.Accessory)
                    .WithMany(p => p.ProductAccessoriesAccessory)
                    .HasForeignKey(d => d.AccessoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ProductAccessories_dbo.Products_AccessoryId");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductAccessoriesProduct)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ProductAccessories_dbo.Products_ProductId");
            });

            modelBuilder.Entity<ProductGroups>(entity =>
            {
                entity.HasIndex(e => e.GroupId)
                    .HasName("IX_GroupId");

                entity.HasIndex(e => e.Id)
                    .HasName("IX_Id");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.ProductGroups)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ProductGroups_dbo.Groups_GroupId");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.ProductGroups)
                    .HasForeignKey<ProductGroups>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ProductGroups_dbo.Products_Id");
            });

            modelBuilder.Entity<ProductParameterValues>(entity =>
            {
                entity.HasIndex(e => e.ImageValueId)
                    .HasName("IX_ImageValueId");

                entity.HasIndex(e => e.ParameterId)
                    .HasName("IX_ParameterId");

                entity.HasIndex(e => e.ProductId)
                    .HasName("IX_ProductId");

                entity.HasOne(d => d.ImageValue)
                    .WithMany(p => p.ProductParameterValues)
                    .HasForeignKey(d => d.ImageValueId)
                    .HasConstraintName("FK_dbo.ProductParameterValues_dbo.Media_ImageValueId");

                entity.HasOne(d => d.Parameter)
                    .WithMany(p => p.ProductParameterValues)
                    .HasForeignKey(d => d.ParameterId)
                    .HasConstraintName("FK_dbo.ProductParameterValues_dbo.Parameters_ParameterId");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductParameterValues)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_dbo.ProductParameterValues_dbo.Products_ProductId");
            });

            modelBuilder.Entity<Products>(entity =>
            {
                entity.HasIndex(e => e.MainImageId)
                    .HasName("IX_MainImageId");

                entity.HasIndex(e => e.ProducerOwnerId)
                    .HasName("IX_ProducerOwnerId");

                entity.HasIndex(e => e.SmallImageId)
                    .HasName("IX_SmallImageId");

                entity.HasOne(d => d.MainImage)
                    .WithMany(p => p.ProductsMainImage)
                    .HasForeignKey(d => d.MainImageId)
                    .HasConstraintName("FK_dbo.Products_dbo.Media_MainImageId");

                entity.HasOne(d => d.ProducerOwner)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.ProducerOwnerId)
                    .HasConstraintName("FK_dbo.Products_dbo.Producers_ProducerOwnerId");

                entity.HasOne(d => d.SmallImage)
                    .WithMany(p => p.ProductsSmallImage)
                    .HasForeignKey(d => d.SmallImageId)
                    .HasConstraintName("FK_dbo.Products_dbo.Media_SmallImageId");
            });

            modelBuilder.Entity<Sections>(entity =>
            {
                entity.HasIndex(e => e.StyleNameId)
                    .HasName("IX_StyleNameId");

                entity.HasIndex(e => e.StyleParameterId)
                    .HasName("IX_StyleParameterId");

                entity.HasIndex(e => e.StyleValueId)
                    .HasName("IX_StyleValueId");

                entity.HasOne(d => d.StyleName)
                    .WithMany(p => p.SectionsStyleName)
                    .HasForeignKey(d => d.StyleNameId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Sections_dbo.Styles_StyleNameId");

                entity.HasOne(d => d.StyleParameter)
                    .WithMany(p => p.SectionsStyleParameter)
                    .HasForeignKey(d => d.StyleParameterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Sections_dbo.Styles_StyleParameterId");

                entity.HasOne(d => d.StyleValue)
                    .WithMany(p => p.SectionsStyleValue)
                    .HasForeignKey(d => d.StyleValueId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Sections_dbo.Styles_StyleValueId");
            });

            modelBuilder.Entity<ShopGroups>(entity =>
            {
                entity.HasIndex(e => e.GroupId)
                    .HasName("IX_GroupId");

                entity.HasIndex(e => e.ShopId)
                    .HasName("IX_ShopId");

                //entity.HasOne(d => d.Group)
                //    .WithMany(p => p.ShopGroups)
                //    .HasForeignKey(d => d.GroupId)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("FK_dbo.ShopGroups_dbo.Groups_GroupId");

                //entity.HasOne(d => d.Shop)
                //    .WithMany(p => p.ShopGroups)
                //    .HasForeignKey(d => d.ShopId)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("FK_dbo.ShopGroups_dbo.Shops_ShopId");
            });

            modelBuilder.Entity<ShopWorkTimes>(entity =>
            {
                entity.HasIndex(e => e.ShopId)
                    .HasName("IX_ShopId");

                entity.HasOne(d => d.Shop)
                    .WithMany(p => p.ShopWorkTimes)
                    .HasForeignKey(d => d.ShopId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ShopWorkTimes_dbo.Shops_ShopId");
            });

            modelBuilder.Entity<ShopWorkTimesToDisplay>(entity =>
            {
                entity.HasIndex(e => e.ShopId)
                    .HasName("IX_ShopId");

                entity.HasOne(d => d.Shop)
                    .WithMany(p => p.ShopWorkTimesToDisplay)
                    .HasForeignKey(d => d.ShopId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.ShopWorkTimesToDisplay_dbo.Shops_ShopId");
            });

            modelBuilder.Entity<Shops>(entity =>
            {
                entity.HasIndex(e => e.PresentationId)
                    .HasName("IX_PresentationId");

                entity.HasIndex(e => e.StoreChainId)
                    .HasName("IX_StoreChainId");

                entity.HasOne(d => d.Presentation)
                    .WithMany(p => p.Shops)
                    .HasForeignKey(d => d.PresentationId)
                    .HasConstraintName("FK_dbo.Shops_dbo.Media_PresentationId");

                entity.HasOne(d => d.StoreChain)
                    .WithMany(p => p.Shops)
                    .HasForeignKey(d => d.StoreChainId)
                    .HasConstraintName("FK_dbo.Shops_dbo.StoreChains_StoreChainId");
            });

            modelBuilder.Entity<Translations>(entity =>
            {
                entity.HasIndex(e => e.LanguageId)
                    .HasName("IX_LanguageId");

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.Translations)
                    .HasForeignKey(d => d.LanguageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.Translations_dbo.Languages_LanguageId");
            });

            modelBuilder.Entity<UserGroups>(entity =>
            {
                entity.HasIndex(e => e.GroupId)
                    .HasName("IX_GroupId");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX_UserId");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.UserGroups)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.UserGroups_dbo.Groups_GroupId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserGroups)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.UserGroups_dbo.LocalUsers_UserId");
            });
        }
    }
}
