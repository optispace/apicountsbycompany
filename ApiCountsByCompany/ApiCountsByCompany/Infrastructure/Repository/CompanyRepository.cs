﻿using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Domain.Model;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly IFileManager _fileManager;

        public CompanyRepository(IFileManager fileManager)
        {
            _fileManager = fileManager;
        }
        public async Task<Company> GetCompanyNameBySerialNumber(string serialNumber)
        {
            return await await Task.FromResult(_fileManager.GetCompanyNameBySerialNumber(serialNumber));
        }
    }
}
