﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.ConnectionToDb;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository
{
    public class PointRepository : IPointRepository
    {
        private readonly IFileManager _fileManager;
        private readonly IDbManager _dbManager;
        private readonly IContextDb _contextDb;

        public PointRepository(IFileManager fileManager, IDbManager dbManager, IContextDb contextDb)
        {
            _fileManager = fileManager;
            _dbManager = dbManager;
            _contextDb = contextDb;
        }

        public async Task<Points> GetPointsByCompanyNameService(string serialNumber, string companyName)
        {
           return await _dbManager.GetInAndOutPointsBySerialNumber(serialNumber, companyName);
        }

        public Task<Points> GetPointsByCompanyNameService(string companyName)
        {
            throw new NotImplementedException();
        }
        public  List<Points> GetPointsByCompanyName(string companyName)
        {
            return  _dbManager.GetAllInAndOutPointsByCompany( companyName);

        }

        public List<CounterStatePerMinutes> GetCounterStatePerMinutes(string companyName,List<long> pointsIds, DateTime from, DateTime to)
        {
            return _dbManager.GetCounterStatePerMinutes(companyName,pointsIds, from, to);
        }

        public async Task<List<long>> GetPointsIdbyShopIdAsync(OptiSpaceContext optiSpaceContext, IEnumerable<long> shopIds,int portNumber)
        {
            List<long> listPoints = new List<long>();
            foreach (long shop in shopIds)
            {
                var points = optiSpaceContext.Points
                    .Where((p => p.ShopId == shop && p.PortNumber == portNumber))
                    .Select(x => x.Id);
                foreach (long point in points)
                    listPoints.Add(point);
            }
            
            return await Task.FromResult(listPoints);
        }

        public long GetPointIdBySerialnumber(OptiSpaceContext optiSpaceContext, string serialnumber)
        {
            return  optiSpaceContext.Points.FirstOrDefault(p => p.SerialNumber == serialnumber && p.PortNumber == 0).Id;                                                                              
        } 

        
    }
}
