﻿using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql.ConnectionToDb;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository
{
    public class ShopRepository : IShopRepository
    {

        public async Task<IEnumerable<long>> GetAllShopsByGroupIdAsync(OptiSpaceContext optiSpaceContext, long groupId)
            => await Task.FromResult(optiSpaceContext.ShopGroups
                .Where(p => p.GroupId == groupId)
                .Select(p => p.ShopId).ToList());

        

    }
}
