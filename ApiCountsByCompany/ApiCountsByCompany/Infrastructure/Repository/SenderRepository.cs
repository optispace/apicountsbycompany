﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.Commands.Device;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository
{
    public class SenderRepository : ISenderRepository
    {
        private readonly IFileManager _fileManager;
        private readonly IDbManager _dbManager;

        public SenderRepository(IFileManager fileManager, IDbManager dbManager)
        {
            _fileManager = fileManager;
            _dbManager = dbManager;
        }

        public async Task<AddCountsFileResponse> AddCountsToFileAsync(CountModel countModel)
        {
            string responseIsAddtoFile = await _fileManager.IsAddCountsToTxtFileBySerial_String(countModel.serialNumber, countModel.inOutMark);

            return new AddCountsFileResponse()
            {
                ResponseCountsAddToFile = responseIsAddtoFile
            };
        }

        public async Task<AddCountsDbResponse> AddCountsToDbAsync(CountModel countModel)
        {
            string responseIsAddtoDb = await _dbManager.AddCount(countModel);

            return new AddCountsDbResponse()
            {
                ResponseCountsAddToDb = responseIsAddtoDb
            };
        }

        public async Task<AddCountsFileResponse> AddCountsListFileAsync(List<CountModel> countDTOs)
        {
            string responseIsAddtoFile = await await Task.FromResult(_fileManager.IsAddCountsToTxtFileBySerialList(countDTOs));

            return new AddCountsFileResponse()
            {
                ResponseCountsAddToFile = responseIsAddtoFile
            };
        }

        public async Task<AddCountsDbResponse> AddCountsListDBAsync(List<CountModel> countsDtos)
        {
            AddCountsDbResponse responseIsAddtoDb = await await Task.FromResult(_dbManager.AddCountList(countsDtos));

            return new AddCountsDbResponse()
            {
                ResponseCountsAddToDb = responseIsAddtoDb.ResponseCountsAddToDb,
                ResponseOrginalSerialNumber = responseIsAddtoDb.ResponseOrginalSerialNumber,
                ResponseCompanyName = responseIsAddtoDb.ResponseCompanyName,
                ResponseAmount = responseIsAddtoDb.ResponseAmount,
            };

        }

        public async Task<LifeSignalDbResponse> LifeSignalAsync(LiveSignalModel lifeSignalModel)
        {
            LifeSignalDbResponse responselifeSignal = await await Task.FromResult(_dbManager.AddLifeSignal(lifeSignalModel));

            return new LifeSignalDbResponse()
            {
                ResponseLifeSignal = responselifeSignal.ResponseLifeSignal,
                ResponseOrginalSerialNumber = responselifeSignal.ResponseOrginalSerialNumber,
                ResponseCompanyName = responselifeSignal.ResponseCompanyName
            };
        }

        public async Task<LifeSignalDbResponse> LifeSignalVpnAsync(CreateLiveSignalVpn createLiveSignalVpn)
        {
            LifeSignalDbResponse responselifeSignal = await await Task.FromResult(_dbManager.AddLifeSignalVpn(createLiveSignalVpn));

            return new LifeSignalDbResponse()
            {
                ResponseLifeSignal = responselifeSignal.ResponseLifeSignal,
                ResponseOrginalSerialNumber = responselifeSignal.ResponseOrginalSerialNumber,
                ResponseCompanyName = responselifeSignal.ResponseCompanyName
            };
        }

        public async Task<LifeSignalDbResponse> LifeSignalToListDbAsync(List<LiveSignalModel> lifeSignalModelList)
        {
            LifeSignalDbResponse responselifeSignal = await await Task.FromResult(_dbManager.AddLifeSignalList(lifeSignalModelList));

            return new LifeSignalDbResponse()
            {
                ResponseLifeSignal = responselifeSignal.ResponseLifeSignal,
                ResponseOrginalSerialNumber = responselifeSignal.ResponseOrginalSerialNumber,
                ResponseCompanyName = responselifeSignal.ResponseCompanyName
            };
        }


    }
}
