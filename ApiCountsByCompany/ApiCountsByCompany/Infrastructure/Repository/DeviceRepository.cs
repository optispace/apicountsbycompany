﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Repository;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository
{
    public class DeviceRepository : IDeviceRepository
    {
        private readonly IDbManager _dbManager;

        public DeviceRepository(IDbManager dbManager)
        {
            _dbManager = dbManager;
        }

        public async Task<IEnumerable<DeviceDetailsDto>> GetDevices(string companyName)
        {
            return await _dbManager.GetDevicesDetials(companyName);
        }

        public async Task<AddDeviceResponse> AddDevice(DeviceDTO deviceDTO)
        {
            return await _dbManager.AddDevice(deviceDTO);
        }
        public async Task<AddDeviceResponse> RemoveDevice(DeviceDTO deviceDTO)
        {
            return await _dbManager.RemoveDevice(deviceDTO);
        }
        public async Task<AddDeviceResponse> AddExtendedDevice(DeviceExtendedDTO deviceDTO)
        {
            return await _dbManager.AddExtendedDevice(deviceDTO);
        }
    }
}
