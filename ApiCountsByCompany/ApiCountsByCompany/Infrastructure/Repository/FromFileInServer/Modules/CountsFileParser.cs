﻿using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Domain.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Common
{
    public static class CountsFileParser
    {
        public static string GetNameFile(string serialnumber)
        {
            string NameFile = $"{serialnumber}{DateTime.Today.ToString(Common.Constants.FileFormatCounts, CultureInfo.InvariantCulture)}{Common.Constants.FormatTxt}";
            return NameFile;
        }

        public static CountsModel AbsAmountPeopleInStore(string serialnumber)
        {
            string NameFile = CountsFileParser.GetNameFile(serialnumber);
            int In = 0;
            int Out = 0;
            int result;

            string fileName = Common.Constants.PathToFolderWithCounts + NameFile;
            if (!File.Exists(fileName))
            {
                result = 0;
            }
            else
            {
                using (StreamReader streamReader = File.OpenText(fileName))
                {
                    string inOut = String.Empty;
                    while ((inOut = streamReader.ReadLine()) != null)
                    {
                        string direction = inOut.Split(Common.Constants.SeparatorInTextFileWithCounts)[1];
                        if (direction == Common.Constants.InMark)
                        {
                            In++;
                        }
                        else if (direction == Common.Constants.OutMark)
                        {
                            Out++;
                        }
                    }
                    result = Math.Abs(In - Out);
                }
            }
            return new CountsModel()
            {
                Amount = result,
            };
        }

        public static string IsAddCountsToFile_String(string serialnumber, string inOutMark)
        {
            string result = "";
            try
            {
                string NameFile = CountsFileParser.GetNameFile(serialnumber);
                int AmountCountsToAdd = 1;
                string PathPlusFileName = Common.Constants.PathToFolderWithCounts + NameFile;

                if (!File.Exists(PathPlusFileName))
                {
                    System.IO.File.Create(Common.Constants.PathToFolderWithCounts + NameFile).Close();
                }

                using (StreamWriter streamReader = File.AppendText(PathPlusFileName))
                {
                    for (int i = 0; i < AmountCountsToAdd; i++)
                    {
                        streamReader.WriteLine(DateTime.Now.ToString(Common.Constants.FileFormatCountsHHmmss) + Common.Constants.SeparatorInTextFileWithCounts + inOutMark);
                    }
                }
                result = Constants.StatusRequestOk;
            }
            catch (Exception e)
            {
                result = e.ToString();
            }
            return result;
        }

        public static bool IsAddCountsToFile_Bool(string serialnumber, string inOutMark)
        {
            bool result = false;
            try
            {
                string NameFile = CountsFileParser.GetNameFile(serialnumber);
                int AmountCountsToAdd = 1;

                string PathPlusFileName = Common.Constants.PathToFolderWithCounts + NameFile;

                if (!File.Exists(PathPlusFileName))
                {
                    System.IO.File.Create(Common.Constants.PathToFolderWithCounts + NameFile).Close();
                }

                using (StreamWriter streamReader = File.AppendText(PathPlusFileName))
                {
                    for (int i = 0; i < AmountCountsToAdd; i++)
                    {
                        streamReader.WriteLine(DateTime.Now.ToString(Common.Constants.FileFormatCountsHHmmss) + Common.Constants.SeparatorInTextFileWithCounts + inOutMark);
                    }
                }
                result = true;
            }
            catch (Exception e)
            {
                result = false;
            }
            return result;
        }

        public static string IsAddCountsListToFile(List<CountModel> countDTOs)
        {
            string result = "";

            try
            {
                List<List<CountModel>> groups = countDTOs.GroupBy(c => new
                {
                    c.serialNumber,
                    c.Port,
                }).Select(x => x.ToList()).ToList();

                foreach (List<CountModel> item in groups)
                {
                    string NameFile = CountsFileParser.GetNameFile(item.FirstOrDefault().serialNumber);

                    string PathPlusFileName = Common.Constants.PathToFolderWithCounts + NameFile;

                    if (!File.Exists(PathPlusFileName))
                    {
                        System.IO.File.Create(Common.Constants.PathToFolderWithCounts + NameFile).Close();
                    }
                    foreach (var singleCount in item)
                    {
                        using (StreamWriter streamReader = File.AppendText(PathPlusFileName))
                        {
                            string OriginalDeviceSerialNumberDecrypt = Helper.DecryptCoder(singleCount.OriginalDeviceSerialNumber);

                            if (OriginalDeviceSerialNumberDecrypt != "")
                            {
                                streamReader.WriteLine(singleCount.Hour + ":" + singleCount.Minutes + ":" + singleCount.Seconds + Common.Constants.SeparatorInTextFileWithCounts + singleCount.Port + Common.Constants.SeparatorInTextFileWithCounts + OriginalDeviceSerialNumberDecrypt);
                            }
                        }
                    }
                }
                result = Constants.StatusRequestOk;
            }
            catch (Exception e)
            {
                result = e.ToString();
            }
            return result;
        }
        public static bool IsDeleteAllCounts(string serialnumber)
        {
            bool isDeleted = false;
            string NameFile = CountsFileParser.GetNameFile(serialnumber);
            string PathPlusFileName = Common.Constants.PathToFolderWithCounts + NameFile;

            if (!File.Exists(PathPlusFileName))
            {
                return isDeleted;
            }
            while (!isDeleted)
            {
                try
                {
                    File.Delete(PathPlusFileName);
                    isDeleted = true;
                }
                catch (Exception e)
                {
                }
            }
            return isDeleted;
        }
    }
}

