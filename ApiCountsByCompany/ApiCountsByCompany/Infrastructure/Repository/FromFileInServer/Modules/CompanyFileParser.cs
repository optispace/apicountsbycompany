﻿using ApiCountsByCompany.Core.Domain;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository
{
    public static class CompanyFileParser
    {
        public static Company GetCompanyNameBySerialnumber(string serialNumber)
        {
            string CompanyName = "";

            foreach (string line in File.ReadLines(Common.Constants.PathToFileWthNameCompanies))
            {
                string[] keyvalue = line.Split(Common.Constants.SeparatorInTextFileWithCounts);
                if (keyvalue.Length == 2 && keyvalue[1] == serialNumber)
                {
                    CompanyName = keyvalue[0];
                }
            }
            return new Company() 
            { 
                Name = CompanyName,
            };
        }
    }
}
