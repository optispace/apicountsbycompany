﻿using ApiCountsByCompany.Common;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer
{
    public class FileManager : IFileManager
    {

        public async Task<Company> GetCompanyNameBySerialNumber(string serialNumber)
        {
            return await Task.FromResult(CompanyFileParser.GetCompanyNameBySerialnumber(serialNumber));
        }
        public async Task<CountsModel> AbsAmountPeopleInStore(string company)
        {
            return await Task.FromResult(CountsFileParser.AbsAmountPeopleInStore(company));
        }
        public async Task<bool> IsAddCountsToTxtFileBySerial_Bool(string serialNumber, string inOut)
        {
            return await Task.FromResult(CountsFileParser.IsAddCountsToFile_Bool(serialNumber, inOut));
        }

        public async Task<string> IsAddCountsToTxtFileBySerial_String(string serialNumber, string inOut)
        {
            return await Task.FromResult(CountsFileParser.IsAddCountsToFile_String(serialNumber, inOut));
        }

        public async Task<string> IsAddCountsToTxtFileBySerialList(List<CountModel> countDTOs)
        {
            return await Task.FromResult(CountsFileParser.IsAddCountsListToFile(countDTOs));
        }
        public async Task<bool> IsDeleteAllCountsWithfileTxt(string serialNumber)
        {
            return await Task.FromResult(CountsFileParser.IsDeleteAllCounts(serialNumber));
        }
    }
}
