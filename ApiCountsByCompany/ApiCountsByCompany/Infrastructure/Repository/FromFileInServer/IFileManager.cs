﻿using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Domain.Model;
using ApiCountsByCompany.Infrastructure.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Infrastructure.Repository.FromFileInServer
{
    public interface IFileManager : IService
    {
        Task<Company> GetCompanyNameBySerialNumber(string serialNumber);

        Task<CountsModel> AbsAmountPeopleInStore(string company);

        Task<string> IsAddCountsToTxtFileBySerial_String(string serialNumber, string inOut);

        Task<bool> IsAddCountsToTxtFileBySerial_Bool(string serialNumber, string inOut);

        Task<string> IsAddCountsToTxtFileBySerialList(List<CountModel> countDTOs);


        Task<bool> IsDeleteAllCountsWithfileTxt(string serialNumber);
    }
}
