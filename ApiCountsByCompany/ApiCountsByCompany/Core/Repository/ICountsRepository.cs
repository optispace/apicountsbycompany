﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Domain.Model;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Repository
{
    public interface ICountsRepository :IRepository
    {
        Task<long> GetCountsByPortAsync(OptiSpaceHistoryContext optiSpaceHistoryContext, DateTime dateForm, DateTime dateTo, List<long> arrayPointId);
        Task<CountsModel> GetCountsByCompanyNameService(string companyName);
        Task<CountsModel> GetCountsByCompanyNameService_DB(CountsDbDto getCountsDbDto);
        Task<CountsModel> GetCountsByCompanyNamePerDayINplusOUTService_DB(CountsDbDto getCountsDbDto);
        Task<CountsModel> GetRealCountsByCompanyName(CountsDbDto countsDbDto);
        Task<bool> AddCountsAsync(string serialNumber, string InOut);
        Task<bool> DeleteAllCountsAsync(string serialNumber);
        Task<bool> DeleteAllCountsAsync_DB(CountsDbDto countsDbDto);
    }
}
