﻿using ApiCountsByCompany.Core.Domain.CssAi;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Repository
{
    public interface ICssAiRepository : IRepository
    {
        Task AddCounts(List<Counts> counts);
        Task AddLiveSignals(List<LiveSignals> liveSignals);
    }
}
