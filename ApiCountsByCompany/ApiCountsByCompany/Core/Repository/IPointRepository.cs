﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Domain.Model;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Repository
{
    public interface IPointRepository : IRepository
    {
        Task<List<long>> GetPointsIdbyShopIdAsync(OptiSpaceContext optiSpaceContext, IEnumerable<long> shopIds,int portNumber);
        List<Points> GetPointsByCompanyName(string companyName);
        List<CounterStatePerMinutes> GetCounterStatePerMinutes(string companyName,List<long> pointsIds, DateTime from, DateTime to);
        long GetPointIdBySerialnumber(OptiSpaceContext optiSpaceContext,string serialnumber);
    }
}
