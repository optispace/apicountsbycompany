﻿using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Infrastructure.Commands.Summary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Repository
{
    public interface ISummaryCountsRepository : IRepository
    {
        Task<IEnumerable<CountsPerDay>> GetCountsPerDayAsync(DayPeriodQuery dayPeriodQuery);
        Task<IEnumerable<CountsPerDay>> GetCountsPerDayPerHourAsync(DayPeriodQuery dayPeriodQuery);

    }
}
