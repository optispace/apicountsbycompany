﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Infrastructure.DTO;
using ApiCountsByCompany.Infrastructure.Repository.FromDBMsSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Repository
{
    public interface IDeviceRepository : IRepository
    {
        Task<IEnumerable<DeviceDetailsDto>> GetDevices(string companyName);
        Task<AddDeviceResponse> AddDevice(DeviceDTO deviceDTO);
        Task<AddDeviceResponse> AddExtendedDevice(DeviceExtendedDTO deviceDTO);
        Task<AddDeviceResponse> RemoveDevice(DeviceDTO deviceDTO);
    }
}
