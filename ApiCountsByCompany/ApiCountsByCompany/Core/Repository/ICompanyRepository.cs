﻿using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Domain.Model;
using ApiCountsByCompany.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Repository
{
    public interface ICompanyRepository :IRepository
    {
        Task<Company> GetCompanyNameBySerialNumber(string serialNumber);
    }
}
