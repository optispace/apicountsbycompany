﻿using ApiCountsByCompany.Contracts.Responses;
using ApiCountsByCompany.Core.Domain;
using ApiCountsByCompany.Infrastructure.Commands.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Repository
{
    public interface ISenderRepository : IRepository
    {
        Task<AddCountsDbResponse> AddCountsListDBAsync(List<CountModel> countsDtos);
        Task<AddCountsFileResponse> AddCountsToFileAsync(CountModel countModel);
        Task<AddCountsDbResponse> AddCountsToDbAsync(CountModel countModel);
        Task<AddCountsFileResponse> AddCountsListFileAsync(List<CountModel> countsDtos);
        Task<LifeSignalDbResponse> LifeSignalAsync(LiveSignalModel lifeSignalModel);
        Task<LifeSignalDbResponse> LifeSignalVpnAsync(CreateLiveSignalVpn createLiveSignalVpn);
        Task<LifeSignalDbResponse> LifeSignalToListDbAsync(List<LiveSignalModel> lifeSignalModelList);
    }
}
