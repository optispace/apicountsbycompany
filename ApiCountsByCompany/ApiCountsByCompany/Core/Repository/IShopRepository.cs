﻿using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Repository
{
    public interface IShopRepository:IRepository
    {
        Task<IEnumerable<long>> GetAllShopsByGroupIdAsync(OptiSpaceContext optiSpaceContext, long groupId);
    }
}
