﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Domain
{
    public class CountsPerDay
    {
        public string storeName { get; set; }
        public int count { get; set; }
        public string date { get; set; }
    }
}
