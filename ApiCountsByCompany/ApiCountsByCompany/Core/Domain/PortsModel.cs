﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Domain
{
    public class PortsModel
    {
        public long PortZero_Eye { get; set; }
        public long PortOne_Eye { get; set; }
        public long PortTwo_Eye { get; set; }
        public long PortThree_Eye { get; set; }
        public long PortFour_Eye { get; set; }
        public long PortFive_Eye { get; set; }
        public long PortSix_Eye { get; set; }
        public long PortSeven_Eye { get; set; }
        public long PortEight_Eye { get; set; }

    }
}
