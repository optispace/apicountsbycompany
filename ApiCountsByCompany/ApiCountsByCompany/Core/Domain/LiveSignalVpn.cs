﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Domain
{
    public class LiveSignalVpn
    {
        public long Id { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public bool Live { get; set; }
        public long PointId { get; set; }
        public string Ip { get; set; }

        protected LiveSignalVpn()
        {
        }
        public LiveSignalVpn(DateTime dateFrom, DateTime dateTo, bool live, long pointId, string ip)
        {
            DateFrom = DateFrom;
            DateTo = dateTo;
            Live = live;
            PointId = pointId;
            Ip = ip;
        }
    }
}
