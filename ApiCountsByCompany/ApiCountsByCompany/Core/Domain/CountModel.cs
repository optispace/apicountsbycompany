﻿using ApiCountsByCompany.Infrastructure.Repository.FromFileInServer.OptispaceHistoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Domain
{
    public class CountModel
    {
        public string serialNumber { get; set; }
        public string companyName { get;  set; }
        public string OriginalDeviceSerialNumber { get; set; }
        public int Pointid { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public int Hour { get; set; }
        public int Minutes { get; set; }
        public int Seconds { get; set; }
        public int Count { get; set; }
        public int Port { get; set; }
        public long personHeight { get; set; }
        public string inOutMark { get; set; }

    }
}
