﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Domain
{
    public class LiveSignalModel
    {
        public string companyName { get; set; }
        public string serialNumber { get; set; }
        public string OriginalDeviceSerialNumber { get; set; }
        public long Pointid { get; set; }
        public DateTime DateTimeFrom { get; set; }
        public DateTime DateTimeTo { get; set; }
        public bool Life { get; set; }
        public long Type { get; set; }

    }
}
