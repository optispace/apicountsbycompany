﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Domain
{
    public class Company
    {
        public string Name { get; set; }
    }
}
