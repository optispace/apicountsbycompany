﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Core.Domain
{
    public class DeviceModel
    {
        public string SerialNumber { get; set; }
        public string InOutMark { get; set; }
    }
}
