﻿using System;

namespace ApiCountsByCompany.Core.Domain.CssAi
{
    public class Counts
    {
        public string serialNumber { get; set; }
        public string companyName { get; set; }
        public DateTime dateTime { get; set; }
        public long countNumber { get; set; }
        public long pointId { get; set; }
        public long personHeight { get; set; }
    }
}
