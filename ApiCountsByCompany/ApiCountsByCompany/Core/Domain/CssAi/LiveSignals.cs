﻿using System;

namespace ApiCountsByCompany.Core.Domain.CssAi
{
    public class LiveSignals
    {
        public string serialNumber { get; set; }
        public string companyName { get; set; }
        public DateTime dateTimeFrom { get; set; }
        public DateTime dateTimeTo { get; set; }
        public long life { get; set; }
        public long type { get; set; }

    }
}
