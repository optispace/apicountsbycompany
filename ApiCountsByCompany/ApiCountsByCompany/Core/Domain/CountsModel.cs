﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Domain.Model
{
    public class CountsModel
    {
        public int Amount { get; set; }
        public string InOutMark { get; set; }

    }
}
