﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Contracts
{
    public class ApiRoutes
    {
        public const string Root = "";
        public const string Version = "";
        public const string Base = Root + "/" + Version;


        public static class Counts
        {
            public const string counts = Base + "/counts";
        }

        public static class CountsDB
        {
            public const string countsdb = Base + "/countsdb";
            public const string countsreal = Base + "/countsdb/realcounts";
            
        }

        public static class Sender
        {
            public const string senderCountsFileAndDbList = Base + "/sender/CountsFileAndDbList";
            public const string senderCountsFileList = Base + "/sender/CountsFileList";
            public const string senderCountsInFile = Base + "/sender/CountsFile";
            public const string senderCountsDbList = Base + "/sender/CountsDbList";
            public const string senderCountsInDb = Base + "/sender/CountsDb";

            public const string senderLifeSignal = Base + "/sender/LifeSignal";
            public const string senderLifeSignalList = Base + "/sender/LifeSignalList";

            public const string senderCountsDBList_TEST = Base + "/sender/TestAddcountsToDb";
        }

        public static class Company
        {
            public const string company = Base + "/company/";
        }

        public static class Device
        {
            public const string device = Base + "/device";
            public const string devicepoint = Base + "/device/devicepoint";
            public const string devicedetails = Base + "/device/devicedetails";
            public const string deviceLifeSignalVpn = Base + "/device/LifeSignalVpn";
        }

        public static class ReportManager
        {
            public const string getSummarryCountsByGroupIdPerDay = Base + "/ReportManager/GetSummarryCountsByGroupIdPerDay/";
        }

        public static class CssAi
        {
            public const string PostCounts = Base + "/CssAi/Counts/";
            public const string PostLiveSignals = Base + "/CssAi/LiveSignals/";
        }

    }
}