﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Contracts.Responses
{
    public class AddCountsFileAndDbResponse
    {
        public string ResponseCountsAddToDb { get; set; }
        public string ResponseOrginalSerialNumber { get; set; }
        public string ResponseCompanyName { get; set; }
        public int ResponseAmount { get; set; }
        public string ResponseCountsAddToFile { get; set; }
    }
}
