﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Contracts.Responses
{
    public class LifeSignalDbResponse
    {
        public string ResponseLifeSignal { get; set; }
        public string ResponseOrginalSerialNumber { get; set; }
        public string ResponseCompanyName { get; set; }
    }
}
