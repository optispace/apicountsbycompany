﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Contracts.Responses
{
    public class AddCountsFileResponse
    {
        public string ResponseCountsAddToFile { get; set; }
    }
}
