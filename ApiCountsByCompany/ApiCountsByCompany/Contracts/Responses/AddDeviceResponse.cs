﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCountsByCompany.Contracts.Responses
{
    public class AddDeviceResponse
    {
        public string Message { get; set; }
        public bool Success { get; set; }
        public string CompanyName { get; set; }
        public string SerialNumber { get; set; }
        public string PointIDIn { get; set; }
        public string PointIDOut { get; set; }
    }
}
