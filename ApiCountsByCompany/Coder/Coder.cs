﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Coder
{
    public class Coder
    {
            static readonly string PasswordHash = "P@@pw0rd";
            static readonly string SaltKey = "S@Ld&KEY";
            static readonly string VIKey = "@1B5c3s4e5F6g7H8";
            private static long _pierwsza = 27011;
            static readonly string keyIOS = "optiguardmsdna18";

            private static long H(string nazwa)
            {
                if (nazwa != null)
                {
                    nazwa = nazwa.Trim().RemoveAccent().RemoveNewLines();
                    long g, h = 0;

                    foreach (char p in nazwa)
                    {
                        h = (h << 4) + p;
                        if ((g = (h & 0xF0000000)) == 0)
                            h = h ^ (g | (g >> 32));
                    }
                    return h % _pierwsza + 1;
                }
                return 0;
            }

            public static string Encrypt(string plainText)
            {
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

                byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
                var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
                var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

                byte[] cipherTextBytes;

                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                        cryptoStream.FlushFinalBlock();
                        cipherTextBytes = memoryStream.ToArray();
                        cryptoStream.Close();
                    }
                    memoryStream.Close();
                }
                return Convert.ToBase64String(cipherTextBytes);
            }

            public static string Decrypt(string encryptedText)
            {
                byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
                byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
                var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

                var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
                var memoryStream = new MemoryStream(cipherTextBytes);
                var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                byte[] plainTextBytes = new byte[cipherTextBytes.Length];

                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                memoryStream.Close();
                cryptoStream.Close();
                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
            }


            public static string TryDecrypt(string encryptedText)
            {
                try
                {
                    return Coder.Decrypt(encryptedText);
                }
                catch
                {
                    return String.Empty;
                }
            }

            //https://stackoverflow.com/questions/19698272/encrypt-in-java-and-decrypt-in-c-sharp-for-aes-256-bit
            public static string EncryptForJava(string PlainText)
            {
                PlainText = PlainText.Trim();
                RijndaelManaged aes = new RijndaelManaged();
                aes.BlockSize = 128;
                aes.KeySize = 256;

                // It is equal in java 
                /// Cipher _Cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");    
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;

                //musi miec 32 znaki
                byte[] keyArr = Convert.FromBase64String("P00pw0rdS0Ld0KEY01B5c3s4e5F6g7H8xxxxxxxxxxxx");
                byte[] KeyArrBytes32Value = new byte[32];
                Array.Copy(keyArr, KeyArrBytes32Value, 32);

                // Initialization vector.   
                // It could be any value or generated using a random number generator.
                byte[] ivArr = { 5, 2, 1, 8, 3, 15, 12, 41, 22, 11, 73, 13, 17, 19, 11, 64 };
                byte[] IVBytes16Value = new byte[16];
                Array.Copy(ivArr, IVBytes16Value, 16);

                aes.Key = KeyArrBytes32Value;
                aes.IV = IVBytes16Value;

                ICryptoTransform encrypto = aes.CreateEncryptor();

                byte[] plainTextByte = ASCIIEncoding.UTF8.GetBytes(PlainText);
                byte[] CipherText = encrypto.TransformFinalBlock(plainTextByte, 0, plainTextByte.Length);
                return Convert.ToBase64String(CipherText);

            }

            public static string DecryptForJava(string CipherText)
            {
                CipherText = CipherText.Trim();
                RijndaelManaged aes = new RijndaelManaged();
                aes.BlockSize = 128;
                aes.KeySize = 256;

                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;

                //musi miec 32 znaki
                byte[] keyArr = Convert.FromBase64String("P00pw0rdS0Ld0KEY01B5c3s4e5F6g7H8xxxxxxxxxxxx");
                byte[] KeyArrBytes32Value = new byte[32];
                Array.Copy(keyArr, KeyArrBytes32Value, 32);

                // Initialization vector.   
                // It could be any value or generated using a random number generator.
                byte[] ivArr = { 5, 2, 1, 8, 3, 15, 12, 41, 22, 11, 73, 13, 17, 19, 11, 64 };
                byte[] IVBytes16Value = new byte[16];
                Array.Copy(ivArr, IVBytes16Value, 16);

                aes.Key = KeyArrBytes32Value;
                aes.IV = IVBytes16Value;

                ICryptoTransform decrypto = aes.CreateDecryptor();

                byte[] encryptedBytes = Convert.FromBase64CharArray(CipherText.ToCharArray(), 0, CipherText.Length);
                byte[] decryptedData = decrypto.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);
                return ASCIIEncoding.UTF8.GetString(decryptedData);
            }



            public static string EncryptStringIOS(string plainSourceStringToEncrypt)
            {
                //Set up the encryption objects
                using (AesCryptoServiceProvider acsp = GetProvider(Encoding.Default.GetBytes(keyIOS)))
                {
                    byte[] sourceBytes = Encoding.ASCII.GetBytes(plainSourceStringToEncrypt);
                    ICryptoTransform ictE = acsp.CreateEncryptor();

                    //Set up stream to contain the encryption
                    MemoryStream msS = new MemoryStream();

                    //Perform the encrpytion, storing output into the stream
                    CryptoStream csS = new CryptoStream(msS, ictE, CryptoStreamMode.Write);
                    csS.Write(sourceBytes, 0, sourceBytes.Length);
                    csS.FlushFinalBlock();

                    //sourceBytes are now encrypted as an array of secure bytes
                    byte[] encryptedBytes = msS.ToArray(); //.ToArray() is important, don't mess with the buffer

                    //return the encrypted bytes as a BASE64 encoded string
                    return Convert.ToBase64String(encryptedBytes);
                }
            }


            /// <summary>
            /// Decrypts a BASE64 encoded string of encrypted data, returns a plain string
            /// </summary>
            /// <param name="base64StringToDecrypt">an Aes encrypted AND base64 encoded string</param>
            /// <param name="passphrase">The passphrase.</param>
            /// <returns>returns a plain string</returns>
            public static string DecryptStringIOS(string base64StringToDecrypt)
            {
                //Set up the encryption objects
                using (AesCryptoServiceProvider acsp = GetProvider(Encoding.Default.GetBytes(keyIOS)))
                {
                    byte[] RawBytes = Convert.FromBase64String(base64StringToDecrypt);
                    ICryptoTransform ictD = acsp.CreateDecryptor();

                    //RawBytes now contains original byte array, still in Encrypted state

                    //Decrypt into stream
                    MemoryStream msD = new MemoryStream(RawBytes, 0, RawBytes.Length);
                    CryptoStream csD = new CryptoStream(msD, ictD, CryptoStreamMode.Read);
                    //csD now contains original byte array, fully decrypted

                    //return the content of msD as a regular string
                    return (new StreamReader(csD)).ReadToEnd();
                }
            }

            private static AesCryptoServiceProvider GetProvider(byte[] key)
            {
                AesCryptoServiceProvider result = new AesCryptoServiceProvider();
                result.BlockSize = 128;
                result.KeySize = 128;
                result.Mode = CipherMode.CBC;
                result.Padding = PaddingMode.PKCS7;

                result.GenerateIV();
                result.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

                byte[] RealKey = GetKey(key, result);
                result.Key = RealKey;
                // result.IV = RealKey;
                return result;
            }

            private static byte[] GetKey(byte[] suggestedKey, SymmetricAlgorithm p)
            {
                byte[] kRaw = suggestedKey;
                List<byte> kList = new List<byte>();

                for (int i = 0; i < p.LegalKeySizes[0].MinSize; i += 8)
                {
                    kList.Add(kRaw[(i / 8) % kRaw.Length]);
                }
                byte[] k = kList.ToArray();
                return k;
            }
        }


    }

