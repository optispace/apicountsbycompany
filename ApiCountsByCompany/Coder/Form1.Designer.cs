﻿namespace Coder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Coder_input_1 = new System.Windows.Forms.TextBox();
            this.Coder_input_2 = new System.Windows.Forms.TextBox();
            this.Coder_input_3 = new System.Windows.Forms.TextBox();
            this.Coder_Action_1 = new System.Windows.Forms.Button();
            this.Coder_Action_2 = new System.Windows.Forms.Button();
            this.Coder_Action_3 = new System.Windows.Forms.Button();
            this.Coder_Action_4 = new System.Windows.Forms.Button();
            this.Coder_input_4 = new System.Windows.Forms.TextBox();
            this.Coder_Action_6 = new System.Windows.Forms.Button();
            this.Coder_input_6 = new System.Windows.Forms.TextBox();
            this.Coder_Action_5 = new System.Windows.Forms.Button();
            this.Coder_input_5 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Coder_input_1
            // 
            this.Coder_input_1.Location = new System.Drawing.Point(28, 62);
            this.Coder_input_1.Name = "Coder_input_1";
            this.Coder_input_1.Size = new System.Drawing.Size(324, 20);
            this.Coder_input_1.TabIndex = 0;
            // 
            // Coder_input_2
            // 
            this.Coder_input_2.Location = new System.Drawing.Point(28, 91);
            this.Coder_input_2.Name = "Coder_input_2";
            this.Coder_input_2.Size = new System.Drawing.Size(324, 20);
            this.Coder_input_2.TabIndex = 1;
            // 
            // Coder_input_3
            // 
            this.Coder_input_3.Location = new System.Drawing.Point(28, 120);
            this.Coder_input_3.Name = "Coder_input_3";
            this.Coder_input_3.Size = new System.Drawing.Size(324, 20);
            this.Coder_input_3.TabIndex = 2;
            // 
            // Coder_Action_1
            // 
            this.Coder_Action_1.Location = new System.Drawing.Point(358, 60);
            this.Coder_Action_1.Name = "Coder_Action_1";
            this.Coder_Action_1.Size = new System.Drawing.Size(154, 23);
            this.Coder_Action_1.TabIndex = 3;
            this.Coder_Action_1.Text = "Encrypt for PC";
            this.Coder_Action_1.UseVisualStyleBackColor = true;
            this.Coder_Action_1.Click += new System.EventHandler(this.Coder_Action_1_Click);
            // 
            // Coder_Action_2
            // 
            this.Coder_Action_2.Location = new System.Drawing.Point(358, 89);
            this.Coder_Action_2.Name = "Coder_Action_2";
            this.Coder_Action_2.Size = new System.Drawing.Size(154, 23);
            this.Coder_Action_2.TabIndex = 4;
            this.Coder_Action_2.Text = "Decrypt for PC";
            this.Coder_Action_2.UseVisualStyleBackColor = true;
            this.Coder_Action_2.Click += new System.EventHandler(this.Coder_Action_2_Click);
            // 
            // Coder_Action_3
            // 
            this.Coder_Action_3.Location = new System.Drawing.Point(358, 117);
            this.Coder_Action_3.Name = "Coder_Action_3";
            this.Coder_Action_3.Size = new System.Drawing.Size(154, 23);
            this.Coder_Action_3.TabIndex = 5;
            this.Coder_Action_3.Text = "Encrypt for Android";
            this.Coder_Action_3.UseVisualStyleBackColor = true;
            this.Coder_Action_3.Click += new System.EventHandler(this.Coder_Action_3_Click);
            // 
            // Coder_Action_4
            // 
            this.Coder_Action_4.Location = new System.Drawing.Point(358, 146);
            this.Coder_Action_4.Name = "Coder_Action_4";
            this.Coder_Action_4.Size = new System.Drawing.Size(154, 23);
            this.Coder_Action_4.TabIndex = 7;
            this.Coder_Action_4.Text = "Decrypt for Android";
            this.Coder_Action_4.UseVisualStyleBackColor = true;
            this.Coder_Action_4.Click += new System.EventHandler(this.Coder_Action_4_Click);
            // 
            // Coder_input_4
            // 
            this.Coder_input_4.Location = new System.Drawing.Point(28, 149);
            this.Coder_input_4.Name = "Coder_input_4";
            this.Coder_input_4.Size = new System.Drawing.Size(324, 20);
            this.Coder_input_4.TabIndex = 6;
            // 
            // Coder_Action_6
            // 
            this.Coder_Action_6.Location = new System.Drawing.Point(358, 201);
            this.Coder_Action_6.Name = "Coder_Action_6";
            this.Coder_Action_6.Size = new System.Drawing.Size(154, 23);
            this.Coder_Action_6.TabIndex = 11;
            this.Coder_Action_6.Text = "Decrypt for IOS";
            this.Coder_Action_6.UseVisualStyleBackColor = true;
            this.Coder_Action_6.Click += new System.EventHandler(this.Coder_Action_6_Click);
            // 
            // Coder_input_6
            // 
            this.Coder_input_6.Location = new System.Drawing.Point(28, 204);
            this.Coder_input_6.Name = "Coder_input_6";
            this.Coder_input_6.Size = new System.Drawing.Size(324, 20);
            this.Coder_input_6.TabIndex = 10;
            // 
            // Coder_Action_5
            // 
            this.Coder_Action_5.Location = new System.Drawing.Point(358, 172);
            this.Coder_Action_5.Name = "Coder_Action_5";
            this.Coder_Action_5.Size = new System.Drawing.Size(154, 23);
            this.Coder_Action_5.TabIndex = 9;
            this.Coder_Action_5.Text = "Encrypt for IOS";
            this.Coder_Action_5.UseVisualStyleBackColor = true;
            this.Coder_Action_5.Click += new System.EventHandler(this.Coder_Action_5_Click);
            // 
            // Coder_input_5
            // 
            this.Coder_input_5.Location = new System.Drawing.Point(28, 175);
            this.Coder_input_5.Name = "Coder_input_5";
            this.Coder_input_5.Size = new System.Drawing.Size(324, 20);
            this.Coder_input_5.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 262);
            this.Controls.Add(this.Coder_Action_6);
            this.Controls.Add(this.Coder_input_6);
            this.Controls.Add(this.Coder_Action_5);
            this.Controls.Add(this.Coder_input_5);
            this.Controls.Add(this.Coder_Action_4);
            this.Controls.Add(this.Coder_input_4);
            this.Controls.Add(this.Coder_Action_3);
            this.Controls.Add(this.Coder_Action_2);
            this.Controls.Add(this.Coder_Action_1);
            this.Controls.Add(this.Coder_input_3);
            this.Controls.Add(this.Coder_input_2);
            this.Controls.Add(this.Coder_input_1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Coder_input_1;
        private System.Windows.Forms.TextBox Coder_input_2;
        private System.Windows.Forms.TextBox Coder_input_3;
        private System.Windows.Forms.Button Coder_Action_1;
        private System.Windows.Forms.Button Coder_Action_2;
        private System.Windows.Forms.Button Coder_Action_3;
        private System.Windows.Forms.Button Coder_Action_4;
        private System.Windows.Forms.TextBox Coder_input_4;
        private System.Windows.Forms.Button Coder_Action_6;
        private System.Windows.Forms.TextBox Coder_input_6;
        private System.Windows.Forms.Button Coder_Action_5;
        private System.Windows.Forms.TextBox Coder_input_5;
    }
}

