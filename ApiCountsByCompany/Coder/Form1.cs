﻿using System;
using System.Windows.Forms;

namespace Coder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Coder_Action_1_Click(object sender, EventArgs e)
        {
            Coder_input_1.Text = Coder.Encrypt(Coder_input_1.Text);
        }

        private void Coder_Action_2_Click(object sender, EventArgs e)
        {
            Coder_input_2.Text = Coder.Decrypt(Coder_input_2.Text);
        }

        private void Coder_Action_3_Click(object sender, EventArgs e)
        {
            Coder_input_3.Text = Coder.EncryptForJava(Coder_input_3.Text);
        }

        private void Coder_Action_4_Click(object sender, EventArgs e)
        {
            Coder_input_4.Text = Coder.DecryptForJava(Coder_input_4.Text);
        }

        private void Coder_Action_5_Click(object sender, EventArgs e)
        {
            Coder_input_5.Text = Coder.EncryptStringIOS(Coder_input_5.Text);
        }

        private void Coder_Action_6_Click(object sender, EventArgs e)
        {
            Coder_input_6.Text =Coder.DecryptStringIOS(Coder_input_6.Text);
        }
    }
}
