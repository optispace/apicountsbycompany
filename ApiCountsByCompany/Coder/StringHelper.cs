﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Coder
{
    public static class StringHelper
    {
        public static string RemoveAccent(this string txt)
        {
            byte[] bytes = Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return Encoding.ASCII.GetString(bytes);
        }
        public static string RemoveNewLines(this string txt)
        {
            return Regex.Replace(txt, @"\t|\n|\r", "");
        }

        public static string ToStringBooleanLowercase(object input)
        {
            string result = input.ToString();
            if (result.Equals("True", StringComparison.InvariantCulture) || result.Equals("False", StringComparison.InvariantCulture))
            {
                result = result.ToLower();
            }
            return result;
        }
    }
}

